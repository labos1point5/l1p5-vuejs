# Next (change me)

## Simulators

- Purchases: fix spelling

## Transition

- Table of Actions:
    - Observers can now load more items (was limited to 50 previously)
    - Add some counters (#actions, #laboratories, #actions displayed)
- Filters:
    - Observers can now combine using `AND`/`OR` "administrations" and "disciplines"


## Core

- Add a management command to change an "Administration" label: `python manage.py administrations mv LABEL_SRC LABEL_DEST`

## Carbon

- Fix `populate_commutes` command according to the new model CommuteSection/Survey models


# 4.1.5

## Simulators

- Purchases:
  - Add a warning explainig why the purchase simulator disappeared
  - Remove corresponding route

## Carbon

- Frontend: fix french typo `lien publique -> lien public`
- Backend: allow superuser to access `views.get_ghgi_consumptions`

## Transition

- Frontend: change "intern" tag icon


# 4.1.4


## Simulators

- Food simulator: fix a bug


## Transition

- Add tags: `recrutement, numérique, stagiaire, cdd, permanent`
- Change left menu to keep only `Data/Les données` menu