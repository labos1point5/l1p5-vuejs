function error {
  echo "ERROR: $@" 1>&2
  exit 1
}

function load_conf {
  if [ -z $DIR ]; then
    error "Missing \$DIR environment variable"
  fi
  if [ -e "$DIR/conf.sh" ]; then
      . $DIR/conf.sh
  else
      error "Missing $DIR/conf.sh file, create it by renaming $DIR/conf.example.sh"
  fi
}