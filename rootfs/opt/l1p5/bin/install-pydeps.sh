#!/bin/bash

set -euxo pipefail

echo "Install python dependencies"
python3 -m pip install -r /opt/l1p5/requirements.txt
