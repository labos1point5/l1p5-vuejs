import { test, expect } from '@playwright/test'
import {
  authenticatedConnectionWithTags,
  logIn,
  legalStatements,
  createGHGIForLaboratory,
  uploadBuildings,
  uploadDevices,
  uploadPurchases,
  uploadTravels,
  uploadVehicles,
  addTags,
  addCommutesAndFoodsSurvey,
  addResearchActivities,
  cloneSurvey,
  GHGIPuclicLink,
  adminLogIn
} from '../utils'

import fs from 'node:fs'
import { join } from 'path'
import { execSync } from 'child_process'

const homefsDir = join(__dirname, '..', '..')

test.afterEach(async () => {
  let stdout = execSync('python3 manage.py clean --yesiknowwhatiamdoing', {
    cwd: homefsDir,
    stdio: 'pipe',
    encoding: 'utf-8'
  })
  console.log(stdout)
})

async function go ({ page, withTags = false }, next) {
  if (withTags) {
    await authenticatedConnectionWithTags({ page })
  } else {
    await logIn({ page })
    await legalStatements({ page })
  }
  await createGHGIForLaboratory({ page })
  await next({ page })
}

async function checkExport ({ page, module }) {
  await page
    .locator('a')
    .filter({ hasText: 'Empreinte carbone & soumission' })
    .click()

  const [download] = await Promise.all([
    page.waitForEvent('download'),
    page.getByTestId(`exportToFile-${module}`).click()
  ])

  // if you want to store the reference file
  await download.saveAs('/tmp/actual.txt')
  const expected = fs.readFileSync(join(__dirname, `exports/${module}.txt`), {
    encoding: 'utf8'
  })
  const actual = fs.readFileSync(await download.path(), { encoding: 'utf8' })
  return [actual, expected]
}

test('[authenticated]add tags', async ({ page }) => {
  await logIn({ page })
  await addTags({ page })
  await expect(page.getByTestId('tags-table')).toHaveScreenshot(
    'tags-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]upload building template file', async ({ page }) => {
  await go({ page }, uploadBuildings)
  await expect(page.getByTestId('buildings-table')).toHaveScreenshot(
    'buildings-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]export buildings', async ({ page }) => {
  await go({ page }, uploadBuildings)
  let [actual, expected] = await checkExport({ page, module: 'buildings' })
  expect(removeUUID(actual)).toBe(removeUUID(expected))
})

test('[authenticated]upload devices template file', async ({ page }) => {
  await go({ page }, uploadDevices)
  await expect(page.getByTestId('devices-table')).toHaveScreenshot(
    'devices-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]export devices template file', async ({ page }) => {
  await go({ page }, uploadDevices)
  let [actual, expected] = await checkExport({ page, module: 'devices' })
  expect(actual).toBe(expected)
})

test('[authenticated]upload purchases template file', async ({ page }) => {
  await go({ page }, uploadPurchases)
  await expect(page.getByTestId('purchases-table')).toHaveScreenshot(
    'purchases-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]export purchases', async ({ page }) => {
  await go({ page }, uploadPurchases)
  let [actual, expected] = await checkExport({ page, module: 'purchases' })
  expect(actual).toBe(expected)
})

test('[authenticated]upload vechicles template file', async ({ page }) => {
  await go({ page }, uploadVehicles)
  await expect(page.getByTestId('vehicles-table')).toHaveScreenshot(
    'vehicles-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]export vehicles', async ({ page }) => {
  await go({ page }, uploadVehicles)
  let [actual, expected] = await checkExport({ page, module: 'vehicles' })
  expect(actual).toBe(expected)
})

test('[authenticated]upload travels template file', async ({ page }) => {
  await go({ page }, uploadTravels)
  await expect(page.getByTestId('travels-table')).toHaveScreenshot(
    'travels-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]export travels', async ({ page }) => {
  await go({ page }, uploadTravels)
  let [actual, expected] = await checkExport({ page, module: 'travels' })
  expect(actual).toBe(expected)
})

test('[authenticated]add commutes and foods with survey', async ({ page }) => {
  test.setTimeout(60 * 1000)
  await go({ page, withTags: true }, ({ page }) =>
    addCommutesAndFoodsSurvey({
      page,
      positionLabel: 'chercheur.e ou enseignant.e'
    })
  )
  let mask = await page.getByTestId('maskedOnTest')
  await expect(page.getByTestId('commutes-table')).toHaveScreenshot(
    'commutes-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
  await page.locator('li').filter({ hasText: 'Alimentation' }).first().click()
  await expect(page.getByTestId('food-table')).toHaveScreenshot(
    'food-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
})

test('[authenticated]export commutes/foods', async ({ page }) => {
  test.setTimeout(60 * 1000)
  await go({ page, withTags: true }, ({ page }) =>
    addCommutesAndFoodsSurvey({
      page,
      positionLabel: 'chercheur.e ou enseignant.e'
    })
  )
  let [actual, expected] = await checkExport({ page, module: 'commutes' })
  // need to get rid of the seqID (which is not reproducible)
  let sactual = actual.split('\n')
  let sexpected = expected.split('\n')
  for (let i in sactual) {
    let slactual = sactual[i].split('\t').slice(1)
    let slexpected = sexpected[i].split('\t').slice(1)
    for (let j in slactual) {
      expect(slactual[j]).toBe(slexpected[j])
    }
  }
})

test('[authenticated]clone commutes', async ({ page }) => {
  test.setTimeout(60 * 1000)
  await authenticatedConnectionWithTags({ page })
  await createGHGIForLaboratory({ page, year: '2022' })
  await addCommutesAndFoodsSurvey({
    page,
    positionLabel: 'chercheur.e ou enseignant.e'
  })

  await page.goto('http://localhost:8080/administration/all-ghgi')
  await page.getByTestId('menu-ghgis').click()
  await legalStatements({ page })
  await createGHGIForLaboratory({ page, year: '2023' })

  await cloneSurvey({ page, isCommute: true })
  // Hide ID on screenshots
  let mask = page.getByTestId('maskedOnTest')
  await expect(page.getByTestId('commutes-table')).toHaveScreenshot(
    'commutes-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
})

test('[authenticated]clone foods', async ({ page }) => {
  test.setTimeout(60 * 1000)
  await authenticatedConnectionWithTags({ page })
  await createGHGIForLaboratory({ page })
  await addCommutesAndFoodsSurvey({
    page,
    positionLabel: 'chercheur.e ou enseignant.e'
  })

  await page.goto('http://localhost:8080/administration/all-ghgi')
  await page.getByTestId('menu-ghgis').click()
  await legalStatements({ page })
  await createGHGIForLaboratory({ page, year: '2023' })

  await cloneSurvey({ page, isCommute: false })
  // Hide ID on screenshots
  let mask = await page.getByTestId('maskedOnTest')
  await expect(page.getByTestId('food-table')).toHaveScreenshot(
    'food-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
})

test('[authenticated]add research activities', async ({ page }) => {
  await go({ page, withTags: true }, ({ page }) =>
    addResearchActivities({ page, isAuthenticated: true })
  )
  await expect(page.getByTestId('ractivities-table')).toHaveScreenshot(
    'ractivities-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]export research activities', async ({ page }) => {
  await go({ page, withTags: true }, ({ page }) =>
    addResearchActivities({ page, isAuthenticated: true })
  )
  let [actual, expected] = await checkExport({ page, module: 'ractivities' })
  expect(actual).toBe(expected)
})

async function createActivityForSynthesis ({ page }) {
  await addCommutesAndFoodsSurvey({
    page,
    isAuthenticated: true,
    positionLabel: 'chercheur.e ou enseignant.e'
  })
  await uploadPurchases({ page })
  await uploadDevices({ page })
  await uploadVehicles({ page })
  await uploadBuildings({ page })
  await uploadTravels({ page })
  await addResearchActivities({ page, isAuthenticated: true })
}

test('[authenticated]synthesis', async ({ page }) => {
  test.slow()
  await authenticatedConnectionWithTags({ page })
  await createGHGIForLaboratory({ page })
  await createActivityForSynthesis({ page })
  await page.waitForTimeout(3000)
  await page
    .locator('a')
    .filter({ hasText: 'Empreinte carbone & soumission' })
    .click()
  await expect(page.getByTestId('ghgi-synthesis')).toHaveScreenshot(
    'ghgi-synhesis.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

function removeUUID (data) {
  let lines = data.split('\n')
  lines.splice(2, 1)
  return lines.join('\n')
}

test('[authenticated] export synthesis', async ({ page }) => {
  test.slow()
  await authenticatedConnectionWithTags({ page })
  await createGHGIForLaboratory({ page })
  await createActivityForSynthesis({ page })
  await page.waitForTimeout(3000)
  await page
    .locator('a')
    .filter({ hasText: 'Empreinte carbone & soumission' })
    .click()

  let [actual, expected] = await checkExport({ page, module: 'total' })
  expect(removeUUID(actual)).toBe(removeUUID(expected))
})

test('[authenticated]check public link', async ({ page }) => {
  test.setTimeout(80 * 1000)
  await authenticatedConnectionWithTags({ page })
  await createGHGIForLaboratory({ page })
  await uploadPurchases({ page })
  // spare some time and ony test the bare minimum
  // await uploadDevices({ page })
  // await uploadVehicles({ page })
  // await uploadBuildings({ page })
  // await uploadTravels({ page })
  // await addCommutesAndFoodsSurvey({ page })
  // await addResearchActivities({ page })
  await GHGIPuclicLink({ page })
  await expect(page.getByTestId('ghgi-synthesis')).toHaveScreenshot(
    'ghgi-public-synthesis.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

// SUPERADMIN tests
test('[authenticated.admin]Users management', async ({ page }) => {
  await adminLogIn({ page })
  await page.getByTestId('users-admin').click()
  await page.getByTestId('update-l1p5-test-1@l1p5.org').click()
  await page.getByTestId('input-email').fill('arachnaphobia@l1p5.org')
  await page.getByTestId('submit-update-user').click()
  // recover the original email (make the test idempotent)
  // refreshing is a way to get a full reload from the DB
  // and check that the previous actions were corrects
  await page.reload()
  await page.getByTestId('users-admin').click()
  await page.getByTestId('update-arachnaphobia@l1p5.org').click()
  await page.getByTestId('input-email').fill('l1p5-test-1@l1p5.org')
  await page.getByTestId('submit-update-user').click()
})
