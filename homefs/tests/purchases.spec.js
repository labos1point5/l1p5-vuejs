import { test, expect } from '@playwright/test'

import { anonymousConnection } from './utils'

test('upload purchases template file', async ({ page }) => {
  await anonymousConnection({ page })
  await page.locator('a').filter({ hasText: 'Achats' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page.getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' }).setInputFiles('public/static/carbon/purchasesTemplate.tsv')
  await page.getByRole('button', { name: 'Valider' }).click()
  await expect(page.getByTestId('purchases-table')).toHaveScreenshot('table-snapshot.png')
})
