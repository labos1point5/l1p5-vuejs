import { test, expect } from '@playwright/test'
import {
  anonymousConnection,
  createGHGIForEntity,
  uploadBuildings,
  uploadDevices,
  uploadPurchases,
  uploadTravels,
  uploadVehicles,
  addCommutesAndFoodsAnonymous,
  addResearchActivities
} from '../utils'

import { join } from 'path'

test('[anonymous] upload building template file', async ({ page }) => {
  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await uploadBuildings({ page })
  await expect(page.getByTestId('buildings-table')).toHaveScreenshot(
    'buildings-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[anonymous]upload devices template file', async ({ page }) => {
  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await uploadDevices({ page })
  await expect(page.getByTestId('devices-table')).toHaveScreenshot(
    'devices-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[anonymous]upload purchases template file', async ({ page }) => {
  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await uploadPurchases({ page })
  await expect(page.getByTestId('purchases-table')).toHaveScreenshot(
    'purchases-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[anonymous]upload vechicles template file', async ({ page }) => {
  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await uploadVehicles({ page })
  await expect(page.getByTestId('vehicles-table')).toHaveScreenshot(
    'vehicles-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[anonymous]upload travels template file', async ({ page }) => {
  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await uploadTravels({ page })
  await expect(page.getByTestId('travels-table')).toHaveScreenshot(
    'travels-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[anonymous]add commutes and foods', async ({ page }) => {
  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await addCommutesAndFoodsAnonymous({
    page
    // positionLabel: 'Membre de la structure'
  })
  await expect(page.getByTestId('food-table')).toHaveScreenshot(
    'food-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
  await page.locator('a').filter({ hasText: 'Dpts domicile / travail' }).click()
  await expect(page.getByTestId('commutes-table')).toHaveScreenshot(
    'commutes-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[anonymous]add research activities', async ({ page }) => {
  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await addResearchActivities({ page, isAuthenticated: false })
  await expect(page.getByTestId('ractivities-table')).toHaveScreenshot(
    'ractivities-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[anonymous]synthesis', async ({ page }) => {
  test.slow()

  await anonymousConnection({ page })
  await createGHGIForEntity({ page })
  await uploadPurchases({ page })
  await uploadDevices({ page })
  await uploadVehicles({ page })
  await uploadBuildings({ page })
  await uploadTravels({ page })
  await addCommutesAndFoodsAnonymous({
    page
    // positionLabel: 'Membre de la structure'
  })
  await addResearchActivities({ page })
  // if we go too fast the store might be in an inconsistent state ?!
  await page.waitForTimeout(3000)
  await page
    .locator('a')
    .filter({ hasText: 'Empreinte carbone & soumission' })
    .click()
  await expect(page.getByTestId('ghgi-synthesis')).toHaveScreenshot(
    'ghgi-synhesis.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})
