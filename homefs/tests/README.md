# Update the golden images

Updating the golden image requires to run the tests in a Linux environment.
To do so we wrap the execution in a linux container that you can call with:

```
./tests/runnner.sh -u

# to reuse an existing container (named playwright)
./tests/runner.sh -u -n
```

Note: The (new) golden images are available in `homefs/tests/*-snapshots` but 
may be owned by `root`.


# Run the tests

Testing requires to be in the same environment where the golden images have been generated.
`runner.sh` allows to run everything in the same Linux environement.

```
./tests/runner.sh

# to reuse an existing container (named playwright)
./tests/runner.sh -u -n
```