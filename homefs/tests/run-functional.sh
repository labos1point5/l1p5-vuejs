#!/usr/bin/env bash

# Used in CI to run the tests

set -x

# coma separated list of browsers
BROWSERS=${BROWSERS:-""}

IFS=','; BROWSERS=($BROWSERS); unset IFS;
npm install --include-dev

npx playwright install --with-deps ${BROWSERS[@]}
# make sure we kill anyway
PROJECT=""
if [[ ! -z "${BROWSERS}" ]]
then
  PROJECT="--project ${BROWSERS[@]}"
fi
npx playwright test $@ $PROJECT
rc=$?
sleep 1

echo "Exiting"
exit $rc
