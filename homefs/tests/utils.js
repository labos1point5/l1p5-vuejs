import { join } from 'path'

const homefsDir = join(__dirname, '..')

export async function createGHGIForEntity ({ page, year = '2022' }) {
  await page.getByRole('combobox').selectOption(year)
  await page.getByPlaceholder('Budget annuel *').click()
  await page.getByPlaceholder('Budget annuel *').fill('10000')

  await page.locator('input[name="pt.member"]').click()
  await page.locator('input[name="pt.member"]').fill('10')

  // save label depends whether the user is authenticated or not
  await page.getByTestId('ghgi-boundary-save').click()
}

export async function createGHGIForLaboratory ({ page, year = '2022' }) {
  await page.getByRole('combobox').selectOption(year)
  await page.getByPlaceholder('Budget annuel *').click()
  await page.getByPlaceholder('Budget annuel *').fill('10000')

  await page.locator('input[name="pt.researcher"]').click()
  await page.locator('input[name="pt.researcher"]').fill('10')
  await page.locator('input[name="pt.teacher"]').click()
  await page.locator('input[name="pt.teacher"]').fill('10')
  await page.locator('input[name="pt.support"]').click()
  await page.locator('input[name="pt.support"]').fill('10')
  await page.locator('input[name="pt.student-postdoc"]').click()
  await page.locator('input[name="pt.student-postdoc"]').fill('10')

  // save label depends whether the user is authenticated or not
  await page.getByTestId('ghgi-boundary-save').click()
}

export async function anonymousConnection ({ page }) {
  await page.goto('http://localhost:8080/ges-1point5')
  await page
    .locator('label')
    .filter({
      hasText:
        "J'affirme avoir pris connaissance de la Charte de Labos 1point5."
    })
    .locator('span')
    .first()
    .click()
  await page.getByRole('button', { name: 'Commencer' }).click()
}

export async function adminLogIn ({ page }) {
  await page.goto('http://localhost:8080/')
  await page.locator('.logbox > a').click()
  await page
    .getByRole('textbox', { name: 'Email *' })
    .fill('l1p5-test-admin-0-@l1p5.org')
  await page.getByRole('textbox', { name: 'Mot de passe *' }).fill('l1p5-test')
  await page.getByRole('button', { name: ' Se connecter' }).click()
}

export async function logIn ({ page }) {
  await page.goto('http://localhost:8080/')
  await page.locator('.logbox > a').click()
  await page
    .getByRole('textbox', { name: 'Email *' })
    .fill('l1p5-test-0@l1p5.org')
  await page.getByRole('textbox', { name: 'Mot de passe *' }).fill('l1p5-test')
  await page.getByRole('button', { name: ' Se connecter' }).click()
  // by default we land in the entity view
  await page.getByTestId('menu-ghgis').click()
  // click to add a new ghgi
}

export async function logOut ({ page }) {
  await page.locator('.logbox > a').hover()
  await page.getByTestId('log-out').click()
}

// Charter, privacy policy, authorisation
export async function legalStatements ({ page }) {
  // by default we land in the ghgi card view, to add a ghgi we need to click on
  // the special card
  await page.getByTestId('ghgi-add-card').click()
  await page
    .locator('label')
    .filter({
      hasText:
        "J'affirme avoir pris connaissance de la Charte de Labos 1point5."
    })
    .locator('span')
    .first()
    .click()
  await page
    .locator('label')
    .filter({
      hasText:
        "J'affirme avoir pris connaissance de la Politique de confidentialité de Labos"
    })
    .locator('span')
    .first()
    .click()
  await page
    .locator('label')
    .filter({ hasText: 'Je suis habilité.e par la' })
    .locator('span')
    .first()
    .click()
  await page.getByRole('button', { name: ' Commencer' }).click()
}

export async function authenticatedConnectionWithTags ({ page }) {
  await logIn({ page })
  await addTags({ page })
  await page.getByText('GES 1point5').first().click()
  await legalStatements({ page })
}

export async function newGHGI ({ page }) {
  await legalStatements({ page })
  await createGHGIForEntity({ page, year: '2023' })
}

export async function noMoreLoading ({ page }) {
  // there's a loading spin that appears when saving, make sure it disappeared before proceeding
  let loading = await page.locator('.loading-overlay')
  let count = await loading.count()
  for (let i = 0; i < count; i++) {
    await loading.nth(i).waitFor({ state: 'hidden' })
  }
}

export async function uploadBuildings ({ page }) {
  await page.locator('li').filter({ hasText: 'Bâtiments' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles(join(homefsDir, 'public/static/carbon/buildingTemplate.tsv'))
  await page.getByTestId('buildings-save').click()
  await noMoreLoading({ page })
}

export async function uploadDevices ({ page }) {
  await page.locator('li').filter({ hasText: 'Matériel informatique' }).click()
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles(join(homefsDir, 'public/static/carbon/devicesTemplate.tsv'))
  // Accept the warning due to the number of screens
  // await page.getByRole('button', { name: 'OK' }).click()
  await page.getByTestId('devices-save').click()
  await noMoreLoading({ page })
}

export async function uploadPurchases ({ page }) {
  await page.locator('li').filter({ hasText: 'Achats' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles(
      join(homefsDir, 'public/static/carbon/purchasesTemplate.tsv')
    )
  await page.getByTestId('purchases-save').click()
  await noMoreLoading({ page })
}

export async function uploadVehicles ({ page }) {
  await page.locator('li').filter({ hasText: 'Véhicules' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles(join(homefsDir, 'public/static/carbon/vehiclesTemplate.tsv'))
  await page.getByTestId('vehicles-save').click()
  await noMoreLoading({ page })
}

export async function uploadTravels ({ page }) {
  await page.getByTestId('menu-travels').click()
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles(join(homefsDir, 'public/static/carbon/travelsTemplate.tsv'))
  // Actually there's no more loading overlay displayed during the parsing so
  // wait a bit and cross your finger
  await page.waitForTimeout(3000)
  await page.getByTestId('travels-save').click()
  await noMoreLoading({ page })
}

export async function commuteAnswer ({
  page,
  isAuthenticated,
  positionLabel = null
}) {
  if (positionLabel !== null) {
    // otherwise the user isn't asked for
    await page.getByText(positionLabel).click()
    await page.getByTestId('next-step-survey').click()
  }
  if (isAuthenticated) {
    // is authenticated
    await page.getByPlaceholder('Ajouter').click()
    await page.getByRole('button', { name: 'Bat. 2' }).click()
    // click somewhere else to close the selection to uncover the next button
    await page.getByText(/.*_OF_l1p5-test-0@l1p5.org/).click()
    await page.keyboard.press('Escape', { delay: 50 })
    await page.getByTestId('next-step-survey').click()
  }
  await page.getByRole('combobox').selectOption('3')
  await page.getByTestId('next-step-survey').click()
  await page.getByRole('button', { name: 'Train' }).click()
  await page.getByTestId('next-step-commutes').click()
  await page.getByPlaceholder('0').fill('100')
  await page.getByTestId('next-step-commutes').click()
  await page.getByTestId('next-step-commutes').click()
}

export async function foodAnswer ({
  page,
  isAuthenticated,
  positionLabel = null
}) {
  if (!isAuthenticated) {
    // is unauthenticated
    await page.locator('li').filter({ hasText: 'Alimentation' }).click()
    await page.getByTestId('food-add').click()
    if (positionLabel !== null) {
      await page.getByText(positionLabel).click()
      await page.getByTestId('next-step-survey').click()
    }
    await page.getByRole('combobox').selectOption('3')
    await page.getByTestId('next-step-survey').click()
  } else {
    // is authenticated
    await page.getByRole('button', { name: 'Suivant ' }).click()
  }
  await page.getByRole('button', { name: 'Je mange peu de viande' }).click()
  await page.getByTestId('next-step-food').click()
  await page.getByRole('combobox').selectOption('3')
  await page.getByTestId('next-step-food').click()
}

export async function addCommutesAndFoodsAnonymous ({ page, positionLabel }) {
  await page.locator('a').filter({ hasText: 'Dpts domicile / travail' }).click()
  await page.getByTestId('commutes-add').click()
  await commuteAnswer({ page, isAuthenticated: false, positionLabel })
  await page.getByTestId('commutes-save').click()
  await noMoreLoading({ page })
  await foodAnswer({ page, isAuthenticated: false, positionLabel })
  await page.getByTestId('food-save').click()
  await noMoreLoading({ page })
}

export async function addCommutesAndFoodsSurvey ({ page, positionLabel }) {
  await page.locator('a').filter({ hasText: 'Dpts domicile / travail' }).click()
  await page.getByTestId('commutes').first().click()
  await page.getByTestId('foods').first().click()
  // add labels to the survey
  await page.getByTestId('survey-settings').first().click()
  await page.getByPlaceholder('Ajouter').click()
  await page.getByRole('button', { name: 'Bat. 1' }).click()
  await page.getByRole('button', { name: 'Bat. 2' }).click()
  await page.getByRole('button', { name: 'Bat. 3' }).click()
  // escape drop down menu to avoid save issue
  await page.locator('header').click()
  await page.getByRole('button', { name: ' Sauvegarder' }).first().click()
  const surveyUrl = await page.getByTestId('survey-link').first().inputValue()
  await logOut({ page })
  await page.goto(surveyUrl)
  await page.getByRole('button', { name: 'Commencer ' }).click()
  await commuteAnswer({ page, isAuthenticated: true, positionLabel })
  await foodAnswer({ page, isAuthenticated: true, positionLabel })
  await page.getByText('Soumettre').click()
  await logIn({ page })
  await page.getByTestId('loadGHGI(2022)').first().click()
  await page
    .locator('li')
    .filter({ hasText: 'Dpts domicile / travail' })
    .click()
  // close the survey
  await page.getByTestId('commutes').first().click()
  // too fast ?
  await page.getByTestId('foods').first().click()
}

// clone Survey doesn't make difference between commutes and foods
// Doesn't matter if you clone foods or commutes, each of them will be cloned
export async function cloneSurvey ({ page, isCommute }) {
  if (isCommute) {
    await page
      .locator('li')
      .filter({ hasText: 'Dpts domicile / travail' })
      .click()
    await page.getByTestId('dropdown-menu').first().click()
    await page.locator('a').filter({ hasText: 'Cloner' }).first().click()
  } else {
    await page.locator('li').filter({ hasText: 'Alimentation' }).first().click()
    await page.getByTestId('dropdown-menu').nth(1).click()
    await page.locator('a').filter({ hasText: 'Cloner' }).nth(1).click()
  }

  await page.getByTestId('clone-survey').click()
}

export async function addTags ({ page }) {
  await page.getByText('Mes labels').click()
  await page.getByRole('button', { name: ' Ajouter' }).click()
  await page.locator('input[name="name"]').fill('Bâtiment')
  await page
    .getByPlaceholder('Description (optionel)')
    .fill('Liste des bâtiments sur le site')
  await page.locator('input[name="tags"]').fill('Bat. 1')
  await page.getByTestId('add-tag').click()
  await page.locator('input[name="tags"]').fill('Bat. 2')
  await page.getByTestId('add-tag').click()
  await page.locator('input[name="tags"]').fill('Bat. 3')
  await page.getByTestId('add-tag').click()
  await page.getByTestId('submit-tags').click()
  await noMoreLoading({ page })
}

export async function addResearchActivities ({ page, isAuthenticated }) {
  await page.locator('li').filter({ hasText: 'Activités de recherche' }).click()
  // CERN
  await page.getByTestId('new-ractivity').click()
  await page.getByRole('combobox').selectOption('with.lhc')
  await page.getByPlaceholder('Quantité').fill('2')
  if (isAuthenticated) {
    await page.getByPlaceholder('Ajouter').click()
    await page.getByRole('button', { name: 'Bat. 1' }).click()
    await page.locator('header').click()
  }
  await page.getByTestId('add-ractivity').click()
  // GENCI computing
  await page.getByTestId('new-ractivity').click()
  await page.locator('a').filter({ hasText: 'Calcul GENCI' }).click()
  await page.getByRole('combobox').selectOption('occigen.cpu')
  await page.getByPlaceholder('Quantité').fill('2')
  if (isAuthenticated) {
    await page.getByPlaceholder('Ajouter').click()
    await page.getByRole('button', { name: 'Bat. 2' }).click()
    await page.locator('header').click()
  }
  await page.getByTestId('add-ractivity').click()
  // Astronomy
  await page.getByTestId('new-ractivity').click()
  await page.locator('a').filter({ hasText: 'Astronomie' }).click()
  await page.getByPlaceholder('Nom du laboratoire').click()
  await page
    .getByRole('button', { name: 'GEPI Observatoire de Paris,' })
    .click()
  if (isAuthenticated) {
    await page.getByPlaceholder('Ajouter').click()
    await page.getByRole('button', { name: 'Bat. 3' }).click()
    await page.locator('header').click()
  }
  await page.getByTestId('add-ractivity').click()
  // Fertilisers
  await page.getByTestId('new-ractivity').click()
  await page.getByText('Activités agricoles').click()
  await page.getByRole('combobox').selectOption('minerals')
  await page.getByPlaceholder('Quantité').fill('200')
  if (isAuthenticated) {
    await page.getByPlaceholder('Ajouter').click()
    await page.getByRole('button', { name: 'Bat. 1' }).click()
    await page.locator('header').click()
  }
  await page.getByTestId('add-ractivity').click()
  // Livestock
  await page.getByTestId('new-ractivity').click()
  await page.locator('a').filter({ hasText: 'Activités agricoles' }).click()
  await page.locator('a').filter({ hasText: 'Cheptel' }).click()
  await page.getByRole('combobox').selectOption('beef')
  await page.getByPlaceholder('Nombre').fill('100')
  if (isAuthenticated) {
    await page.getByPlaceholder('Ajouter').click()
    await page.getByRole('button', { name: 'Bat. 2' }).click()
    await page.locator('header').click()
  }
  await page.getByTestId('add-ractivity').click()
  await page.getByTestId('save-ractivities').click()
  await noMoreLoading({ page })
}

export async function GHGIPuclicLink ({ page }) {
  await page.goto('http://localhost:8080/administration/all-ghgi')
  await page.getByTestId('menu-ghgis').click()
  const [publicGHGI] = await Promise.all([
    page.waitForEvent('popup'),
    page.getByTestId('loadPublicGHGI(2022)').click()
  ])
  const publicGHGIUrl = publicGHGI.url()
  await logOut({ page })
  await page.goto(publicGHGIUrl)
}
