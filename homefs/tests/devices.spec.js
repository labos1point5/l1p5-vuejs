import { test, expect } from '@playwright/test'

import { anonymousConnection } from './utils'

test('upload devices template file', async ({ page }) => {
  await anonymousConnection({ page })
  await page.locator('a').filter({ hasText: 'Matériel informatique' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page.getByTestId('devices-upload').setInputFiles('public/static/carbon/devicesTemplate.tsv')
  // Accept the warning due to the number of screens
  await page.getByRole('button', { name: 'OK' }).click()
  await page.getByRole('button', { name: 'Valider' }).click()
  await expect(page.getByTestId('devices-table')).toHaveScreenshot('table-snapshot.png')
})
