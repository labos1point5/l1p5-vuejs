import random

from django.core.management.base import BaseCommand, CommandError

from backend.carbon.models import GHGI, CommuteSection, SurveyAnswer

from backend.test_utils import ensure_lab_created, with_test_user

L1P5_GHGI_YEAR = "2042"
NUMBER_BY_CAT = 2

# make the randomness deterministic
random.seed(42)


@with_test_user
def populate(u, email, password):
    # make it a referent for a laboratory
    l = ensure_lab_created(u)

    # create a ghgi
    g = GHGI(
        laboratory=l,
        year=2042,
        nResearcher=1,
        nProfessor=1,
        nEngineer=1,
        nStudent=1,
        budget=int(10e6),
    )
    g.save()

    km = 20
    nWorkingDay = 1
    for status in ["researcher", "researcher", "engineer", "student"]:
        s = SurveyAnswer(ghgi=g, position=status, nWorkingDay=nWorkingDay)
        s.save()
        cs = CommuteSection(survey=s, mode="car", engine="diesel", distance=km)
        cs.save()

    # create a ghgi
    g = GHGI(
        laboratory=l,
        year=2043,
        nResearcher=1,
        nProfessor=1,
        nEngineer=1,
        nStudent=1,
        budget=int(10e6),
    )
    g.save()

    km = 20
    nWorkingDay = 2
    for status in ["researcher", "researcher", "engineer", "student"]:
        s = SurveyAnswer(
            ghgi=g,
            nWorkingDay=nWorkingDay,
            position=status,
            nWorkingDay2=nWorkingDay / 2,
        )
        s.save()
        cs1 = CommuteSection(
            survey=s,
            mode="car",
            distance=km,
            isDay2=False,
            pooling=False,
            engine="diesel",
        )
        cs1.save()
        cs2 = CommuteSection(
            survey=s, mode="bus", distance=10, isDay2=True, pooling=False
        )
        cs2.save()

    # create a ghgi
    g = GHGI(
        laboratory=l,
        year=2044,
        nResearcher=2,
        nProfessor=2,
        nEngineer=2,
        nStudent=2,
        budget=int(10e6),
    )
    g.save()

    for status in ["researcher", "researcher", "engineer", "student"]:
        s = SurveyAnswer(ghgi=g, nWorkingDay=random.randint(1, 5), position=status)
        cs = CommuteSection(
            survey=s,
            mode="car",
            engine="diesel",
            distance=random.randint(1, 30),
            isDay2=False,
        )
        s.save()
        cs.save()


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):
        populate()
