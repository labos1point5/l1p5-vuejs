import json
from pathlib import Path

from django.contrib.contenttypes.models import ContentType

from backend.carbon.models import (
    GHGI,
    CommuteSection,
    Meal,
    SurveyAnswer,
)
from backend.users.models import L1P5User
from backend.utils import get_entity_type
from backend.core.models.core import Boundary, Members, PositionTitle, Settings
from ..utils import (
    do_save_buildings,
    do_save_computer_devices,
    do_save_purchases,
    do_save_travels,
    do_save_vehicles,
    remove_keys,
)


HERE = Path(__file__).parent.resolve()


def _make_fake_commutes(ghgi, commutes):
    for c in commutes:
        sections = c.pop("sections", [])
        sa = SurveyAnswer.objects.create(
            ghgi=ghgi, **remove_keys(c, ["id", "ghgi", "tags", "meals"])
        )
        sections_objs = [
            CommuteSection(survey=sa, **remove_keys(s, ["id", "survey"]))
            for s in sections
        ]
        CommuteSection.objects.bulk_create(sections_objs)


def _make_fake_foods(ghgi, foods):
    for food in foods:
        meals = food.pop("meals")
        sa = SurveyAnswer.objects.create(
            ghgi=ghgi, **remove_keys(food, ["id", "ghgi", "tags", "sections"])
        )
        sections_objs = [
            Meal(survey=sa, **remove_keys(s, ["id", "survey"])) for s in meals
        ]
        Meal.objects.bulk_create(sections_objs)


def find_good_enough_label_mapping(labels1, labels2):
    """dummy heuristic to build a mapping of labels based on bast matches."""
    from difflib import SequenceMatcher
    from operator import itemgetter

    ratios = [
        (SequenceMatcher(a=a, b=b).ratio(), a, b) for a in labels1 for b in labels2
    ]
    ratios = sorted(ratios, reverse=True, key=itemgetter(0))
    # build the mapping stop when all the label of labels1 have been covered
    mapping = dict()
    for _, a, b in ratios:
        if a not in mapping.keys():
            mapping[a] = b

    return mapping


def update_travel_labels(travels):
    """/!\ in place modification."""
    target_labels = Settings.get_value(name="TRAVEL_POSITION_LABELS")
    source_labels = set([t["status"] for t in travels])
    mapping_labels = find_good_enough_label_mapping(source_labels, target_labels)

    target_purposes = Settings.get_value(name="TRAVEL_PURPOSE_LABELS")
    source_purposes = set([t["purpose"] for t in travels])
    mapping_purposes = find_good_enough_label_mapping(source_purposes, target_purposes)

    for t in travels:
        if t["status"] in mapping_labels:
            t["status"] = mapping_labels[t["status"]]
        else:
            t["status"] = target_labels[0]
        if t["purpose"] in mapping_purposes:
            t["purpose"] = mapping_purposes[t["purpose"]]
        else:
            t["purpose"] = target_purposes[0]

    return travels


def update_survey_labels(surveys):
    """/!\ in place modification."""
    target_labels = Settings.get_value(name="CF_LABEL_MAPPING").values()
    source_labels = set([t["position"] for t in surveys])
    mapping_labels = find_good_enough_label_mapping(source_labels, target_labels)

    for s in surveys:
        if s["position"] in mapping_labels:
            s["position"] = mapping_labels[s["position"]]
        else:
            s["position"] = target_labels[0]
    return surveys


def get_or_set_fakeGHGI(force=False):
    import random

    random.seed(42)
    fake_email = "fake-user@fake-lab.fr"
    fake_entity_name = "Lab example"
    fake_latitude = 42.0
    fake_longitude = 42.0
    fake_ghgi_year = 2019
    entity_klass = get_entity_type()

    if force:
        # will remove every thing
        L1P5User.objects.filter(email=fake_email).delete()
    fake_user, _ = L1P5User.objects.get_or_create(email=fake_email)
    klass = get_entity_type()
    entity, _ = klass.objects.get_or_create(
        name=fake_entity_name,
        referent=fake_user,
        defaults=dict(latitude=fake_latitude, longitude=fake_longitude),
    )

    # implement get or create for the fake ghgi
    ghgi = GHGI.objects.filter(
        content_type=ContentType.objects.get_for_model(entity_klass),
        object_id=entity.id,
    ).first()
    if not ghgi:
        # remove any existing states for the fake user
        # create the boundary
        b = Boundary(budget=random.randint(1e6, 2e6))
        b.save()

        for position in PositionTitle.objects.all():
            Members.objects.create(
                boundary=b, position=position, number=random.randint(30, 50)
            )

        # create the ghgi
        ghgi = GHGI.objects.create(entity=entity, year=fake_ghgi_year, boundary=b)

        # add the activity based on the reference GHGI and adapt them when
        # necessary to new labels as configured in backend/settings/l1p5

        ghgi_data = json.loads((Path(__file__).parent / "fakeGHGI.json").read_bytes())
        do_save_buildings(entity, ghgi, ghgi_data["buildings"])
        do_save_purchases(ghgi, ghgi_data["purchases"])
        do_save_computer_devices(ghgi, ghgi_data["devices"])
        do_save_vehicles(entity, ghgi, ghgi_data["vehicles"])
        do_save_travels(ghgi, update_travel_labels(ghgi_data["travels"]))
        # we can't factorize yet for the two following activities
        # as data are saved individually (through the survey)
        _make_fake_commutes(ghgi, update_survey_labels(ghgi_data["commutes"]))
        _make_fake_foods(ghgi, update_survey_labels(ghgi_data["foods"]))
        return ghgi
    else:
        return ghgi
