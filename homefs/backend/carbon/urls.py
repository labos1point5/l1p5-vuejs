"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.urls import path
from . import views as carbonviews

URL_PATTERNS = [
    path("api/fakeGHGI/", carbonviews.get_fakeGHGI, name="fakeGHGI"),
    path("api/get_counts/", carbonviews.get_counts, name="get_counts"),
    path("api/save_ghgi/", carbonviews.save_ghgi, name="save_ghgi"),
    path("api/delete_ghgi/", carbonviews.delete_ghgi, name="delete_ghgi"),
    path("api/get_all_ghgi/", carbonviews.get_all_ghgi, name="get_all_ghgi"),
    path(
        "api/get_all_ghgi_without_consumptions/",
        carbonviews.get_all_ghgi_without_consumptions,
        name="get_all_ghgi",
    ),
    path(
        "api/get_all_ghgi_admin/",
        carbonviews.get_all_ghgi_admin,
        name="get_all_ghgi_admin",
    ),
    path(
        "api/get_ghgis_consumptions/",
        carbonviews.get_ghgis_consumptions,
        name="get_ghgis_consumptions",
    ),
    path(
        "api/get_ghgi_consumptions/",
        carbonviews.get_ghgi_consumptions,
        name="get_ghgi_consumptions",
    ),
    path(
        "api/get_ghgi_consumptions_by_uuid/",
        carbonviews.get_ghgi_consumptions_by_uuid,
        name="get_ghgi_consumptions_by_uuid",
    ),
    path("api/save_vehicles/", carbonviews.save_vehicles, name="save_vehicles"),
    path(
        "api/get_all_vehicles/", carbonviews.get_all_vehicles, name="get_all_vehicles"
    ),
    path("api/save_buildings/", carbonviews.save_buildings, name="save_buildings"),
    path(
        "api/get_all_buildings/",
        carbonviews.get_all_buildings,
        name="get_all_buildings",
    ),
    path("api/save_commutes/", carbonviews.save_commutes, name="save_commutes"),
    path("api/save_foods/", carbonviews.save_foods, name="save_foods"),
    path("api/save_survey/", carbonviews.save_survey, name="save_survey"),
    path("api/save_travels/", carbonviews.save_travels, name="save_travels"),
    path(
        "api/save_computer_devices/",
        carbonviews.save_computer_devices,
        name="save_computer_devices",
    ),
    path("api/save_purchases/", carbonviews.save_purchases, name="save_purchases"),
    path(
        "api/save_research_activities/",
        carbonviews.save_research_activities,
        name="save_research_activities",
    ),
    path(
        "api/get_astro_alphas/", carbonviews.get_astro_alphas, name="get_astro_alphas"
    ),
    path("api/get_astro_years/", carbonviews.get_astro_years, name="get_astro_years"),
    path(
        "api/update_submitted/", carbonviews.update_submitted, name="update_submitted"
    ),
    path(
        "api/update_ghgi_synthesis/",
        carbonviews.update_synthesis,
        name="update_ghgi_synthesis",
    ),
    path(
        "api/get_ghgi_survey_info/",
        carbonviews.get_ghgi_survey_info,
        name="get_ghgi_survey_info",
    ),
    path(
        "api/update_survey_active/",
        carbonviews.update_survey_active,
        name="update_survey_active",
    ),
    path(
        "api/save_survey_config/",
        carbonviews.save_survey_config,
        name="save_survey_config",
    ),
    path("api/clone_survey/", carbonviews.clone_survey, name="clone_survey"),
    path("api/copy_data/", carbonviews.copy_data, name="copy_data"),
]
