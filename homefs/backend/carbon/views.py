import copy
import json
import uuid
from typing import Optional

from django.core.exceptions import ValidationError, PermissionDenied
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django.http import Http404

from django.conf import settings
import jsonschema
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from backend.carbon.utils import (
    do_save_buildings,
    do_save_computer_devices,
    do_save_purchases,
    do_save_travels,
    do_save_vehicles,
    get_ghgi_consumption_data,
    serialize_ghgi_with_consumptions,
)
from backend.carbon.fake import get_or_set_fakeGHGI

from ..utils import entity_filter, entity_get

from .models import (
    GHGI,
    Building,
    Electricity,
    Water,
    Heating,
    Refrigerant,
    SelfConsumption,
    Vehicle,
    VehicleConsumption,
    SurveyAnswer,
    CommuteSection,
    Meal,
    Travel,
    TravelSection,
    TravelNames,
    ComputerDevice,
    Purchase,
    ResearchActivity,
)
from .serializers import (
    GHGISerializer,
    BuildingWCSerializer,
    VehicleWCSerializer,
    CommuteSerializer,
    FoodSerializer,
    TravelSerializer,
    ComputerDeviceSerializer,
    PurchaseSerializer,
    ResearchActivitySerializer,
    VehicleConsumptionSerializer,
    ElectricitySerializer,
    RefrigerantSerializer,
    SelfConsumptionSerializer,
    WaterSerializer,
    HeatingSerializer,
)
from .factory import ghgi_filter

from ..core.models import (
    Tag,
    TagCategory,
    TaggedItem,
    boundary_update_or_create,
)
from ..scenario.factory import scenario_filter
from ..users.models import L1P5User

from ..core.serializers import TagCategorySerializer


def owns_ghgi(
    data_key: Optional[str] = "id",
    model_key: Optional[str] = "id",
    allow_super_user: Optional[bool] = False,
):
    """Decorates function to ensure the user owns the ghgi

    Args:

        data_key: where to look in the request.data to find
            the ghgi identifier (e.g id, ghgi_id, uuid)
        model_key: mapping of data_key in the GHGI model (e.g id, uuid)
    """

    def _owns_ghgi(f):
        def wrapped(request, *args, **kwargs):
            if request.method == "GET":
                identifier = int(request.query_params.get(data_key))
            else:
                identifier = request.data[data_key]
            kwds = {model_key: identifier}
            ghgi = get_object_or_404(GHGI, **kwds)
            if ghgi.owner != request.user:
                if allow_super_user:
                    if not L1P5User.objects.get(email=request.user).is_superuser:
                        raise PermissionDenied()
                else:
                    raise PermissionDenied()
            return f(request, ghgi, *args, **kwargs)

        return wrapped

    return _owns_ghgi


def validate_synthesis(synthesis):
    try:
        jsonschema.validate(synthesis, GHGI.SYNTHESIS_SCHEMA)
    except jsonschema.ValidationError as e:
        # level-up the exception
        # in views this will end up with a status code of 400 (Bad Request)
        raise ValidationError(e.message)


def validate_submitted(submitted):
    try:
        jsonschema.validate(submitted, GHGI.SUBMITTED_SCHEMA)
    except jsonschema.ValidationError as e:
        # level-up the exception
        # in views this will end up with a status code of 400 (Bad Request)
        raise ValidationError(e.message)


def validate_uuid(cuuid):
    try:
        uuid.UUID(cuuid)
        return True
    except ValueError:
        return False


from pathlib import Path

HERE = Path(__file__).parent


@api_view(["GET"])
@permission_classes([])
def get_fakeGHGI(request):
    """Get (or generate) a fake GHGI (e.g: for scenario ...).

    Once generated it's stored and reused for subsequent calls
    """
    g = get_or_set_fakeGHGI(force=False)
    r = serialize_ghgi_with_consumptions(g)
    return Response(r, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([])
def get_counts(request):
    from ..transition.utils import get_counts as transition_get_counts

    response = Response(
        {
            "transition": transition_get_counts(),
            "nGHGI": ghgi_filter().count(),
            "nScenario": scenario_filter().count(),
            "nEntity": entity_filter(ghgis__isnull=False).distinct().count(),
        },
        status=status.HTTP_200_OK,
    )
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
    return response


@api_view(["POST"])
def save_ghgi(request):
    data = request.data.copy()
    ghgi_id = data.pop("id")
    boundary_obj = data.pop("boundary", None)
    if boundary_obj is None and ghgi_id is None:
        raise ValidationError("To create a ghgi the boundary must be set")

    # ghgi_id = None means that we're creating a new GHGI
    validate_submitted(data["submitted"])
    if ghgi_id is not None:
        # we're updating one ghgi
        # we check that the user is the owner
        ghgi = get_object_or_404(GHGI, id=ghgi_id)
        if ghgi.owner != request.user:
            raise PermissionDenied()

    # first create or update boundary (if any)
    b = None
    if boundary_obj is not None:
        b = boundary_update_or_create(boundary_obj)
        data["boundary"] = b

    entity = entity_get(referent=request.user)

    if entity is None:
        raise Http404("Entity does not exist")

    data["entity"] = entity

    ghgi, _ = GHGI.objects.update_or_create(id=ghgi_id, defaults=data)
    serializer = GHGISerializer(ghgi)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id", allow_super_user=True)
def delete_ghgi(request, ghgi):
    ghgi.delete()
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
def get_all_ghgi(request):
    if request.user.is_authenticated:
        ghgi_to_return = []
        entity = entity_get(referent_id=request.user.id)
        all_ghgi = GHGI.objects.filter(
            object_id=entity.id,
            content_type=ContentType.objects.get_for_model(entity).id,
        ).order_by("-year")
        for ghgi in all_ghgi:
            all_data = get_ghgi_consumption_data(ghgi.id)
            serializer = GHGISerializer(ghgi)
            final_ghgi = copy.deepcopy(serializer.data)
            for module in all_data.keys():
                final_ghgi[module] = copy.deepcopy(all_data[module])
            ghgi_to_return.append(final_ghgi)
        return Response(ghgi_to_return, status=status.HTTP_200_OK)
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_all_ghgi_without_consumptions(request):
    """Get the GHGIs list but without their associated consumptions

    Returns:
        - None if no user isn't authenticated
            This is peculiar choice but for now this allow to use the
            application in an anonymous way
        - The list of all the GHGIs otherwise
    """
    entity = entity_get(referent_id=request.user.id)
    all_ghgi = []
    if entity is not None:
        all_ghgi = GHGI.objects.filter(
            object_id=entity.id,
            content_type=ContentType.objects.get_for_model(entity).id,
        ).order_by("-year")
    serializer = GHGISerializer(all_ghgi, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
def get_ghgis_consumptions(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            module = request.data["module"]
            ghgi_to_return = []
            all_ghgi = GHGI.objects.filter(id__in=request.data["ghgis"])
            for ghgi in all_ghgi:
                all_data = get_ghgi_consumption_data(ghgi.id, module)
                serializer = GHGISerializer(ghgi)
                final_ghgi = copy.deepcopy(serializer.data)
                final_ghgi[module] = copy.deepcopy(all_data[module])
                ghgi_to_return.append(final_ghgi)
            return Response(ghgi_to_return, status=status.HTTP_200_OK)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
def get_all_ghgi_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            if "years" in request.data.keys():
                years = request.data.pop("years")
                all_ghgi = ghgi_filter(year__in=years)
            else:
                all_ghgi = ghgi_filter()
            serializer = GHGISerializer(all_ghgi, many=True)
            return Response(
                {
                    "ghgis": serializer.data,
                    "years": sorted(
                        list(
                            map(
                                lambda obj: obj["year"],
                                GHGI.objects.values("year").distinct(),
                            )
                        ),
                        reverse=True,
                    ),
                },
                status=status.HTTP_200_OK,
            )
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
def update_synthesis(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            validate_synthesis(request.data["synthesis"])
            ghgi_id = request.data.pop("ghgi_id")
            # save synthesis
            GHGI.objects.filter(id=ghgi_id).update(synthesis=request.data["synthesis"])
            return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_vehicles(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])
    entity = entity_get(referent_id=request.user.id)
    do_save_vehicles(entity, ghgi, data["vehicles"])
    # save vehicles intensity
    GHGI.objects.filter(id=ghgi.id).update(synthesis=data["synthesis"])
    return Response(
        get_ghgi_consumption_data(ghgi.id, "vehicles")["vehicles"],
        status=status.HTTP_201_CREATED,
    )


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_computer_devices(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])
    do_save_computer_devices(ghgi, data["devices"])
    # save devices intensity
    GHGI.objects.filter(id=ghgi.id).update(synthesis=request.data["synthesis"])

    computer_devices = ComputerDevice.objects.filter(ghgi_id=ghgi.id)
    serializer = ComputerDeviceSerializer(computer_devices, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_purchases(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])

    do_save_purchases(ghgi, data["purchases"])

    # save purchases intensity
    GHGI.objects.filter(id=ghgi.id).update(synthesis=data["synthesis"])

    purchases = Purchase.objects.filter(ghgi_id=ghgi.id)
    serializer = PurchaseSerializer(purchases, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_research_activities(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])

    ResearchActivity.objects.filter(ghgi_id=ghgi.id).delete()
    # add new
    for ractivity_data in data["ractivities"]:
        tags = ractivity_data.pop("tags")
        ractivity_data["ghgi_id"] = ghgi.id
        ractivity = ResearchActivity.objects.create(**ractivity_data)
        for tag in tags:
            t = Tag.objects.get(id=tag["id"])
            TaggedItem.objects.create(content_object=ractivity, tag=t)

    # save intensity
    GHGI.objects.filter(id=ghgi.id).update(synthesis=request.data["synthesis"])

    ractivities = ResearchActivity.objects.filter(ghgi_id=ghgi.id)
    serializer = ResearchActivitySerializer(ractivities, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET"])
def get_all_vehicles(request):
    try:
        entity = entity_get(referent_id=request.user.id)
        vehicles = Vehicle.objects.filter(
            object_id=entity.id,
            content_type=ContentType.objects.get_for_model(entity).id,
        )
        serializer = VehicleWCSerializer(vehicles, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_buildings(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])
    entity = entity_get(referent_id=request.user.id)
    buildings = data["buildings"]
    do_save_buildings(entity, ghgi, buildings)
    GHGI.objects.filter(id=ghgi.id).update(synthesis=data["synthesis"])
    return Response(
        get_ghgi_consumption_data(ghgi.id, "buildings")["buildings"],
        status=status.HTTP_201_CREATED,
    )


@api_view(["GET"])
def get_all_buildings(request):
    try:
        entity = entity_get(referent_id=request.user.id)
        buildings = Building.objects.filter(
            object_id=entity.id,
            content_type=ContentType.objects.get_for_model(entity).id,
        )
        serializer = BuildingWCSerializer(buildings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
@permission_classes([])
def get_astro_alphas(request):
    import csv
    import os

    year = request.query_params.get("year")
    labID = request.query_params.get("labID")
    filename = os.path.join(
        settings.BASE_DIR, "data/researchActivities/astroAlpha_" + year + ".csv"
    )
    alphas = None
    with open(filename, newline="") as csvfile:
        for row in csv.DictReader(csvfile):
            lab_id = row.pop("lab_id")
            if lab_id == labID:
                alphas = row
    return Response(alphas, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([])
def get_astro_years(request):
    import os
    import re

    availableYears = []
    for file in os.listdir(os.path.join(settings.BASE_DIR, "data/researchActivities")):
        if file.startswith("astroAlpha"):
            availableYears.append(int(re.match("astroAlpha_(\d+).csv", file).group(1)))
    return Response(availableYears, status=status.HTTP_200_OK)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_commutes(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])
    for commute_data in request.data["commutes"]:
        # update cdeleted status
        SurveyAnswer.objects.filter(id=commute_data["id"]).update(
            cdeleted=commute_data["cdeleted"]
        )
    # update intensity
    GHGI.objects.filter(id=ghgi.id).update(synthesis=data["synthesis"])
    commutes = SurveyAnswer.objects.filter(ghgi_id=ghgi.id)
    serializer = CommuteSerializer(commutes, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_foods(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])
    for food_data in request.data["foods"]:
        # update mdeleted status
        SurveyAnswer.objects.filter(id=food_data["id"]).update(
            mdeleted=food_data["mdeleted"]
        )
    # update intensity
    GHGI.objects.filter(id=ghgi.id).update(synthesis=data["synthesis"])
    foods = SurveyAnswer.objects.filter(ghgi_id=ghgi.id)
    serializer = FoodSerializer(foods, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET"])
@permission_classes([])
def get_ghgi_survey_info(request):
    uuid = request.query_params.get("uuid")
    if validate_uuid(uuid):
        ghgi = GHGI.objects.get(uuid=uuid)
        allTagCategories = ghgi.entity.tag_categories
        tagCategoriesSerializer = TagCategorySerializer(allTagCategories, many=True)
        surveyTags = list(map(lambda x: x.id, ghgi.surveyTags.all()))
        for category in tagCategoriesSerializer.data:
            ftags = [t for t in category["tags"] if t["id"] in surveyTags]
            category["tags"] = ftags
        # clean categories without tags
        categories = [c for c in tagCategoriesSerializer.data if len(c["tags"]) > 0]
        return Response(
            {
                "year": ghgi.year,
                "surveyMessage": ghgi.surveyMessage,
                "commutesActive": ghgi.commutesActive,
                "foodsActive": ghgi.foodsActive,
                "contactEmail": ghgi.entity.referent.email,
                "entityName": ghgi.entity.name,
                "surveyTagCategories": categories,
                "citySize": ghgi.entity.citySize,
            },
            status=status.HTTP_200_OK,
        )
    else:
        return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def clone_survey(request, ghgi):
    data = request.data.copy()
    surveyCloneYear = data.pop("surveyCloneYear")
    GHGI.objects.filter(id=ghgi.id).update(surveyCloneYear=surveyCloneYear)
    to_return = get_ghgi_consumption_data(ghgi.id, "commutes")["commutes"]
    return Response(to_return, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_survey_config(request, ghgi):
    data = request.data.copy()
    surveyMessage = data.pop("surveyMessage")
    surveyTags = data.pop("surveyTags")
    ghgi = GHGI.objects.get(id=ghgi.id)
    ghgi.surveyMessage = surveyMessage
    ghgi.surveyTags.clear()
    for tag in surveyTags:
        t = Tag.objects.get(id=tag["id"])
        ghgi.surveyTags.add(t)
    ghgi.save()
    return Response({}, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@permission_classes([])
def save_survey(request):
    uuid = request.data.pop("uuid")
    survey_data = request.data.pop("survey")
    sections_data = survey_data.pop("sections")
    meals_data = survey_data.pop("meals")
    tags = survey_data.pop("tags")
    ghgi = GHGI.objects.get(uuid=uuid)
    # add the new survey result

    # FIXME(msimonin): validate position
    survey = SurveyAnswer.objects.create(ghgi_id=ghgi.id, **survey_data)
    SurveyAnswer.objects.filter(id=survey.id).update(seqID=survey.id)
    for section_data in sections_data:
        CommuteSection.objects.create(survey_id=survey.id, **section_data)
    for meal_data in meals_data:
        Meal.objects.create(survey_id=survey.id, **meal_data)
    # Add tags
    for tag in tags:
        t = Tag.objects.get(id=tag["id"])
        TaggedItem.objects.create(content_object=survey, tag=t)
    survey = SurveyAnswer.objects.get(id=survey.id)
    serializer = CommuteSerializer(survey)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_travels(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])

    data = request.data
    do_save_travels(ghgi, data["travels"])

    # save travels intensity
    GHGI.objects.filter(id=ghgi.id).update(synthesis=request.data["synthesis"])
    travels = Travel.objects.filter(ghgi_id=ghgi.id)
    serializer = TravelSerializer(travels, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id", allow_super_user=True)
def update_submitted(request, ghgi):
    validate_submitted(request.data["submitted"])
    GHGI.objects.filter(id=ghgi.id).update(submitted=request.data["submitted"])
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def update_survey_active(request, ghgi):
    validate_synthesis(request.data["synthesis"])
    GHGI.objects.filter(id=ghgi.id).update(
        commutesActive=request.data["commutesActive"],
        foodsActive=request.data["foodsActive"],
        synthesis=request.data["synthesis"],
    )
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
@owns_ghgi(data_key="ghgi_id", allow_super_user=True)
def get_ghgi_consumptions(request, ghgi):
    return Response(serialize_ghgi_with_consumptions(ghgi), status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([])
def get_ghgi_consumptions_by_uuid(request):
    uuid = request.query_params.get("uuid")
    try:
        ghgi = get_object_or_404(GHGI, uuid=uuid)
        return Response(
            serialize_ghgi_with_consumptions(ghgi), status=status.HTTP_200_OK
        )
    except:
        # Return a 404 anyway, so that the page not found is displayed to the
        # user
        return Response(None, status=status.HTTP_404_NOT_FOUND)


@api_view(["POST"])
def copy_data(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            from_ghgi = GHGI.objects.get(id=request.data["fromGHGI"])
            to_ghgi = GHGI.objects.get(id=request.data["toGHGI"])

            tagCategories = TagCategory.objects.filter(
                object_id=to_ghgi.entity.id,
                content_type=ContentType.objects.get_for_model(to_ghgi.entity).id,
            )
            tag = None
            for category in tagCategories:
                for t in category.tags.all():
                    if t.name == request.data["toTag"]:
                        tag = t

            classes = {
                "purchases": Purchase,
                "ractivities": ResearchActivity,
                "devices": ComputerDevice,
            }
            serializers = {
                "purchases": PurchaseSerializer,
                "ractivities": ResearchActivitySerializer,
                "devices": ComputerDeviceSerializer,
            }
            if request.data["module2copy"] in ["purchases", "ractivities", "devices"]:
                for item in classes[request.data["module2copy"]].objects.filter(
                    ghgi_id=from_ghgi.id
                ):
                    serializer = serializers[request.data["module2copy"]](
                        item, many=False
                    )
                    data = serializer.data
                    for key in ["id", "tags", "ghgi"]:
                        data.pop(key)
                    data["ghgi"] = to_ghgi
                    obj = classes[request.data["module2copy"]].objects.create(**data)
                    if tag is not None:
                        TaggedItem.objects.create(content_object=obj, tag=tag)

            elif request.data["module2copy"] in ["commutes", "foods"]:
                for survey in SurveyAnswer.objects.filter(ghgi_id=from_ghgi.id):
                    serializer = CommuteSerializer(survey, many=False)
                    data = serializer.data
                    sections_data = data.pop("sections")
                    meals_data = data.pop("meals")
                    for key in ["id", "tags", "ghgi", "seqID"]:
                        data.pop(key)
                    data["ghgi"] = to_ghgi
                    survey = SurveyAnswer.objects.create(**data)
                    SurveyAnswer.objects.filter(id=survey.id).update(seqID=survey.id)

                    if tag is not None:
                        TaggedItem.objects.create(content_object=survey, tag=tag)
                    for section in sections_data:
                        for key in ["id", "survey"]:
                            section.pop(key)
                        section["survey"] = survey
                        CommuteSection.objects.create(**section)
                    for meal in meals_data:
                        for key in ["id", "survey"]:
                            meal.pop(key)
                        meal["survey"] = survey
                        Meal.objects.create(**meal)

            elif request.data["module2copy"] == "travels":
                for item in Travel.objects.filter(ghgi_id=from_ghgi.id):
                    serializer = TravelSerializer(item, many=False)
                    data = serializer.data
                    section_data = data.pop("sections")
                    names_data = data.pop("names")
                    for key in ["id", "tags", "ghgi"]:
                        data.pop(key)
                    data["ghgi"] = to_ghgi
                    travel = Travel.objects.create(**data)
                    if tag is not None:
                        TaggedItem.objects.create(content_object=travel, tag=tag)
                    for name in names_data:
                        TravelNames.objects.create(name=name, travel=travel)
                    for section in section_data:
                        for key in ["id", "travel"]:
                            section.pop(key)
                        section["travel"] = travel
                        TravelSection.objects.create(**section)

            elif request.data["module2copy"] == "vehicles":
                for vehicle in Vehicle.objects.filter(
                    object_id=from_ghgi.entity.id,
                    content_type=ContentType.objects.get_for_model(from_ghgi.entity).id,
                ):
                    serializer = VehicleWCSerializer(vehicle, many=False)
                    consumption = VehicleConsumption.objects.filter(
                        ghgi_id=from_ghgi.id, vehicle_id=vehicle.id
                    ).first()

                    if consumption:
                        data = serializer.data
                        for key in ["id"]:
                            data.pop(key)
                        data["object_id"] = to_ghgi.object_id
                        data["content_type"] = to_ghgi.content_type
                        obj = Vehicle.objects.create(**data)
                        if tag is not None:
                            TaggedItem.objects.create(content_object=obj, tag=tag)
                        serializer = VehicleConsumptionSerializer(
                            consumption, many=False
                        )
                        data = serializer.data
                        for key in ["id", "ghgi", "vehicle"]:
                            data.pop(key)
                        data["ghgi"] = to_ghgi
                        data["vehicle"] = obj
                        VehicleConsumption.objects.create(**data)

            elif request.data["module2copy"] == "buildings":
                for building in Building.objects.filter(
                    object_id=from_ghgi.entity.id,
                    content_type=ContentType.objects.get_for_model(from_ghgi.entity).id,
                ):
                    serializer = BuildingWCSerializer(building, many=False)
                    data = serializer.data
                    for key in ["id", "object_id", "content_type"]:
                        data.pop(key)
                    data["object_id"] = to_ghgi.object_id
                    data["content_type"] = to_ghgi.content_type
                    obj = Building.objects.create(**data)
                    if tag is not None:
                        TaggedItem.objects.create(content_object=obj, tag=tag)

                    electricity = Electricity.objects.filter(
                        ghgi=from_ghgi, building_id=building.id
                    ).first()
                    if electricity:
                        serializer = ElectricitySerializer(electricity, many=False)
                        data = serializer.data
                        for key in ["id", "ghgi", "building"]:
                            data.pop(key)
                        data["ghgi"] = to_ghgi
                        data["building"] = obj
                        Electricity.objects.create(**data)

                    water = Water.objects.filter(
                        ghgi=from_ghgi, building_id=building.id
                    ).first()
                    if water:
                        serializer = WaterSerializer(water, many=False)
                        data = serializer.data
                        for key in ["id", "ghgi", "building"]:
                            data.pop(key)
                        data["ghgi"] = to_ghgi
                        data["building"] = obj
                        Water.objects.create(**data)

                    refrigerant = Refrigerant.objects.filter(
                        ghgi=from_ghgi, building_id=building.id
                    ).first()

                    if refrigerant:
                        serializer = RefrigerantSerializer(refrigerant, many=False)
                        data = serializer.data
                        for key in ["id", "ghgi", "building"]:
                            data.pop(key)
                        data["ghgi"] = to_ghgi
                        data["building"] = obj
                        Refrigerant.objects.create(**data)

                    selfconsumption = SelfConsumption.objects.filter(
                        ghgi=from_ghgi, building_id=building.id
                    ).first()
                    if selfconsumption:
                        serializer = SelfConsumptionSerializer(
                            selfconsumption, many=False
                        )
                        data = serializer.data
                        for key in ["id", "ghgi", "building"]:
                            data.pop(key)
                        data["ghgi"] = to_ghgi
                        data["building"] = obj
                        SelfConsumption.objects.create(**data)

                    heating = Heating.objects.filter(
                        ghgi=from_ghgi, building_id=building.id
                    ).first()
                    if heating:
                        serializer = HeatingSerializer(heating, many=False)
                        data = serializer.data
                        for key in ["id", "ghgi", "building"]:
                            data.pop(key)
                        data["ghgi"] = to_ghgi
                        data["building"] = obj
                        Heating.objects.create(**data)

            return Response(None, status=status.HTTP_204_NO_CONTENT)
