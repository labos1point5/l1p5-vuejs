import copy
import uuid
from typing import Optional

from django.core.exceptions import ValidationError, PermissionDenied
from django.db.models import Q, Count
from django.shortcuts import get_object_or_404
import jsonschema
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework import status

from .models import GHGI, Building, Electricity, Water, Heating, Refrigerant, SelfConsumption, Vehicle, VehicleConsumption, SurveyAnswer, CommuteSection, Meal, Travel, TravelSection, TravelNames, ComputerDevice, Purchase
from .serializers import GHGISerializer, BuildingSerializer, BuildingWCSerializer, VehicleSerializer, VehicleWCSerializer, CommuteSerializer, FoodSerializer, TravelSerializer, ComputerDeviceSerializer, PurchaseSerializer

from ..core.models import Laboratory
from ..scenario.models import Scenario
from ..users.models import L1P5User


def owns_ghgi(data_key: Optional[str]="id", model_key: Optional[str]="id", allow_super_user: Optional[bool]=False):
    """Decorates function to ensure the user owns the ghgi

    Args:

        data_key: where to look in the request.data to find
            the ghgi identifier (e.g id, ghgi_id, uuid)
        model_key: mapping of data_key in the GHGI model (e.g id, uuid)
    """
    def _owns_ghgi(f):
        def wrapped(request, *args, **kwargs):
            identifier = request.data[data_key]
            kwds = {model_key: identifier}
            ghgi = get_object_or_404(GHGI, **kwds)
            if (ghgi.owner != request.user):
                if allow_super_user :
                    if not L1P5User.objects.get(email=request.user).is_superuser:
                        raise PermissionDenied()
                else :
                    raise PermissionDenied()
            return f(request, ghgi, *args, **kwargs)
        return wrapped
    return _owns_ghgi

def validate_synthesis(synthesis):
    try:
        jsonschema.validate(synthesis, GHGI.SYNTHESIS_SCHEMA)
    except jsonschema.ValidationError as e:
        # level-up the exception
        # in views this will end up with a status code of 400 (Bad Request)
        raise ValidationError(e.message)

def validate_submitted(submitted):
    try:
        jsonschema.validate(submitted, GHGI.SUBMITTED_SCHEMA)
    except jsonschema.ValidationError as e:
        # level-up the exception
        # in views this will end up with a status code of 400 (Bad Request)
        raise ValidationError(e.message)

def validate_uuid(cuuid):
    try:
        uuid.UUID(cuuid)
        return True
    except ValueError:
        return False

@api_view(['GET'])
@permission_classes([])
def get_counts(request):
    from ..transition.models import PublicAction, PUBLISHED
    response =  Response({
        'transition': dict(
            nActions=PublicAction.objects.filter(admin_status=PUBLISHED).count(),
            nLaboratories=Laboratory.objects.select_related("actions").filter(actions__admin_status=PUBLISHED).distinct().count()
        ),
        'nGHGI': GHGI.objects.count(),
        'nScenario': Scenario.objects.count(),
        'nLaboratory': Laboratory.objects.filter(bges__isnull=False).distinct().count()
    },
    status=status.HTTP_200_OK)
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
    return response

@api_view(['POST'])
def save_ghgi(request):
    data = request.data.copy()
    ghgi_id = data.pop("id")
    # ghgi_id = None means that we're creating a new GHGI
    validate_submitted(data['submitted'])
    if ghgi_id is not None:
        # we're updating one ghgi
        # we check that the user is the owner
        ghgi = get_object_or_404(GHGI, id=ghgi_id)
        if (ghgi.owner != request.user):
            raise PermissionDenied()

    laboratory = get_object_or_404(Laboratory, referent_id=request.user.id)
    data["laboratory"] = laboratory
    ghgi, _ = GHGI.objects.update_or_create(id=ghgi_id, defaults=data)
    serializer = GHGISerializer(ghgi)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def delete_ghgi(request, ghgi):
    ghgi.delete()
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def get_all_ghgi(request):
    if request.user.is_authenticated:
        ghgi_to_return = []
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        all_ghgi = GHGI.objects.filter(laboratory_id=laboratory.id).order_by('-year')
        for ghgi in all_ghgi:
            all_data = get_ghgi_consumption_data(ghgi.id)
            serializer = GHGISerializer(ghgi)
            final_ghgi = copy.deepcopy(serializer.data)
            for module in all_data.keys():
                final_ghgi[module] = copy.deepcopy(all_data[module])
            ghgi_to_return.append(final_ghgi)
        return Response(ghgi_to_return, status=status.HTTP_200_OK)
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_all_ghgi_without_consumptions(request):
    """Get the GHGIs list but without their associated consumptions

    Returns:
        - None if no user isn't authenticated
            This is peculiar choice but for now this allow to use the
            application in an anonymous way
        - The list of all the GHGIs otherwise
    """
    if not request.user.is_authenticated:
        # FIXME(msimonin): this is dead code as we
        # enforce isAuthenticated as default permission class in the default.py
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    laboratory = Laboratory.objects.get(referent_id=request.user.id)
    all_ghgi = GHGI.objects.filter(laboratory_id=laboratory.id).order_by('-year')
    serializer = GHGISerializer(all_ghgi, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
def get_ghgis_consumptions(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            module = request.data['module']
            ghgi_to_return = []
            all_ghgi = GHGI.objects.filter(id__in=request.data['ghgis'])
            for ghgi in all_ghgi:
                all_data = get_ghgi_consumption_data(ghgi.id, module)
                serializer = GHGISerializer(ghgi)
                final_ghgi = copy.deepcopy(serializer.data)
                final_ghgi[module] = copy.deepcopy(all_data[module])
                ghgi_to_return.append(final_ghgi)
            return Response(ghgi_to_return, status=status.HTTP_200_OK)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def get_all_ghgi_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            all_ghgi = GHGI.objects.all()
            serializer = GHGISerializer(all_ghgi, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def update_synthesis(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            validate_synthesis(request.data['synthesis'])
            ghgi_id = request.data.pop('ghgi_id')
            # save synthesis
            GHGI.objects.filter(id=ghgi_id).update(
                synthesis=request.data['synthesis']
            )
            return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def save_vehicles(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])

    laboratory = Laboratory.objects.get(referent_id=request.user.id)
    ghgi_consumptions = []
    for vehicle_data in data['vehicles']:
        vehicle_id = vehicle_data.pop('id')
        consumption_data = vehicle_data.pop('consumption')
        consumption_id = consumption_data.pop('id')
        vehicle_data['laboratory'] = laboratory
        vehicle, created = Vehicle.objects.update_or_create(id=vehicle_id, defaults=vehicle_data)
        consumption_data['ghgi'] = ghgi
        consumption_data['vehicle'] = vehicle
        consumption, created = VehicleConsumption.objects.update_or_create(id=consumption_id, defaults=consumption_data)
        ghgi_consumptions.append(consumption.id)
    # Delete consumptions no longer defined
    consumptions = VehicleConsumption.objects.filter(ghgi_id=ghgi.id)
    for consumption in consumptions:
        if not consumption.id in ghgi_consumptions:
            consumption.delete()
    # Delete vehicles with no consumption
    vehicles = Vehicle.objects.filter(laboratory_id=laboratory.id)
    for vehicle in vehicles:
        consumptions = VehicleConsumption.objects.filter(vehicle_id=vehicle.id)
        if len(consumptions) == 0:
            vehicle.delete()
    # save vehicles intensity
    GHGI.objects.filter(id=ghgi.id).update(
        synthesis=data['synthesis']
    )
    return Response(
        get_ghgi_consumption_data(ghgi.id, 'vehicles')['vehicles'],
        status=status.HTTP_201_CREATED
    )

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def save_computer_devices(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])
    ComputerDevice.objects.filter(ghgi_id=ghgi.id).delete()
    # ajouter les nouveaux
    for computer_device_data in data['devices']:
        computer_device_data['ghgi_id'] = ghgi.id
        computer_device = ComputerDevice.objects.create(**computer_device_data)

    # save devices intensity
    GHGI.objects.filter(id=ghgi.id).update(
        synthesis=request.data['synthesis']
    )

    computer_devices = ComputerDevice.objects.filter(ghgi_id=ghgi.id)
    serializer = ComputerDeviceSerializer(computer_devices, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def save_purchases(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])

    Purchase.objects.filter(ghgi_id=ghgi.id).delete()
    # ajouter les nouveaux
    for purchase_data in data['purchases']:
        purchase_data['ghgi_id'] = ghgi.id

        purchase = Purchase.objects.create(**purchase_data)

    # save purchases intensity
    GHGI.objects.filter(id=ghgi.id).update(
        synthesis=data['synthesis']
    )

    purchases = Purchase.objects.filter(ghgi_id=ghgi.id)
    serializer = PurchaseSerializer(purchases, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def get_all_vehicles(request):
    try:
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        vehicles = Vehicle.objects.filter(laboratory_id=laboratory.id)
        serializer = VehicleWCSerializer(vehicles, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def save_buildings(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])

    laboratory = Laboratory.objects.get(referent_id=request.user.id)
    all_electricities_id = []
    all_heating_ids = []
    all_refrigerants_ids = []
    all_waters_id = []
    for building_data in data['buildings']:
        building_id = building_data.pop('id')
        building_data['laboratory'] = laboratory
        # Remove consumption data from building data to be able to create building
        electricity_data = building_data.pop('electricity')
        heatings_data = building_data.pop('heatings')
        refrigerants = building_data.pop('refrigerants')
        water_data = building_data.pop('water')
        selfconsumption_data = building_data.pop('selfConsumption')
        # Create building if not exists
        building, created = Building.objects.update_or_create(id=building_id, defaults=building_data)
        # Electricity
        electricity_id = electricity_data.pop('id')
        electricity_data['ghgi'] = ghgi
        electricity_data['building'] = building
        electricity, created = Electricity.objects.update_or_create(id=electricity_id, defaults=electricity_data)
        all_electricities_id.append(electricity.id)
        # Heatings
        for heating_data in heatings_data:
            heating_id = heating_data.pop('id')
            heating_data['ghgi'] = ghgi
            heating_data['building'] = building
            heating, created = Heating.objects.update_or_create(id=heating_id, defaults=heating_data)
            all_heating_ids.append(heating.id)
        # Refrigerant gas
        for refrigerant_data in refrigerants:
            refrigerant_id = refrigerant_data.pop('id')
            refrigerant_data['ghgi'] = ghgi
            refrigerant_data['building'] = building
            refrigerant, created = Refrigerant.objects.update_or_create(id=refrigerant_id, defaults=refrigerant_data)
            all_refrigerants_ids.append(refrigerant.id)
        # Water
        water_id = water_data.pop('id')
        water_data['ghgi'] = ghgi
        water_data['building'] = building
        water, created = Water.objects.update_or_create(id=water_id, defaults=water_data)
        all_waters_id.append(water.id)
        # Selfconsumption
        SelfConsumption.objects.filter(building=building, ghgi=ghgi).delete()
        selfconsumption = SelfConsumption.objects.create(
            building=building,
            ghgi=ghgi,
            total=selfconsumption_data
        )
    # Remove refrigerants and heatings that are no longer associated with the building and ghgi
    refrigerants = Refrigerant.objects.filter(ghgi=ghgi)
    for refrigerant in refrigerants:
        if not refrigerant.id in all_refrigerants_ids:
            refrigerant.delete()
    heatings = Heating.objects.filter(ghgi=ghgi)
    for heating in heatings:
        if not heating.id in all_heating_ids:
            heating.delete()
    # Remove electricity consumptions no longer present in the data being saved for the given ghgi
    electricities = Electricity.objects.filter(ghgi_id=ghgi.id)
    for electricity in electricities:
        if not electricity.id in all_electricities_id:
            electricity.delete()
    # Remove water consumptions no longer present in the data being saved for the given ghgi
    waters = Water.objects.filter(ghgi_id=ghgi.id)
    for water in waters:
        if not water.id in all_waters_id:
            water.delete()
    # Remove buildings no longer linked to a consumption thus to a given ghgi
    buildings = Building.objects.filter(laboratory_id=laboratory.id)
    for building in buildings:
        electricity = Electricity.objects.filter(building_id=building.id)
        water = Water.objects.filter(building_id=building.id)
        heating = Heating.objects.filter(building_id=building.id)
        if len(electricity) == 0 and len(heating) == 0 and len(water) == 0:
            building.delete()
    GHGI.objects.filter(id=ghgi.id).update(
        synthesis=data['synthesis']
    )
    return Response(
        get_ghgi_consumption_data(ghgi.id, 'buildings')['buildings'],
        status=status.HTTP_201_CREATED
    )

@api_view(['POST'])
def get_all_buildings(request):
    try:
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        buildings = Building.objects.filter(laboratory_id=laboratory.id)
        serializer = BuildingWCSerializer(buildings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def save_commutes(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])
    for commute_data in request.data['commutes']:
        # update deleted status
        SurveyAnswer.objects.filter(id=commute_data['id']).update(
            deleted=commute_data['deleted']
        )
    # update intensity
    GHGI.objects.filter(id=ghgi.id).update(
        synthesis=data['synthesis']
    )
    commutes = SurveyAnswer.objects.filter(ghgi_id=ghgi.id)
    serializer = CommuteSerializer(commutes, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def get_ghgi_survey_info(request):
    uuid = request.data.pop('uuid')
    if validate_uuid(uuid) :
        ghgi = GHGI.objects.get(uuid=uuid)
        return Response({
            'year': ghgi.year,
            'surveyMessage': ghgi.surveyMessage,
            'commutesActive': ghgi.commutesActive,
            'foodsActive': ghgi.foodsActive,
            'contactEmail': ghgi.laboratory.referent.email,
            'labName': ghgi.laboratory.name,
            'citySize': ghgi.laboratory.citySize
            }, status=status.HTTP_200_OK)
    else:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def clone_survey(request, ghgi):
    data = request.data.copy()
    surveyCloneYear = data.pop('surveyCloneYear')
    GHGI.objects.filter(id=ghgi.id).update(surveyCloneYear=surveyCloneYear)
    to_return = get_ghgi_consumption_data(ghgi.id, 'commutes')['commutes']
    return Response(to_return, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def save_survey_message(request, ghgi):
    data = request.data.copy()
    surveyMessage = data.pop('surveyMessage')
    GHGI.objects.filter(id=ghgi.id).update(surveyMessage=surveyMessage)
    return Response({}, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def save_survey(request):
    uuid = request.data.pop('uuid')
    survey_data = request.data.pop('survey')
    sections_data = survey_data.pop('sections')
    meals_data = survey_data.pop('meals')
    ghgi = GHGI.objects.get(uuid=uuid)
    # add the new survey result
    survey = SurveyAnswer.objects.create(ghgi_id=ghgi.id, **survey_data)
    SurveyAnswer.objects.filter(id=survey.id).update(seqID=survey.id)
    for section_data in sections_data:
        CommuteSection.objects.create(survey_id=survey.id, **section_data)
    for meal_data in meals_data:
        Meal.objects.create(survey_id=survey.id, **meal_data)
    survey = SurveyAnswer.objects.get(id=survey.id)
    serializer = CommuteSerializer(survey)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def save_travels(request, ghgi):
    data = request.data
    validate_synthesis(data["synthesis"])

    def make_travel(data_travel):
        data = copy.deepcopy(data_travel)
        data["ghgi_id"] = ghgi.id
        data.pop("sections")
        data.pop("names")
        travel = Travel.objects.create(**data)
        return travel
        
    def make_sections(travel, data):
        data_sections = data["sections"]
        sections = [TravelSection(
                    travel=travel, 
                    distance=section['distance'],
                    transportation=section['transportation'],
                    carpooling=section['carpooling'],
                    type=section['type'],
                    isRoundTrip=section['isRoundTrip']
                ) for section in data_sections]
        return sections
    
    def make_names(travel, data):
        data_names = data["names"]
        names = [TravelNames(travel=travel, name=name) for name in data_names]
        return names
   
   
    data = request.data

    # Delete all travels
    Travel.objects.filter(ghgi_id=ghgi.id).delete()

    # build / save all the travels
    all_travels = []
    for d in data["travels"]:
        all_travels.append(make_travel(d))

    # build all sections
    all_sections = []
    for (t, d) in zip(all_travels, request.data["travels"]):
        all_sections.extend(make_sections(t, d))
    TravelSection.objects.bulk_create(all_sections)

    # build names
    all_names = []
    for (t, d) in zip(all_travels, request.data["travels"]):
        all_names.extend(make_names(t, d))
    TravelNames.objects.bulk_create(all_names)

    # save travels intensity
    GHGI.objects.filter(id=ghgi.id).update(
        synthesis=request.data['synthesis']
    )

    travels = Travel.objects.filter(ghgi_id=ghgi.id)
    serializer = TravelSerializer(travels, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id", allow_super_user=True)
def update_submitted(request, ghgi):
    validate_submitted(request.data['submitted'])
    GHGI.objects.filter(id=ghgi.id).update(
        submitted=request.data['submitted']
    )
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id")
def update_survey_active(request, ghgi):
    validate_synthesis(request.data['synthesis'])
    GHGI.objects.filter(id=ghgi.id).update(
        commutesActive=request.data['commutesActive'],
        foodsActive=request.data['foodsActive'],
        synthesis=request.data['synthesis']
    )
    return Response(None, status=status.HTTP_204_NO_CONTENT)

def get_ghgi_consumption_data(ghgi_id, module = None):

    ghgi = GHGI.objects.get(id=ghgi_id)
    to_return = {
        'vehicles': [],
        'buildings': [],
        'commutes': [],
        'travels': [],
        'devices': [],
        'purchases': [],
        'foods': []
    }

    if module == 'buildings' or module is None:
        # add buildings and modify serializer representation from many consumptions to one consumption
        buildings = Building.objects.filter(Q(electricity__ghgi_id=ghgi_id) | Q(heatings__ghgi_id=ghgi_id) | Q(refrigerants__ghgi_id=ghgi_id)).distinct()
        buildings_data = BuildingSerializer(buildings, many=True).data
        for building_data in buildings_data:
            building_data['heatings'] = filter(
                lambda heating: heating['ghgi'] == ghgi_id,
                building_data['heatings']
            )
            filtered_electricity = list(filter(
                lambda electricity: electricity['ghgi'] == ghgi_id,
                building_data['electricity']
            ))
            if len(filtered_electricity) > 0:
                building_data['electricity'] = filtered_electricity[0]
            building_data['refrigerants'] = filter(
                lambda refrigerant: refrigerant['ghgi'] == ghgi_id,
                building_data['refrigerants']
            )
            filtered_water = list(filter(
                lambda water: water['ghgi'] == ghgi_id,
                building_data['water']
            ))
            if len(filtered_water) > 0:
                building_data['water'] = filtered_water[0]
            filtered_selfconsumption = building_data['selfConsumption'] = list(filter(
                lambda sconsumption: sconsumption['ghgi'] == ghgi_id,
                building_data['selfConsumption']
            ))
            if len(filtered_selfconsumption) > 0:
                building_data['selfConsumption'] = filtered_selfconsumption[0]['total']
        to_return['buildings'] = buildings_data

    if module == 'vehicles' or module is None:
        # add vehicles and modify serializer representation from many consumptions to one consumption
        vehicles = Vehicle.objects.filter(consumption__ghgi_id=ghgi_id).distinct()
        vehicles_data = VehicleSerializer(vehicles, many=True).data
        for vehicle_data in vehicles_data:
            filtered_vehicle = vehicle_data['consumption'] = list(filter(
                lambda vehicle: vehicle['ghgi'] == ghgi_id,
                vehicle_data['consumption']
            ))
            if len(filtered_vehicle) > 0:
                vehicle_data['consumption'] = filtered_vehicle[0]
        to_return['vehicles'] = vehicles_data

    if module == 'commutes' or module is None:
        # add commutes considering if its a clone survey
        if ghgi.surveyCloneYear is None:
            commutes = SurveyAnswer.objects.filter(
                ghgi_id=ghgi_id
            ).annotate(
                nsections=Count('sections')
            ).filter(Q(nsections__gt=0) | Q(nWorkingDay=0))
            commutesSerializer = CommuteSerializer(commutes, many=True)
            to_return['commutes'] = commutesSerializer.data
        else:
            try:
                ghgiSurvey = GHGI.objects.get(year=ghgi.surveyCloneYear, laboratory=ghgi.laboratory)
                commutes = SurveyAnswer.objects.filter(
                    ghgi_id=ghgiSurvey.id
                ).annotate(
                    nsections=Count('sections')
                ).filter(Q(nsections__gt=0) | Q(nWorkingDay=0))
                commutesSerializer = CommuteSerializer(commutes, many=True)
                to_return['commutes'] = commutesSerializer.data
            except GHGI.DoesNotExist as exc:
                # TODO : modify year for the ghgi id
                # if no GHGI linked found, remove it
                GHGI.objects.filter(id=ghgi_id).update(surveyCloneYear=None)

    if module == 'foods' or module is None:
        # add foods considering if its a clone survey
        if ghgi.surveyCloneYear is None:
            foods = SurveyAnswer.objects.filter(
                ghgi_id=ghgi_id
            ).annotate(
                nmeals=Count('meals')
            ).filter(Q(nmeals__gt=0) | Q(nWorkingDay=0))
            foodsSerializer = FoodSerializer(foods, many=True)
            to_return['foods'] = foodsSerializer.data
        else:
            try:
                ghgiSurvey = GHGI.objects.get(year=ghgi.surveyCloneYear, laboratory=ghgi.laboratory)
                foods = SurveyAnswer.objects.filter(
                    ghgi_id=ghgiSurvey.id
                ).annotate(
                    nmeals=Count('meals')
                ).filter(Q(nmeals__gt=0) | Q(nWorkingDay=0))
                foodsSerializer = FoodSerializer(foods, many=True)
                to_return['foods'] = foodsSerializer.data
            except GHGI.DoesNotExist as exc:
                # TODO : modify year for the ghgi id
                # if no GHGI linked found, remove it
                GHGI.objects.filter(id=ghgi_id).update(surveyCloneYear=None)

    if module == 'travels' or module is None:
        # add travels
        travels = Travel.objects.filter(ghgi_id=ghgi_id)
        serializertravels = TravelSerializer(travels, many=True)
        to_return['travels'] = serializertravels.data

    if module == 'devices' or module is None:
        # add computer devices
        computer_devices = ComputerDevice.objects.filter(ghgi_id=ghgi_id)
        serializercd = ComputerDeviceSerializer(computer_devices, many=True)
        to_return['devices'] = serializercd.data

    if module == 'purchases' or module is None:
        # add purchases
        purchases = Purchase.objects.filter(ghgi_id=ghgi_id)
        serializerp = PurchaseSerializer(purchases, many=True)
        to_return['purchases'] = serializerp.data

    return to_return

@api_view(['POST'])
@owns_ghgi(data_key="ghgi_id", allow_super_user=True)
def get_ghgi_consumptions(request, ghgi):
    to_return = get_ghgi_consumption_data(ghgi.id)
    to_return['ghgi'] = GHGISerializer(ghgi).data
    return Response(to_return, status=status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes([])
def get_ghgi_consumptions_by_uuid(request):
    uuid = request.data.pop('uuid')
    ghgi = GHGI.objects.get(uuid=uuid)
    to_return = get_ghgi_consumption_data(ghgi.id)
    to_return['ghgi'] = GHGISerializer(ghgi).data
    return Response(to_return, status=status.HTTP_200_OK)