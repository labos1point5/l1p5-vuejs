import uuid


from django.db import models
from django.conf import settings


ALL_MODULES = [
    "electricity",
    "heatings",
    "refrigerants",
    "water",
    "construction",
    "commutes",
    "travels",
    "devices",
    "purchases",
    "vehicles",
    "foods",
]


class GHGI(models.Model):
    # Used to validate the synthesis json-field
    # Rationale: do our best to not pollute the database with mal-formed synthesis
    # Also there's an implicit contract that the frontend sends all the modules
    SYNTHESIS_SCHEMA = {
        "type": "object",
        "properties": 
            {m: {"$ref": "#/definitions/carbonIntensity"} for m in ALL_MODULES},
        # the combination of the two following is to make sure we accept 
        # only the above properties
        "additionalProperties": False,
        "minProperties": len(ALL_MODULES),
        "definitions": {
            "carbonIntensity": {
                "properties": {
                    "intensity": {"type": "number"},
                    "intensitywc": {"type": "number"},
                    "uncertainty": {"type": "number"},
                    "uncertaintywc": {"type": "number"},
                    "group": {"type": ["string", "null"]}
                },
            "additionalProperties": False,
            "minProperties": 4
            },
        }
    }

    # Used to validate the submitted json-field
    # Rationale: do our best to not pollute the database with mal-formed synthesis
    # Also there's an implicit contract that the frontend sends all the modules
    SUBMITTED_SCHEMA = {
        "type": "object",
        "properties": {m: {"type": "boolean"} for m in ALL_MODULES},
        "additionalProperties": False,
        "minProperties": len(ALL_MODULES)
    }

    uuid = models.UUIDField(
        null=True,
        unique = True,
        editable = False
    )
    year = models.IntegerField()
    created = models.DateField(auto_now_add=True)
    description = models.TextField(null=True)
    nResearcher = models.IntegerField(null=True)
    nProfessor = models.IntegerField(null=True)
    nEngineer = models.IntegerField(null=True)
    nStudent = models.IntegerField(null=True)
    budget = models.IntegerField(null=True)

    surveyCloneYear = models.IntegerField(null=True)
    surveyMessage = models.CharField(max_length=500, null=True)
    commutesActive = models.BooleanField(default=False)
    foodsActive = models.BooleanField(default=False)
    
    laboratory = models.ForeignKey('core.Laboratory', related_name='bges', on_delete=models.CASCADE, null=True)

    # synthesis as computed by the client side application
    synthesis = models.JSONField(null=True)
    submitted = models.JSONField(null=True)    

    @property
    def owner(self):
        return self.laboratory.referent
    
    def save(self, *args, **kwargs):
        ''' On save, update uuid and avoid clash with existing code '''
        if not self.uuid:
            self.uuid = uuid.uuid4()
        return super(GHGI, self).save(*args, **kwargs)


class SurveyAnswer(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='answers', on_delete=models.CASCADE)
    seqID = models.CharField(max_length=10, null=True)
    position = models.CharField(max_length=250)
    nWorkingDay = models.IntegerField()
    nWorkingDay2 = models.IntegerField(default=0, null=True)
    nCateringMeal = models.IntegerField(default=0, null=True)
    message = models.CharField(max_length=500, null=True)
    deleted = models.BooleanField(default=False)

class CommuteSection(models.Model):
    survey = models.ForeignKey(SurveyAnswer, related_name='sections', on_delete=models.CASCADE)
    mode = models.CharField(max_length=250, null=True)
    distance = models.IntegerField(default=0)
    isDay2 = models.BooleanField(default=False)
    pooling = models.IntegerField(default=1)
    engine = models.CharField(max_length=25, null=True)

class Meal(models.Model):
    survey = models.ForeignKey(SurveyAnswer, related_name='meals', on_delete=models.CASCADE)
    type = models.CharField(max_length=250, null=True)
    amount = models.IntegerField()

class Travel(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='travel', on_delete=models.CASCADE)
    location = models.CharField(max_length=250, null=True)
    purpose = models.CharField(max_length=250, null=True)
    status = models.CharField(max_length=250, null=True)
    amount = models.IntegerField()

class TravelNames(models.Model):
    name = models.CharField(max_length=50)
    travel = models.ForeignKey(Travel, related_name='names', on_delete=models.CASCADE)

class TravelSection(models.Model):
    travel = models.ForeignKey(Travel, related_name='sections', on_delete=models.CASCADE)
    type = models.CharField(max_length=2,choices=[('NA', 'Nationale'), ('IN', 'Internationale'), ('MX', 'Mixte')], default='NA')
    isRoundTrip = models.BooleanField(default=True)
    distance = models.FloatField()
    transportation = models.CharField(max_length=250)
    carpooling = models.IntegerField(null=True)

class Vehicle(models.Model):
    name = models.CharField(max_length=250)
    type = models.CharField(max_length=250)
    engine = models.CharField(max_length=250)
    unit = models.CharField(max_length=250)
    power = models.IntegerField(null=True)
    noEngine = models.IntegerField(null=True)
    shp = models.IntegerField(null=True)
    controled = models.BooleanField()
    laboratory = models.ForeignKey('core.Laboratory', related_name='vehicle', on_delete=models.CASCADE, null=True)

class Building(models.Model):
    laboratory = models.ForeignKey('core.Laboratory', related_name='building', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=250)
    area = models.FloatField()
    share = models.FloatField()
    selfProduction = models.BooleanField()
    constructionYear = models.IntegerField(null=True)
    site = models.CharField(max_length=250, null=True)

class Consumption(models.Model):
    january = models.FloatField(null=True)
    february = models.FloatField(null=True)
    march = models.FloatField(null=True)
    april = models.FloatField(null=True)
    may = models.FloatField(null=True)
    june = models.FloatField(null=True)
    july = models.FloatField(null=True)
    august = models.FloatField(null=True)
    septembre = models.FloatField(null=True)
    octobre = models.FloatField(null=True)
    novembre = models.FloatField(null=True)
    decembre = models.FloatField(null=True)
    total = models.FloatField(null=True)
    isMonthly = models.BooleanField()

    class Meta:
        abstract = True

class VehicleConsumption(Consumption):
    ghgi = models.ForeignKey(GHGI, related_name='consumption', on_delete=models.CASCADE)
    vehicle = models.ForeignKey(Vehicle, related_name='consumption', on_delete=models.CASCADE, null=True)

class Heating(Consumption):
    ghgi = models.ForeignKey(GHGI, related_name='heatings', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='heatings', on_delete=models.CASCADE, null=True)
    type = models.CharField(max_length=250, null=True)
    urbanNetwork = models.CharField(max_length=250, null=True)
    isOwnedByLab = models.BooleanField()

class Electricity(Consumption):
    ghgi = models.ForeignKey(GHGI, related_name='electricity', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='electricity', on_delete=models.CASCADE, null=True)

class Water(Consumption):
    ghgi = models.ForeignKey(GHGI, related_name='water', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='water', on_delete=models.CASCADE, null=True)

class Refrigerant(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='refrigerants', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='refrigerants', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50)
    total = models.FloatField(null=True)

class SelfConsumption(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='selfConsumption', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='selfConsumption', on_delete=models.CASCADE)
    total = models.IntegerField(null=True)

class ComputerDevice(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='device', on_delete=models.CASCADE)
    type = models.CharField(max_length=250)
    model = models.CharField(max_length=250, null=True)
    amount = models.IntegerField(null=True)
    acquisitionYear = models.IntegerField(null=True)

class Purchase(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='purchase', on_delete=models.CASCADE)
    code = models.CharField(max_length=250)
    amount = models.DecimalField(max_digits=10, decimal_places=2)