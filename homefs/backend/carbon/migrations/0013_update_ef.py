# Generated by Django 3.1.5 on 2021-10-30 06:48

from django.db import migrations

def update_ef_vehicles(apps, schema_editor):
    Vehicule = apps.get_model('carbon', 'Vehicule')
    for row in Vehicule.objects.all():
        if row.typeVehicule == 'Voiture':
            row.typeVehicule = 'car'
        elif row.typeVehicule == 'Moto':
            row.typeVehicule = 'motorbike'
        elif row.typeVehicule == 'Vélo':
            row.typeVehicule = 'bike'
        elif row.typeVehicule == 'Trottinette':
            row.typeVehicule = 'scooter'
        elif row.typeVehicule == 'Aéronef':
            row.typeVehicule = 'aircraft'
        elif row.typeVehicule == 'Bateau':
            row.typeVehicule = 'boat'
        if row.motorisation == 'Essence':
            row.motorisation = 'gasoline'
        elif row.motorisation == 'Diesel':
            row.motorisation = 'diesel'
        elif row.motorisation == 'Motorisation inconnue':
            row.motorisation = 'unknown.engine'
        elif row.motorisation == 'GPL':
            row.motorisation = 'lpg'
        elif row.motorisation == 'CNG':
            row.motorisation = 'cng'
        elif row.motorisation == 'Hybride':
            row.motorisation = 'hybrid'
        elif row.motorisation == 'Électrique':
            row.motorisation = 'electric'
        elif row.motorisation == 'Electrique':
            row.motorisation = 'electric'
        elif row.motorisation == 'Avion à hélices (pistons)':
            row.motorisation = 'plane.pistons'
        elif row.motorisation == 'Hélicoptère (pistons)':
            row.motorisation = 'helicopter.pistons'
        elif row.motorisation == 'Avion à hélices (turbines)':
            row.motorisation = 'plane.turbines'
        elif row.motorisation == 'Hélicoptère (turbines)':
            row.motorisation = 'helicopter.turbines'
        elif row.motorisation == 'Musculaire':
            row.motorisation = 'muscular'
        elif row.motorisation == 'Fioul lourd HFO':
            row.motorisation = 'heavy.oil'
        elif row.motorisation == 'Fioul léger HFO':
            row.motorisation = 'light.oil'
        elif row.motorisation == 'Campagnes en mer':
            row.motorisation = 'campaigns'
        if row.unite == 'litres':
            row.unite = 'litre'
        row.save(update_fields=['typeVehicule', 'motorisation', 'unite'])

def update_ef_commutes(apps, schema_editor):
    DeplacementDT = apps.get_model('carbon', 'DeplacementDT')
    for row in DeplacementDT.objects.all():
        if row.motorisation == 'Essence':
            row.motorisation = 'gasoline'
        elif row.motorisation == 'Diesel':
            row.motorisation = 'diesel'
        elif row.motorisation == 'Motorisation inconnue':
            row.motorisation = 'unknown.engine'
        elif row.motorisation == 'GPL':
            row.motorisation = 'lpg'
        elif row.motorisation == 'CNG':
            row.motorisation = 'cng'
        elif row.motorisation == 'Hybride':
            row.motorisation = 'hybrid'
        elif row.motorisation == 'Hybrid':
            row.motorisation = 'hybrid'
        elif row.motorisation == 'Électrique':
            row.motorisation = 'electric'
        elif row.motorisation == 'Electrique':
            row.motorisation = 'electric'
        elif row.motorisation == '':
            row.motorisation = None
        if row.motorisation2 == 'Essence':
            row.motorisation2 = 'gasoline'
        elif row.motorisation2 == 'Diesel':
            row.motorisation2 = 'diesel'
        elif row.motorisation2 == 'Motorisation inconnue':
            row.motorisation2 = 'unknown.engine'
        elif row.motorisation2 == 'GPL':
            row.motorisation2 = 'lpg'
        elif row.motorisation2 == 'CNG':
            row.motorisation2 = 'cng'
        elif row.motorisation2 == 'GNV':
            row.motorisation2 = 'cng'
        elif row.motorisation2 == 'Hybride':
            row.motorisation2 = 'hybrid'
        elif row.motorisation2 == 'Hybrid':
            row.motorisation2 = 'hybrid'
        elif row.motorisation2 == 'Électrique':
            row.motorisation2 = 'electric'
        elif row.motorisation2 == 'Electrique':
            row.motorisation2 = 'electric'
        if row.statut == 'chercheurs':
            row.statut = 'researcher'
        elif row.statut == 'docs':
            row.statut = 'student'
        elif row.statut == 'ITA':
            row.statut = 'engineer'
        row.save(update_fields=['motorisation', 'motorisation2', 'statut'])

def update_ef_buildings(apps, schema_editor):
    import os
    import json
    DATA_DIR = os.path.abspath(os.path.join(__file__, "../../../../data"))
    with open(os.path.join(DATA_DIR, 'heatingFactors.json')) as ef_file :
        data = ef_file.read()
    factors = json.loads(data)
    Batiment = apps.get_model('carbon', 'Batiment')
    for row in Batiment.objects.all():
        if row.typeChauffage == 'Chauffage urbain':
            row.typeChauffage = 'urban.network'
            new_name = row.reseauChauffageUrbain.replace(', France continentale, Base Carbone', '')
            for factor_class in factors.keys():
                for factor_name in factors[factor_class].keys():
                    if factors[factor_class][factor_name]['description_FR'] == new_name:
                        row.reseauChauffageUrbain = factor_name 
        elif row.typeChauffage == 'Gaz naturel':
            row.typeChauffage = 'naturalgas'
            row.reseauChauffageUrbain = ''
        elif row.typeChauffage == 'Propane':
            row.typeChauffage = 'propane'
            row.reseauChauffageUrbain = ''
        elif row.typeChauffage == 'Fioul domestique':
            row.typeChauffage = 'heating.oil'
            row.reseauChauffageUrbain = ''
        elif row.typeChauffage == 'Biométhane':
            row.typeChauffage = 'biomethane'
            row.reseauChauffageUrbain = ''
        elif row.typeChauffage == 'Granulés de bois':
            row.typeChauffage = 'wood.pellets'
            row.reseauChauffageUrbain = ''
        elif row.typeChauffage == 'Plaquettes':
            row.typeChauffage = 'wood.chips'
            row.reseauChauffageUrbain = ''
        elif row.typeChauffage == 'Bois bûche':
            row.typeChauffage = 'wood.logs'
            row.reseauChauffageUrbain = ''
        row.save(update_fields=['typeChauffage', 'reseauChauffageUrbain'])

def update_ef_refrigerants(apps, schema_editor):
    GazRefrigerant = apps.get_model('carbon', 'GazRefrigerant')
    for row in GazRefrigerant.objects.all():
        row.nom = row.nom.lower()
        row.save(update_fields=['nom'])

def update_ef_travels(apps, schema_editor):
    Mission = apps.get_model('carbon', 'Mission')
    for row in Mission.objects.all():
        if row.modeDeplacement == 'Avion':
            row.modeDeplacement = 'plane'
        elif row.modeDeplacement == 'Train':
            row.modeDeplacement = 'train'
        elif row.modeDeplacement == 'Voiture':
            row.modeDeplacement = 'car'
        elif row.modeDeplacement == 'Taxi':
            row.modeDeplacement = 'cab'
        elif row.modeDeplacement == 'Bus':
            row.modeDeplacement = 'bus'
        elif row.modeDeplacement == 'Tramway':
            row.modeDeplacement = 'tram'
        elif row.modeDeplacement == 'RER':
            row.modeDeplacement = 'rer'
        elif row.modeDeplacement == 'Métro':
            row.modeDeplacement = 'subway'
        elif row.modeDeplacement == 'Ferry':
            row.modeDeplacement = 'ferry'
        if row.statut == 'Personne invitée':
            row.statut = 'guest'
        elif row.statut == 'Chercheur.e-EC':
            row.statut = 'researcher'
        elif row.statut == 'Doc-Post doc':
            row.statut = 'docs'
        elif row.statut == 'ITA':
            row.statut = 'ita'
        elif row.statut == '':
            row.statut = None
        if row.motifDeplacement == 'Etude terrain':
            row.motifDeplacement = 'field.study'
        elif row.motifDeplacement == 'Colloque-Congrès':
            row.motifDeplacement = 'conference'
        elif row.motifDeplacement == 'Séminaire':
            row.motifDeplacement = 'seminar'
        elif row.motifDeplacement == 'Enseignement':
            row.motifDeplacement = 'teaching'
        elif row.motifDeplacement == 'Collaboration':
            row.motifDeplacement = 'collaboration'
        elif row.motifDeplacement == 'Visite':
            row.motifDeplacement = 'visit'
        elif row.motifDeplacement == 'Administration de la recherche':
            row.motifDeplacement = 'research.management'
        elif row.motifDeplacement == 'Autre':
            row.motifDeplacement = 'other'
        elif row.motifDeplacement == '':
            row.motifDeplacement = None
        row.save(update_fields=['modeDeplacement', 'statut', 'motifDeplacement'])

class Migration(migrations.Migration):

    dependencies = [
        ('carbon', '0012_alter_bges_submitted'),
    ]

    operations = [
        migrations.RunPython(update_ef_vehicles),
        migrations.RunPython(update_ef_commutes),
        migrations.RunPython(update_ef_buildings),
        migrations.RunPython(update_ef_refrigerants),
        migrations.RunPython(update_ef_travels),
    ]
