# Generated by Django 3.2.7 on 2022-08-24 14:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20201010_1637'),
        ('carbon', '0023_bges_surveycloneyear'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Vehicule',
            new_name='Vehicle',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='nom',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='typeVehicule',
            new_name='type',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='motorisation',
            new_name='engine',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='unite',
            new_name='unit',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='puissance',
            new_name='power',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='nbMoteurs',
            new_name='noEngine',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='controleOperationnel',
            new_name='controled',
        ),
        migrations.RenameField(
            model_name='Vehicle',
            old_name='laboratoire',
            new_name='laboratory',
        ),
        migrations.RenameField(
            model_name='Consommation',
            old_name='vehicule',
            new_name='vehicle',
        ),
        migrations.AlterField(
            model_name='consommation',
            name='vehicle',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='consumption', to='carbon.vehicle'),
        ),
        migrations.AlterField(
            model_name='vehicle',
            name='laboratory',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vehicle', to='api.laboratoire'),
        ),
    ]
