# Generated by Django 3.2.7 on 2022-09-29 06:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('carbon', '0029_auto_20220929_0653'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='NewSelfConsumption',
            new_name='SelfConsumption',
        ),
        migrations.AlterField(
            model_name='selfConsumption',
            name='building',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='selfconsumption', to='carbon.building'),
        ),
        migrations.AlterField(
            model_name='selfConsumption',
            name='ghgi',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='selfconsumption', to='carbon.bges'),
        ),
    ]
