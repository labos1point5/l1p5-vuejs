from django.core.management.base import BaseCommand
from rest_framework_simplejwt.tokens import RefreshToken

from backend.users.models import L1P5User


class Command(BaseCommand):
    help = """Get a JWT token for a user."""

    def add_arguments(self, parser):
        parser.add_argument("email", type=str)

    def handle(self, *args, **options):

        email = options["email"]

        user = L1P5User.objects.get(email=email)

        refresh = RefreshToken.for_user(user)

        print(f"Access Token: {refresh.access_token}")
