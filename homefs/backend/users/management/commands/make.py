from django.core.management.base import BaseCommand

from backend.users.models import L1P5Group
from backend.test_utils import ensure_user_created


class Command(BaseCommand):
    help = """Applying a permission profile to a user"""

    def add_arguments(self, parser):
        parser.add_argument(
            "--role", choices=[r.name for r in L1P5Group.objects.iterator()]
        )
        parser.add_argument("email", type=str)
        parser.add_argument("password", type=str)

    def handle(self, *args, **options):
        role = options["role"]
        email = options["email"]
        password = options["password"]

        user = ensure_user_created(email, password=password)
        if role:
            g = L1P5Group.objects.get(name=role)
            user.groups.set([g])
