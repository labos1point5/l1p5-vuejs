from enum import Enum

from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, Group
from .managers import UserManager


class L1P5User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(max_length=190, unique=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(("active"), default=False)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    @property
    def roles(self):
        _roles = []
        for group in self.groups.iterator():
            _roles.append(group.name)
        return _roles

    class Meta:
        verbose_name = "user"


class L1P5Group(Group):
    pass
