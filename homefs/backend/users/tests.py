from django.test import Client
from django.test import TestCase
from rest_framework_simplejwt.tokens import RefreshToken

from backend.transition.models import REVIEWER_GROUP_NAME

from ..test_utils import ensure_admin_created, ensure_user_created

REQUEST_KWARGS = dict(content_type="application/json")


class TestUsers(TestCase):
    def setUp(self) -> None:
        # create two users
        # make sure to name them differently
        self.user = ensure_user_created(email="test@l1p5.org")
        self.admin = ensure_admin_created(email="admin@l1p5.org")

        # create a user to interact with the view
        refresh = RefreshToken.for_user(self.user)
        # in django >= 4.2 we could use the headers for that
        self.api_client = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

        # create a user to interact with the view
        refresh = RefreshToken.for_user(self.admin)
        # in django >= 4.2 we could use the headers for that
        self.api_admin = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def test_get_users(self):
        response = self.api_client.get(
            "/api/get_users/",
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            response.status_code, 403, "regular user can't get the list of users"
        )

        response = self.api_admin.get(
            "/api/get_users/",
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            response.status_code, 200, "admin user can get the list of users"
        )
        users = response.json()
        self.assertEqual(2, len(users))

        # check roles
        user = [u for u in users if u["email"] == self.user.email][0]
        self.assertEqual(0, len(user["roles"]))
        self.assertFalse(user["is_superuser"])

        # check roles
        user = [u for u in users if u["email"] == self.admin.email][0]
        self.assertEqual(0, len(user["roles"]))
        self.assertTrue(user["is_superuser"])

    def test_update_superuser(self):
        # make the user an admin
        data = dict(email=self.user.email, value=True)
        response = self.api_client.post(
            "/api/update_is_admin/",
            data=data,
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            response.status_code, 403, "regular user can't update admin permission"
        )

        response = self.api_admin.post(
            "/api/update_is_admin/",
            data=data,
            **REQUEST_KWARGS,
        )
        self.assertEqual(
            response.status_code, 200, "admin can update admin permissions"
        )

        # chek api level roles
        user = response.json()
        self.assertTrue(user["is_superuser"])

        # make it a reviewer
        data = dict(email=self.user.email, roles=[REVIEWER_GROUP_NAME])
        response = self.api_admin.post(
            "/api/update_roles/",
            data=data,
            **REQUEST_KWARGS,
        )
        user = response.json()
        self.assertEqual(1, len(user["roles"]))
        self.assertEqual(REVIEWER_GROUP_NAME, user["roles"][0])

        # make it a user (no roles)
        data = dict(email=self.user.email, roles=[])
        response = self.api_admin.post(
            "/api/update_roles/",
            data=data,
            **REQUEST_KWARGS,
        )
        user = response.json()
        self.assertEqual(0, len(user["roles"]))

    def test_update_permissions_404(self):
        # make the user an admin
        data = dict(email="IDonotExist@l1p5.org", roles=[REVIEWER_GROUP_NAME])

        response = self.api_admin.post(
            "/api/update_roles/",
            data=data,
            **REQUEST_KWARGS,
        )
        self.assertEqual(response.status_code, 404, "Non existent user raises 404")
