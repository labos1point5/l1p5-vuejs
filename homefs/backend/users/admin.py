from django.contrib import admin
from .models import L1P5User, L1P5Group


# Register your models here.
@admin.register(L1P5User)
class L1P5UserAdmin(admin.ModelAdmin):
    pass


@admin.register(L1P5Group)
class L1P5GroupAdmin(admin.ModelAdmin):
    pass
