#
# This is the L1P5 application configuration
#
# you can configure the position titles used Boundary, Survey and Travel
# Mandatory keys are marked like this: (MANDATORY)
#
# As you need to translate all of these you can define here your custom translation
# that will be sent back to the frontend.

# (Mandatory) The URL of the log (supported string are those supported by the
# HTML img.src attribute)
L1P5_LOGO_URL = "/static/img/labos1p5_logo_white.png"

# (Mandatory) List of apps names to activate
# name is made of the type / appname method
L1P5_ACTIVE_APPS = [
    "application/carbon",
    "application/scenario",
    "application/transition",
    "simulator/travels-simulator",
]

# (Mandatory) List of module names to activate
# name is made of the type / thecollection method
L1P5_ACTIVE_MODULES = [
    "buildings/form",
    "vehicles/form",
    "travels/form",
]


# (Mandatory) Mapping between module type and color
L1P5_MODULES_COLOR = {
    "water": "#931c63",
    "construction": "#af4284",
    "heatings": "#c84896",
    "refrigerants": "#fdc2e5",
    "vehicles": "#fd8e62",
    "travels": "#67c3a5",
    "commutes": "#8ea0cc",
    "devices": "#a6d953",
    "purchases": "#ffda2c",
    "ractivities": "#bf4040",
}


# (Mandatory) Which username to use for geoname service
L1P5_GEONAMES_USERNAME = "labos1point5"

# (Mandatory) Which country the entity is located in
L1P5_ALLOWED_COUNTRIES = [
    "US",
    "CL",
]

# (Mandatory)
L1P5_ENTITY_CLASS = "Laboratory"

L1P5_NUMBER_OF_WORKED_WEEKS = {"default": 47}

L1P5_FILE_FORMATS = ["ges1p5"]

L1P5_ACCEPTED_MIME_TYPES = "application/pdf,image/*,.lss"

# The following is for factorization purpose within this file
# Note however that the values here will end up in the export files
## Boundary: the categories of position titles
_L1P5_PT_RESEARCHER = "pt.researcher"
_L1P5_PT_STUDENTPOSTDOC = "pt.student-postdoc"
_L1P5_PT_ADMINISTRATION = "pt.administration"
_L1P5_PT_MANAGER = "pt.manager"
_L1P5_PT_EDUPUBOUT = "pt.educationpublicoutreach"
_L1P5_PT_SOFTWARE_ENG = "pt.software_eng"
_L1P5_PT_TECHNICAL_ENG = "pt.technical_eng"
_L1P5_PT_TECHNICAL_SCI = "pt.technical_sci"

## Survey: the categories used in the survey
_L1P5_CF_RESEARCHER = "cf.researcher"
_L1P5_CF_STUDENTPOSTDOC = "cf.student-postdoc"
_L1P5_CF_ADMINISTRATION = "cf.administration"
_L1P5_CF_MANAGER = "cf.manager"
_L1P5_CF_EDUPUBOUT = "cf.educationpublicoutreach"
_L1P5_CF_SOFTWARE_ENG = "cf.software_eng"
_L1P5_CF_TECHNICAL_ENG = "cf.technical_eng"
_L1P5_CF_TECHNICAL_SCI = "cf.technical_sci"


## Travel: the categories used in the travel model (identical to the position categories for AURA + guest + unknown)
_L1P5_TRAVEL_RESEARCHER = "pt.researcher"
_L1P5_TRAVEL_STUDENTPOSTDOC = "pt.student-postdoc"
_L1P5_TRAVEL_ADMINISTRATION = "pt.administration"
_L1P5_TRAVEL_MANAGER = "pt.manager"
_L1P5_TRAVEL_EDUPUBOUT = "pt.educationpublicoutreach"
_L1P5_TRAVEL_SOFTWARE_ENG = "pt.software_eng"
_L1P5_TRAVEL_TECHNICAL_ENG = "pt.technical_eng"
_L1P5_TRAVEL_TECHNICAL_SCI = "pt.technical_sci"
_L1P5_TRAVEL_GUEST = "travel.guest"
# this label is special
_L1P5_TRAVEL_UNKNOWN = "travel.unknown"


# (MANDATORY) define whether some position titles are only fractionally in a lab (not the case for AURA)
L1P5_POSITION_TITLES = [
    {"name": _L1P5_PT_RESEARCHER, "quotity": 1},
    {"name": _L1P5_PT_STUDENTPOSTDOC, "quotity": 1},
    {"name": _L1P5_PT_ADMINISTRATION, "quotity": 1},
    {"name": _L1P5_PT_MANAGER, "quotity": 1},
    {"name": _L1P5_PT_EDUPUBOUT, "quotity": 1},
    {"name": _L1P5_PT_SOFTWARE_ENG, "quotity": 1},
    {"name": _L1P5_PT_TECHNICAL_ENG, "quotity": 1},
    {"name": _L1P5_PT_TECHNICAL_SCI, "quotity": 1},
]

# The following are the mandatory values that will be read and sent to the frontend

# (MANDATORY) survey might use a superset of labels
# this mapping is used to correctly computed the number of people in each group (no grouping for AURA)
L1P5_CF_LABEL_MAPPING = {
    _L1P5_PT_RESEARCHER: _L1P5_CF_RESEARCHER,
    _L1P5_PT_STUDENTPOSTDOC: _L1P5_CF_STUDENTPOSTDOC,
    _L1P5_PT_ADMINISTRATION: _L1P5_CF_ADMINISTRATION,
    _L1P5_PT_MANAGER: _L1P5_CF_MANAGER,
    _L1P5_PT_EDUPUBOUT: _L1P5_CF_EDUPUBOUT,
    _L1P5_PT_SOFTWARE_ENG: _L1P5_CF_SOFTWARE_ENG,
    _L1P5_PT_TECHNICAL_ENG: _L1P5_CF_TECHNICAL_ENG,
    _L1P5_PT_TECHNICAL_SCI: _L1P5_CF_TECHNICAL_SCI,
}

# (MANDATORY) travels uses it's own labels
# NOTE(msimonin): There's a remaining convention with the frontend regarding the UNKNOWN="unknown" label
L1P5_TRAVEL_POSITION_LABELS = [
    _L1P5_TRAVEL_RESEARCHER,
    _L1P5_TRAVEL_STUDENTPOSTDOC,
    _L1P5_TRAVEL_ADMINISTRATION,
    _L1P5_TRAVEL_MANAGER,
    _L1P5_TRAVEL_EDUPUBOUT,
    _L1P5_TRAVEL_SOFTWARE_ENG,
    _L1P5_TRAVEL_TECHNICAL_ENG,
    _L1P5_TRAVEL_TECHNICAL_SCI,
    _L1P5_TRAVEL_GUEST,
    _L1P5_TRAVEL_UNKNOWN,
]

# (MANDATORY) mapping from string to travels position labels used to identify entries in the list of travels (manual entries or CSV upload)
# By default the identity is generated but this can be augmented here
# For AURA, we only allow one string per traveler type, in each language, which should match the i18n at the end of this file
L1P5_TRAVEL_POSITION_DICTIONARY = {
    "researcher": _L1P5_TRAVEL_RESEARCHER,
    "chercheur": _L1P5_TRAVEL_RESEARCHER,
    "postdoc / student": _L1P5_TRAVEL_STUDENTPOSTDOC,
    "doctorant / postdoctorant": _L1P5_TRAVEL_STUDENTPOSTDOC,
    "administration / services": _L1P5_TRAVEL_ADMINISTRATION,
    "manager / executive": _L1P5_TRAVEL_MANAGER,
    "management / direction": _L1P5_TRAVEL_MANAGER,
    "education / public outreach": _L1P5_TRAVEL_EDUPUBOUT,
    "education / sensibilisation public": _L1P5_TRAVEL_EDUPUBOUT,
    "software engineer": _L1P5_TRAVEL_SOFTWARE_ENG,
    "ingenieur software": _L1P5_TRAVEL_SOFTWARE_ENG,
    "technician / technical engineer": _L1P5_TRAVEL_TECHNICAL_ENG,
    "ingenieur technique / technicien": _L1P5_TRAVEL_TECHNICAL_ENG,
    "technical scientist": _L1P5_TRAVEL_TECHNICAL_SCI,
    "scientifique technique": _L1P5_TRAVEL_TECHNICAL_SCI,
    "guest": _L1P5_TRAVEL_GUEST,
    "invite": _L1P5_TRAVEL_GUEST,
    "unknown": _L1P5_TRAVEL_UNKNOWN,
    "inconnu": _L1P5_TRAVEL_UNKNOWN,
    "": _L1P5_TRAVEL_UNKNOWN,
}

## Travel: categories for purposes (always gives an "unknown")
_L1P5_TRAVEL_PURPOSE_RELOCATION = "employee.relocation"
_L1P5_TRAVEL_PURPOSE_DATACOLLECTION = "research.data.collection"
_L1P5_TRAVEL_PURPOSE_CONFERENCE = "conference"
_L1P5_TRAVEL_PURPOSE_TEACHING = "teaching"
_L1P5_TRAVEL_PURPOSE_COLLABORATION = "collaboration"
_L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT = "research.management"
_L1P5_TRAVEL_PURPOSE_TECH = "tech.eng.dev"
_L1P5_TRAVEL_PURPOSE_OTHER = "other"
_L1P5_TRAVEL_PURPOSE_UNKNOWN = "unknown"

# Labels for travel purposes
L1P5_TRAVEL_PURPOSE_LABELS = [
    _L1P5_TRAVEL_PURPOSE_RELOCATION,
    _L1P5_TRAVEL_PURPOSE_DATACOLLECTION,
    _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    _L1P5_TRAVEL_PURPOSE_TEACHING,
    _L1P5_TRAVEL_PURPOSE_COLLABORATION,
    _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    _L1P5_TRAVEL_PURPOSE_TECH,
    _L1P5_TRAVEL_PURPOSE_OTHER,
    _L1P5_TRAVEL_PURPOSE_UNKNOWN,
]

# (MANDATORY) mapping from string to travel purposes labels used to identify entries in the list of travels (manual entries or CSV upload)
# By default the identity is generated but this can be augmented here
# For AURA, we only allow one string per travel purpose, in each language, which should match the i18n at the end of this file
L1P5_TRAVEL_PURPOSE_DICTIONARY = {
    "employee relocation": _L1P5_TRAVEL_PURPOSE_RELOCATION,
    "demenagement": _L1P5_TRAVEL_PURPOSE_RELOCATION,
    "data collection": _L1P5_TRAVEL_PURPOSE_DATACOLLECTION,
    "collected de donnees": _L1P5_TRAVEL_PURPOSE_DATACOLLECTION,
    "conference / workshop": _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    "colloque / congres / seminaire / atelier": _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    "teaching": _L1P5_TRAVEL_PURPOSE_TEACHING,
    "enseignement": _L1P5_TRAVEL_PURPOSE_TEACHING,
    "collaboration / visit": _L1P5_TRAVEL_PURPOSE_COLLABORATION,
    "collaboration / visite": _L1P5_TRAVEL_PURPOSE_COLLABORATION,
    "research management": _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    "administration de la recherche": _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    "maintenance / development / testing": _L1P5_TRAVEL_PURPOSE_TECH,
    "maintenance / développement / test technique": _L1P5_TRAVEL_PURPOSE_TECH,
    "other": _L1P5_TRAVEL_PURPOSE_OTHER,
    "autre": _L1P5_TRAVEL_PURPOSE_OTHER,
    "unknown": _L1P5_TRAVEL_PURPOSE_UNKNOWN,
    "inconnu": _L1P5_TRAVEL_PURPOSE_UNKNOWN,
    "": _L1P5_TRAVEL_PURPOSE_UNKNOWN,
}

# Specific translation to send to the frontend
# Note: since those are used in the documentation/help, they will list the authorized categories for travel purposes and employee types, which therefore should at least be included in the two dictionaries above
L1P5_I18N = {
    "en": {
        _L1P5_PT_RESEARCHER: "Researcher",
        _L1P5_PT_STUDENTPOSTDOC: "Postdoc / Student",
        _L1P5_PT_ADMINISTRATION: "Administration / Services",
        _L1P5_PT_MANAGER: "Manager / Executive",
        _L1P5_PT_EDUPUBOUT: "Education / Public Outreach",
        _L1P5_PT_SOFTWARE_ENG: "Software Engineer",
        _L1P5_PT_TECHNICAL_ENG: "Technician / Technical Engineer",
        _L1P5_PT_TECHNICAL_SCI: "Technical Scientist",
        # survey
        _L1P5_CF_RESEARCHER: "a researcher or a professor",
        _L1P5_CF_RESEARCHER + ".short": "Professors / Researchers",
        _L1P5_CF_STUDENTPOSTDOC: "a PhD student or a Post-Doc",
        _L1P5_CF_STUDENTPOSTDOC + ".short": "PhD / Post-Doc",
        _L1P5_CF_ADMINISTRATION: "an administration or services employee",
        _L1P5_CF_ADMINISTRATION + ".short": "Admin / Services",
        _L1P5_CF_MANAGER: "a manager or executive",
        _L1P5_CF_MANAGER + ".short": "Mngr / Exec",
        _L1P5_CF_EDUPUBOUT: "an education, public outreach specialist",
        _L1P5_CF_EDUPUBOUT + ".short": "EPO",
        _L1P5_CF_SOFTWARE_ENG: "a software engineer",
        _L1P5_CF_SOFTWARE_ENG + ".short": "SwEng",
        _L1P5_CF_TECHNICAL_ENG: "a technician or technical engineer",
        _L1P5_CF_TECHNICAL_ENG + ".short": "TechEng",
        _L1P5_CF_TECHNICAL_SCI: "a technical scientist",
        _L1P5_CF_TECHNICAL_SCI + ".short": "TechSci",
        # travel position labels
        _L1P5_TRAVEL_RESEARCHER: "Researcher",
        _L1P5_TRAVEL_STUDENTPOSTDOC: "Student / Post-Doc",
        _L1P5_TRAVEL_ADMINISTRATION: "Administration / Services",
        _L1P5_TRAVEL_MANAGER: "Manager / Executive",
        _L1P5_TRAVEL_EDUPUBOUT: "Education / Public Outreach",
        _L1P5_TRAVEL_SOFTWARE_ENG: "Software Engineer",
        _L1P5_TRAVEL_TECHNICAL_ENG: "Technician / Technical Engineer",
        _L1P5_TRAVEL_TECHNICAL_SCI: "Technical Scientist",
        _L1P5_TRAVEL_GUEST: "Guest",
        _L1P5_TRAVEL_UNKNOWN: "Unknown",
        # travel purpose labels
        _L1P5_TRAVEL_PURPOSE_RELOCATION: "Employee Relocation",
        _L1P5_TRAVEL_PURPOSE_DATACOLLECTION: "Data Collection",
        _L1P5_TRAVEL_PURPOSE_CONFERENCE: "Conference / Workshop",
        _L1P5_TRAVEL_PURPOSE_TEACHING: "Teaching",
        _L1P5_TRAVEL_PURPOSE_COLLABORATION: "Collaboration / Visit",
        _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT: "Research Management",
        _L1P5_TRAVEL_PURPOSE_TECH: "Maintenance / Development / Testing",
        _L1P5_TRAVEL_PURPOSE_OTHER: "Other",
        _L1P5_TRAVEL_PURPOSE_UNKNOWN: "Unknown",
    },
    "fr": {
        _L1P5_PT_RESEARCHER: "Chercheur.e.s",
        _L1P5_PT_STUDENTPOSTDOC: "Doctorants / Postdoctorants",
        _L1P5_PT_ADMINISTRATION: "Administration / Services",
        _L1P5_PT_MANAGER: "Management / Direction",
        _L1P5_PT_EDUPUBOUT: "Éducation / Sensibilisation du Public",
        _L1P5_PT_SOFTWARE_ENG: "Ingénieur Software",
        _L1P5_PT_TECHNICAL_ENG: "Ingénieur Technique / Technicien",
        _L1P5_PT_TECHNICAL_SCI: "Scientifique Technique",
        # survey
        _L1P5_CF_RESEARCHER: "chercheur.e ou enseignant.e chercheur.e",
        _L1P5_CF_RESEARCHER + ".short": "Enseignant.e / Chercheur.e",
        _L1P5_CF_STUDENTPOSTDOC: "doctorant.e ou postdoctorant.e",
        _L1P5_CF_STUDENTPOSTDOC + ".short": "Doc / Post-Doc",
        _L1P5_CF_ADMINISTRATION: "personnel d'administration et de services",
        _L1P5_CF_ADMINISTRATION + ".short": "Admin / Services",
        _L1P5_CF_MANAGER: "managenement et direction",
        _L1P5_CF_MANAGER + ".short": "Mgmt / Dir",
        _L1P5_CF_EDUPUBOUT: "Éducation et Sensibilisation du Public",
        _L1P5_CF_EDUPUBOUT + ".short": "ESP",
        _L1P5_CF_SOFTWARE_ENG: "ingénieur software",
        _L1P5_CF_SOFTWARE_ENG + ".short": "IngSoft",
        _L1P5_CF_TECHNICAL_ENG: "ingénieur technique et technicien",
        _L1P5_CF_TECHNICAL_ENG + ".short": "IngTech",
        _L1P5_CF_TECHNICAL_SCI: "scientifique technique",
        _L1P5_CF_TECHNICAL_SCI + ".short": "SciTech",
        # travel position labels
        _L1P5_TRAVEL_RESEARCHER: "Chercheur",
        _L1P5_TRAVEL_STUDENTPOSTDOC: "Doctorant / Postdoctorant",
        _L1P5_TRAVEL_ADMINISTRATION: "Administration / Services",
        _L1P5_TRAVEL_MANAGER: "Management / Direction",
        _L1P5_TRAVEL_EDUPUBOUT: "Éducation / Sensibilisation Public",
        _L1P5_TRAVEL_SOFTWARE_ENG: "Ingénieur Software",
        _L1P5_TRAVEL_TECHNICAL_ENG: "Ingénieur Technique / Technicien",
        _L1P5_TRAVEL_TECHNICAL_SCI: "Scientifique Technique",
        _L1P5_TRAVEL_GUEST: "Invité",
        _L1P5_TRAVEL_UNKNOWN: "Inconnu",
        # travel purpose labels
        _L1P5_TRAVEL_PURPOSE_RELOCATION: "Déménagement",
        _L1P5_TRAVEL_PURPOSE_DATACOLLECTION: "Collecte de données",
        _L1P5_TRAVEL_PURPOSE_CONFERENCE: "Colloque / Congrès / Séminaire / Atelier",
        _L1P5_TRAVEL_PURPOSE_TEACHING: "Enseignement",
        _L1P5_TRAVEL_PURPOSE_COLLABORATION: "Collaboration / Visite",
        _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT: "Administration de la recherche",
        _L1P5_TRAVEL_PURPOSE_TECH: "Maintenance / développement / test technique",
        _L1P5_TRAVEL_PURPOSE_OTHER: "Autre",
        _L1P5_TRAVEL_PURPOSE_UNKNOWN: "Inconnu",
    },
}
