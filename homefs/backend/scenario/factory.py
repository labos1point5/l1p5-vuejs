from django.apps import apps
from django.contrib.contenttypes.models import ContentType

from backend.utils import get_entity_type

from ..core.models import Settings
from .models import Scenario


def scenario_filter(**kwargs):
    EntityClass = get_entity_type()
    return Scenario.objects.filter(
        **kwargs,
        ghgi__content_type=ContentType.objects.get_for_model(EntityClass).id,
    )
