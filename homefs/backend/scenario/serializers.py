from rest_framework import serializers
from .models import Scenario, Rule
from ..carbon.serializers import GHGISerializer

class RuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rule
        fields = '__all__'

class ScenarioSerializer(serializers.ModelSerializer):
    rules = RuleSerializer(many=True, read_only=True)
    ghgi = GHGISerializer(many=False)
    class Meta:
        model = Scenario
        fields = '__all__'
