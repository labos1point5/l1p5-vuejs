"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.urls import path
from . import views as scenarioviews

URL_PATTERNS = [
    path("api/save_scenario/", scenarioviews.save_scenario, name="save_scenario"),
    path("api/get_scenario/", scenarioviews.get_scenario, name="get_scenario"),
    path("api/delete_scenario/", scenarioviews.delete_scenario, name="delete_scenario"),
    path("api/get_scenarios/", scenarioviews.get_scenarios, name="get_scenarios"),
    path(
        "api/get_scenario_from_uuid/",
        scenarioviews.get_scenario_from_uuid,
        name="get_scenario_from_uuid",
    ),
    path(
        "api/get_scenarios_admin/",
        scenarioviews.get_scenarios_admin,
        name="get_scenarios_admin",
    ),
    path(
        "api/update_scenario_synthesis/",
        scenarioviews.update_synthesis,
        name="update_scenario_synthesis",
    ),
]
