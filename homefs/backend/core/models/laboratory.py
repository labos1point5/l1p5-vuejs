from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation

from .core import AbstractEntity


class Administration(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Discipline(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Laboratory(AbstractEntity):
    administrations = models.ManyToManyField(
        Administration, related_name="laboratories"
    )
    disciplines = models.ManyToManyField(Discipline, through="LaboratoryDiscipline")

    actions = GenericRelation(
        "transition.PublicAction",
        related_name="entity",
        related_query_name="thelaboratory",
    )


class LaboratoryDiscipline(models.Model):
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    laboratory = models.ForeignKey(Laboratory, on_delete=models.CASCADE)
    percentage = models.IntegerField()


def laboratory_update_or_create(data):
    administrations_data = data.pop("administrations")
    disciplines_data = data.pop("disciplines")
    # build the entity object if exists or create it
    entity = Laboratory.objects.filter(referent=data["referent"])
    lid = None
    if len(entity) == 1:
        lid = entity[0].id
    entity, _ = Laboratory.objects.update_or_create(id=lid, defaults=data)
    entity.administrations.clear()
    for administration_data in administrations_data:
        if type(administration_data) is str:
            # Use case insensitive test to check if administration exists or not
            if Administration.objects.filter(name__iexact=administration_data).exists():
                administration = Administration.objects.get(
                    name__iexact=administration_data
                )
                entity.administrations.add(administration)
            else:
                administration = Administration.objects.create(name=administration_data)
                entity.administrations.add(administration)
        else:
            administration = Administration.objects.get(id=administration_data["id"])
            entity.administrations.add(administration)
    entity.disciplines.clear()
    for discipline_data in disciplines_data:
        d, created = LaboratoryDiscipline.objects.update_or_create(
            discipline_id=discipline_data["id"],
            laboratory_id=entity.id,
            percentage=discipline_data["percentage"],
        )
    return entity
