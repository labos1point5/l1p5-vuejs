from django.core.management.base import BaseCommand, CommandParser
from rest_framework.renderers import JSONRenderer

from backend.carbon.fake import get_or_set_fakeGHGI
from backend.carbon.serializers import GHGISerializer
from backend.carbon.utils import serialize_ghgi_with_consumptions


class Command(BaseCommand):
    help = "Manage administrations"

    def add_arguments(self, parser: CommandParser) -> None:
        subparsers = parser.add_subparsers(
            title="subcommand",
            description="subcommand",
            help="fakeGHGI subcommands",
        )
        mv_parser = subparsers.add_parser("generate", help="Generate a new fakeGHGI")
        mv_parser.set_defaults(func=Command.generate)

        prune_parser = subparsers.add_parser(
            "dumps", help="Dumps the fakeGHGI on the screen"
        )
        prune_parser.set_defaults(func=Command.dumps)

    @staticmethod
    def generate(**options):
        get_or_set_fakeGHGI(force=True)

    @staticmethod
    def dumps(**options):
        g = get_or_set_fakeGHGI(force=False)
        r = serialize_ghgi_with_consumptions(g)
        data = JSONRenderer().render(r)
        print(data.decode())

    def handle(self, *args, **options):
        if "func" in options:
            options["func"](**options)
