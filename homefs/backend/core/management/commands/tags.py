from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Bulk create some tags for a lab (given by the referent email)"

    def add_arguments(self, parser):

        parser.add_argument("email", type=str)

        parser.add_argument(
            "category",
            help="category of the tags",
        )
        parser.add_argument(
            "--tags",
            nargs="+",
            help="One or more tags(make sure they don't contains space :) )",
        )

    def handle(self, *app_labels, **options):
        email = options["email"]
        category = options["category"]

        tags = options["tags"]
        # first create the category if needed
        from backend.core.models import TagCategory, Laboratory, Tag

        laboratory = Laboratory.objects.get(referent__email=email)
        TagCategory.objects.filter(laboratory=laboratory, name=category).delete()
        category, _ = TagCategory.objects.update_or_create(
            laboratory=laboratory,
            name=category,
            defaults=dict(description="auto generated", color="#000"),
        )

        for tag in tags:
            t, _ = Tag.objects.update_or_create(
                name=tag, defaults=dict(category=category)
            )
