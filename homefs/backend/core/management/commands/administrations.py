from django.core.management.base import BaseCommand, CommandError, CommandParser

from django.shortcuts import get_object_or_404

from backend.core.models import Administration


class Command(BaseCommand):
    help = "Manage administrations"

    def add_arguments(self, parser: CommandParser) -> None:
        subparsers = parser.add_subparsers(
            title="subcommand",
            description="subcommand",
            help="administration subcommands",
        )
        mv_parser = subparsers.add_parser("mv", help="Change an administration name to another, remove the old one")
        mv_parser.add_argument("old", type=str)
        mv_parser.add_argument("new", type=str)
        mv_parser.set_defaults(func=Command.mv)

    @staticmethod
    def mv(old=None, new=None, **kwargs):
        """Move an old administration label to a new one.
        
        Remove the old administration. Make sure the admnistrations of the
        corresponding labs are changed acordingly.
        
        Args:
            old: label to move (fails if it doensn't exist)
            new: target label. This label will be created if it doesn't exist.

        
        """
        Administration.objects.get(name=old)
        # get old adm
        adm_old = get_object_or_404(Administration, name=old)
        adm_new, _ = Administration.objects.get_or_create(name=new)

        for lab in adm_old.laboratories.iterator():
            # lab has adm_old as administrations
            lab.administrations.add(adm_new)
            lab.save()

        adm_old.delete()

    def handle(self, *args, **options):
        if "func" in options:
            options["func"](**options)
