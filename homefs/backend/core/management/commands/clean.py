from django.core.management.base import BaseCommand, CommandError
from backend.carbon.models import GHGI
from backend.core.models import TagCategory


class Command(BaseCommand):
    help = "Nettoie la base de données"

    def add_arguments(self, parser):
        parser.add_argument(
            "--yesiknowwhatiamdoing",
            action="store_true",
            help="Confirmation pour exécuter la commande de nettoyage",
        )

    def handle(self, **options):
        if not options["yesiknowwhatiamdoing"]:
            raise CommandError(
                "Vous devez ajouter l'option --yesiknowwhatiamdoing pour exécuter cette commande."
            )

        GHGI.objects.all().delete()
        TagCategory.objects.all().delete()

        self.stdout.write(
            self.style.SUCCESS("Nettoyage de la base de données terminé.")
        )
