from django.contrib import admin

from .models.laboratory import Discipline, Laboratory, LaboratoryDiscipline


@admin.register(Laboratory)
class LaboratoryAdmin(admin.ModelAdmin):
    pass
