# Generated by Django 3.2.7 on 2024-05-28 14:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_entity_for_tagcategory'),
    ]

    operations = [
        migrations.CreateModel(
            name='LaboratoryBoundary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nResearcher', models.IntegerField(null=True)),
                ('nProfessor', models.IntegerField(null=True)),
                ('nEngineer', models.IntegerField(null=True)),
                ('nStudent', models.IntegerField(null=True)),
                ('budget', models.IntegerField(null=True)),
            ],
        ),
    ]
