from django.db import migrations, models
import django.db.models.deletion

def entity_for_tagcategory(apps, schema_director):
    TagCategory = apps.get_model("core", "TagCategory")
    Laboratory = apps.get_model("core", "Laboratory")
    ContentType = apps.get_model("contenttypes", "contenttype")
    content_type = ContentType.objects.get_for_model(Laboratory)
    for category in TagCategory.objects.all():
        category.content_type = content_type
        category.object_id = category.laboratory.id
        category.save()

class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('core', '0008_auto_20240505_0801')
    ]

    operations = [
        migrations.AddField(
            model_name='TagCategory',
            name='content_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype'),
        ),
        migrations.AddField(
            model_name='TagCategory',
            name='object_id',
            field=models.PositiveIntegerField(),
        ),
        migrations.RunPython(entity_for_tagcategory),
        migrations.RemoveField(
            model_name='TagCategory',
            name='laboratory',
        ),
    ]
