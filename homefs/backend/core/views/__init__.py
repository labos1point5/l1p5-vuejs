from django.http import Http404

from .core import (
    index_view,
    get_settings,
    save_settings,
    save_entity,
    get_entity,
    update_or_create_tag_category,
    delete_tag_category,
    get_all_tag_categories,
)

from .laboratory import get_administrations, get_disciplines
