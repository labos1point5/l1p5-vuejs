from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.contrib.contenttypes.models import ContentType
from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

import copy

from backend.utils import get_entity_or_404
from backend.core.models.core import PositionTitle
from backend.core.serializers.core import PositionTitleSerializer
from ...utils import entity_get

from ..serializers import GenericEntitySerializer

from ..models import (
    Settings,
    TagCategory,
    Tag,
    Entity,
    entity_update_or_create,
)


from ..serializers import (
    SettingsSerializer,
    TagCategorySerializer,
)

# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name="index.html"))


def settings_response():
    s = Settings.objects.all()
    # send back some static information
    all_settings = dict(settings=SettingsSerializer(s, many=True).data)

    return all_settings


@api_view(["GET"])
@permission_classes([])
def get_settings(request):
    response = settings_response()
    return Response(response, status=status.HTTP_200_OK)


@api_view(["POST"])
def save_settings(request):
    settings = request.data.pop("settings")
    for setting in settings:
        s, created = Settings.objects.update_or_create(
            id=setting["id"], defaults=setting
        )
    settings = Settings.objects.all()
    serializer = SettingsSerializer(settings, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


# FIXME(msimonin): test permissions
@api_view(["POST"])
def save_entity(request):
    if request.user.is_authenticated:
        entity = entity_update_or_create(request.data, request.user)
        serializer = GenericEntitySerializer(entity)
        to_return = copy.deepcopy(serializer.data)
        return Response(to_return, status=status.HTTP_201_CREATED)
    return Response(Entity.objects.none())


@api_view(["GET"])
def get_entity(request):
    if request.user.is_authenticated:
        entity = entity_get(referent=request.user)

        if entity is None:
            # FIXME(msimonin): 404
            return Response(None)

        serializer = GenericEntitySerializer(entity)
        to_return = copy.deepcopy(serializer.data)
        return Response(to_return)

    # FIXME(msimonin): add permission classes
    return Response(None, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def update_or_create_tag_category(request):
    data = request.data.copy()
    category_id = data.pop("id", None)
    tags = data.pop("tags")

    entity = get_entity_or_404(referent=request.user)
    if entity.referent != request.user:
        raise PermissionDenied()

    data["entity"] = entity
    category, _ = TagCategory.objects.update_or_create(id=category_id, defaults=data)
    tag_ids = []
    for tag_data in tags:
        tag_id = tag_data.pop("id", None)
        tag_data["category"] = category
        tag, _ = Tag.objects.update_or_create(id=tag_id, defaults=tag_data)
        tag_ids.append(tag.id)
    Tag.objects.filter(category_id=category.id).exclude(id__in=tag_ids).delete()
    serializer = TagCategorySerializer(category)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def delete_tag_category(request):
    data = request.data.copy()
    category_id = data.pop("id")
    category = TagCategory.objects.get(id=category_id)
    if category.entity.referent != request.user:
        raise PermissionDenied()
    category.delete()
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_all_tag_categories(request):
    entity = get_entity_or_404(referent=request.user)
    if entity.referent != request.user:
        raise PermissionDenied()

    tagCategories = TagCategory.objects.filter(
        object_id=entity.id,
        content_type=ContentType.objects.get_for_model(entity).id,
    )
    serializer = TagCategorySerializer(tagCategories, many=True)
    return Response(serializer.data)
