from rest_framework import serializers

from backend.utils import get_entity_type
from . import EntitySerializer

from ..models import Laboratory, Entity
from .laboratory import LaboratorySerializer


def _entitySerialize(entity):
    if isinstance(entity, Entity):
        serializer = EntitySerializer(entity)
    elif isinstance(entity, Laboratory):
        serializer = LaboratorySerializer(entity)
    else:
        raise ValueError("Unexpected entity object")
    return {"type": entity.__class__.__name__, "data": serializer.data}


# https://www.django-rest-framework.org/api-guide/relations/#generic-relationships
class EntityRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        return _entitySerialize(value)


class GenericEntitySerializer(serializers.Serializer):
    def to_representation(self, instance):
        return _entitySerialize(instance)
