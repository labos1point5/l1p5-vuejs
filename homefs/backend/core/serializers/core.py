from rest_framework import serializers

from backend.core.models.core import Boundary, Location, Members, PositionTitle

from ..models import (
    Settings,
    Entity,
    TagCategory,
    Tag,
)
from ...users.serializers import L1P5UserSerializer


class SettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settings
        fields = "__all__"


class EntitySerializer(serializers.ModelSerializer):
    referent = L1P5UserSerializer(many=False)

    class Meta:
        model = Entity
        fields = "__all__"


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ["id", "name", "color", "category"]


class TagCategorySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)

    class Meta:
        model = TagCategory
        fields = "__all__"


class TaggedItemSerializer(serializers.RelatedField):
    def to_representation(self, value):
        return TagSerializer(value.tag).data


class PositionTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositionTitle
        fields = "__all__"


class MembersSerializer(serializers.ModelSerializer):
    position = PositionTitleSerializer()

    class Meta:
        model = Members
        fields = "__all__"


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = "__all__"


class BoundarySerializer(serializers.ModelSerializer):
    members = MembersSerializer(many=True)
    locations = LocationSerializer(many=True)

    class Meta:
        model = Boundary
        fields = "__all__"
