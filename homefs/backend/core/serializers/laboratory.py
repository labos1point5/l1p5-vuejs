from rest_framework import serializers

from backend.core.models import (
    Administration,
    Discipline,
    Laboratory,
    LaboratoryDiscipline,
)

from backend.users.serializers import L1P5UserSerializer


class AdministrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Administration
        fields = "__all__"


class DisciplineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discipline
        fields = "__all__"


class LaboratoryDisciplineSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source="discipline.id")
    name = serializers.ReadOnlyField(source="discipline.name")

    class Meta:
        model = LaboratoryDiscipline
        fields = ["id", "name", "percentage"]


class LaboratorySerializer(serializers.ModelSerializer):
    administrations = AdministrationSerializer(many=True)
    referent = L1P5UserSerializer(many=False)
    disciplines = LaboratoryDisciplineSerializer(
        source="laboratorydiscipline_set", many=True
    )

    class Meta:
        model = Laboratory
        fields = "__all__"
