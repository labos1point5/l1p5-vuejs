import datetime
import logging
from typing import Optional

from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.contrib.auth.decorators import permission_required
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError
from django.http import FileResponse
from django.shortcuts import get_object_or_404

from backend.utils import entity_get
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.pagination import PageNumberPagination

from backend.transition.utils import (
    can_act,
    can_get,
    notify_publish_action,
    notify_reviewer_add_message,
    notify_submit_action,
    notify_user_add_message,
    related_query_entity,
)
from backend.utils import entity_filter
from backend.utils import get_entity_or_404, get_entity_type
from backend.core.models.core import AbstractEntity
from backend.core.models.laboratory import Laboratory

from ..carbon.models import GHGI

from ..users.models import L1P5User

from .models import (
    QUOTA_ACTION,
    QUOTA_GLOBAL,
    Message,
    PublicAction,
    PublicActionFile,
    Tag,
    global_quota,
    global_quota_empty,
    validate_filename,
    CAN_REVIEW,
    DRAFT,
    SUBMITTED,
    PUBLISHED,
)
from .serializers import (
    GenericSimpleEntitySerializer,
    MessageSerializer,
    PublicActionSerializer,
    SearchActionSerializer,
)


logger = logging.getLogger(__name__)


def build_files_to_save(files, global_usage, action_usage):
    # filter out some files
    # This is done silently for now
    current_global_usage = global_usage
    current_usage = action_usage
    files_to_save = []
    for (name, file) in files.items():
        try:
            validate_filename(name)
            current_global_usage += file.size
            current_usage += file.size
            if current_usage > QUOTA_ACTION:
                raise ValidationError("Quota exceeded")
            if current_global_usage > QUOTA_GLOBAL:
                raise ValidationError("Quota exceeded")
        except ValidationError as e:
            print(f"Skipping {name} : ", e)
            continue
        files_to_save.append((name, file))

    return files_to_save


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_count(request):
    entity = get_entity_or_404(referent=request.user)
    qe = related_query_entity(get_entity_type())
    count = PublicAction.objects.filter(**{qe("id"): entity.id}).count()
    return Response(dict(count=count), status=status.HTTP_200_OK)


@api_view(["GET"])
@renderer_classes([JSONRenderer])
@permission_classes([IsAuthenticated])
def get_transition_quota(request):
    try:
        entity = get_entity_or_404(referent=request.user)
        data = global_quota(entity)
    except Exception as e:
        logger.error(e)
        # don't block but return an empty quota
        return Response(global_quota_empty(), status=status.HTTP_200_OK)
    return Response(data, status=status.HTTP_200_OK)


def _update_or_create(entity: AbstractEntity, action: Optional[dict]):
    """update or create an action.

    Beware on the side-effects on action

    Bonus: no-op for empty action dict
    """
    action_id = action.pop("id", None)
    # strip any lab_id
    action.pop("entity", None)
    # and reviewer (as this is used as shortcut when sending data to the frontend)
    action.pop("reviewer", None)
    # this doesn't call save so resetting the admin status manually
    action.update(admin_status=DRAFT)

    # inject the entity
    action.update(entity=entity)

    # update the available tag list
    tags = set()
    tag_descriptors = action.pop("tags", [])
    for descriptor in tag_descriptors:
        tag, _ = Tag.objects.get_or_create(descriptor=descriptor)
        tags.add(tag)

    new_action, _ = PublicAction.objects.update_or_create(id=action_id, defaults=action)
    new_action.tags.set(tags)

    return new_action


@api_view(["GET", "POST"])
@permission_classes([IsAuthenticated])
def mgmt_public_actions(request):
    entity = entity_get(referent=request.user)
    # FIXME(msimonin): should we get a 404 here
    # I think we kept on using 200 for some compatibility with the frontend code
    if entity is None:
        return Response(None, status=status.HTTP_200_OK)
    if request.method == "GET":
        actions = PublicAction.objects.prefetch_related(
            "entity", "tags", "files"
        ).filter(object_id=entity.id)
        serializer = PublicActionSerializer(
            actions, many=True, context={"request": None}
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "POST":
        new_action = _update_or_create(entity, request.data.copy())
        serializer = PublicActionSerializer(new_action, context={"request": None})
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET", "POST", "DELETE"])
@permission_classes([IsAuthenticated])
def mgmt_public_action(request, action_id):

    a = PublicAction.objects.get(id=action_id)
    if not can_get(request.user, a, allow_public=True):
        raise PermissionDenied()

    if request.method == "DELETE" and can_act(request.user, a):
        a.delete()
        return Response(None, status=status.HTTP_200_OK)

    if request.method == "POST" and can_act(request.user, a):
        # submit the action
        a.admin_status = SUBMITTED
        # reset the reminder if any
        # so that it can appear in the reviewer actions list again
        a.reminder = None
        a.save(update_fields=["admin_status"])
        notify_submit_action(request, a)
        serializer = PublicActionSerializer(a, context={"request": None})
        return Response(serializer.data)

    if request.method == "GET":
        serializer = PublicActionSerializer(a, context={"request": None})
        return Response(serializer.data, status=status.HTTP_200_OK)

    raise PermissionDenied()


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def mgmt_public_files(request, action_id):
    files = request.FILES
    # get the action corresponding to the action id and owned transition lab
    # this should be sufficient to check that we're modifying a owned action

    action = PublicAction.objects.get(id=action_id)

    if not can_act(request.user, action):
        return Response(None, status=status.HTTP_403_FORBIDDEN)

    files_to_save = build_files_to_save(
        files, global_quota(action.entity)["current"], action.quota
    )
    try:
        for (name, file) in files_to_save:
            # FIXME(msimonin): this creates a copy on the fs with a different suffix
            # that's the expected behaviour of the FileSystemStorage (generate a
            # new name if a file with the same name already exists)
            af, _ = PublicActionFile.objects.update_or_create(
                action=action, name=name, defaults=dict(file=file)
            )
    except Exception as e:
        logger.error(e)
        return Response(
            "Something went wrong when uploading the files",
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    # refresh
    action = PublicAction.objects.get(id=action_id)
    serializer = PublicActionSerializer(action, context={"request": None})
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
@renderer_classes([JSONRenderer])
def reviewer_get_quota(request, entity_id):
    entity = get_entity_or_404(id=entity_id)
    if request.user != entity.referent and not request.user.has_perm(CAN_REVIEW):
        raise PermissionDenied
    data = global_quota(entity)
    return Response(data, status=status.HTTP_200_OK)


def get_delete_file(request, af):
    """Works for action file of public file"""
    if request.method == "GET" and can_get(request.user, af, allow_public=True):
        # return the blob with correct content type
        # this might throw if the file isn't found,
        # we catch all error here and return a 404
        try:
            return FileResponse(af.file.open())
        except Exception as e:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

    if not request.user.is_authenticated:
        return Response(None, status=status.HTTP_401_UNAUTHORIZED)

    if request.method == "DELETE" and can_act(request.user, af):
        af.delete()
        return Response(None, status=status.HTTP_200_OK)

    raise PermissionDenied()


@api_view(["GET", "DELETE"])
@permission_classes([])
def mgmt_public_file(request, action_id, file_id):
    af = None
    try:
        af = PublicActionFile.objects.get(id=file_id)
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    assert af is not None

    # We fall here in several situations
    # esp. we looking a published action while being anonymous
    # because our hyper link serializer generates internal links only
    # so we need to check twice who can read the file
    return get_delete_file(request, af)


def filter_laboratory(request, items):
    """Specific filter for the laboratory entity.

    FIXME(msimonin): factorize this elsewhere in a central place
    for use in another apps.
    """
    disciplines = request.query_params.getlist("disciplines[]")
    disciplines_op = request.query_params.get("disciplinesOp", "AND")
    administrations = request.query_params.getlist("administrations[]")
    administrations_op = request.query_params.get("administrationsOp", "AND")

    if disciplines:
        if not disciplines_op or disciplines_op == "AND":
            for discipline in disciplines:
                items = items.filter(thelaboratory__disciplines__id=discipline)
        else:
            # there's at least one discipline
            q = Q(thelaboratory__disciplines__id=disciplines[0])
            for discipline in disciplines[1:]:
                q = q | Q(thelaboratory__disciplines__id=discipline)
            items = items.filter(q)
        items = items.distinct()

    if administrations:
        if not administrations_op or administrations_op == "AND":
            for administration in administrations:
                items = items.filter(
                    thelaboratory__administrations__name__icontains=administration
                )
        else:
            q = Q(thelaboratory__administrations__name__icontains=administrations[0])
            for administration in administrations:
                q = q | Q(
                    thelaboratory__administrations__name__icontains=administrations
                )
            items = items.filter(q)
        items = items.distinct()

    return items


def filter_items(cls, request):
    """
    Filter PublicActions according to their common fields:

    """
    tags = request.query_params.getlist("tags[]")
    tags_op = request.query_params.get("tagsOp", "AND")

    bounds = request.query_params.getlist("bounds[]")
    sort = request.query_params.get("sort", "sort:update:desc")
    status = request.query_params.get("status")
    title = request.query_params.get("title")

    entity_id = request.query_params.get("entity_id")

    entity_type = get_entity_type()

    qe = related_query_entity(entity_type)

    items = (
        # cls.objects.prefetch_related(
        #     "laboratory__disciplines", "laboratory__administrations", "tags", "files"
        # )
        # .prefetch_related("laboratory__referent__groups", "reviewer__groups")
        cls.objects.prefetch_related(
            "entity", "files", "tags", "reviewer__groups"
        ).filter(admin_status=PUBLISHED)
    )
    if tags:
        if not tags_op or tags_op == "AND":
            for tag in tags:
                items = items.filter(tags__descriptor=tag)
        else:
            q = Q(tags__descriptor=tags[0])
            for tag in tags:
                q = q | Q(tags__descriptor=tag)
            items = items.filter(q)
        items = items.distinct()

    if bounds:
        lat1, lon1, lat2, lon2 = bounds
        items = items.filter(
            **{
                qe("latitude__gte"): float(lat1),
                qe("longitude__gte"): float(lon1),
                qe("latitude__lte"): float(lat2),
                qe("longitude__lte"): float(lon2),
            }
        )

    if sort == "sort:creation:asc":
        items = items.order_by("created")
    elif sort == "sort:creation:desc":
        items = items.order_by("-created")
    elif sort == "sort:update:asc":
        items = items.order_by("last_update")
    elif sort == "sort:update:desc":
        items = items.order_by("-last_update")

    if status == "status:running":
        items = items.filter(Q(end__isnull=True) | Q(end__gte=datetime.datetime.now()))
    elif status == "status:ended":
        items = items.filter(Q(end__isnull=False) | Q(end__lte=datetime.datetime.now()))
    elif status == "status:abandoned":
        items = items.filter(abandoned=True)

    if title:
        items = items.filter(title__icontains=title)

    if entity_id:
        items = items.filter(**{qe("id"): entity_id})

    # further filter based on entity nature
    if entity_type == Laboratory:
        items = filter_laboratory(request, items)

    # get unique items at then end
    return items.distinct()


def reviewable_actions():
    return PublicAction.objects.prefetch_related("entity", "tags", "reviewer").filter(
        admin_status=SUBMITTED, reminder__isnull=True
    )


def reminded_actions():
    return (
        PublicAction.objects.prefetch_related("entity", "tags", "reviewer")
        .filter(reminder__isnull=False)
        .order_by("reminder")
    )


def published_actions():
    return PublicAction.objects.prefetch_related("entity", "tags", "reviewer").filter(
        admin_status=PUBLISHED
    )


@api_view(["GET"])
@permission_classes([])
@renderer_classes([JSONRenderer])
def public_actions_published(request):
    """Get the list of matching actions.

    This mainly used for displaying the search map/table.
    """

    # filters depends on the entity !
    actions = filter_items(PublicAction, request)

    paginator = PageNumberPagination()
    paginator.page_size = 1000
    result_page = paginator.paginate_queryset(actions, request)
    serializer = SearchActionSerializer(
        result_page, many=True, context={"request": None}
    )
    return paginator.get_paginated_response(serializer.data)


@api_view(["GET"])
@permission_classes([])
@renderer_classes([JSONRenderer])
def public_action_published(request, action_id):
    """Get one action published or not if the user has sufficient permissions."""

    action = get_object_or_404(PublicAction, id=action_id)
    if not can_get(request.user, action, allow_public=True):
        return Response(None, status=status.HTTP_403_FORBIDDEN)

    # all clear, proceed
    # in this case we want to generate only public links to the files
    action_serializer = PublicActionSerializer(action, context=dict(request=None))
    return Response(action_serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([])
@renderer_classes([JSONRenderer])
def public_entities(request):
    """Search public labs by name."""
    name = request.query_params.get("name", "")
    entities = entity_filter(
        actions__admin_status=PUBLISHED, name__icontains=name
    ).distinct()
    # take 10 first record only
    serializer = GenericSimpleEntitySerializer(entities.all()[0:10], many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@renderer_classes([JSONRenderer])
@permission_required([CAN_REVIEW], raise_exception=True)
def reviewer_actions_submitted(request):
    """Get all actions that have been submitted and no reminder set.
    This means that they are the actions that have to be reviewed currently
    """
    public_actions = (
        reviewable_actions()
        .prefetch_related(
            "files",
        )
        .order_by("reminder", "-last_update")
    )

    serializer = PublicActionSerializer(
        public_actions, many=True, context={"request": None}
    )

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_required([CAN_REVIEW], raise_exception=True)
@renderer_classes([JSONRenderer])
def reviewer_my_actions(request):
    data = request.data
    action_id = data.pop("id")
    action = get_object_or_404(PublicAction, id=action_id)
    action.reviewer = request.user
    # don't update the admin_status field
    action.save(update_fields=["admin_status"])
    serializer = PublicActionSerializer(action, context={"request": None})
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET", "POST", "PUT"])
@renderer_classes([JSONRenderer])
def reviewer_action(request, action_id):
    """Called when reviewing an action called by a reviewer or the action owner (PUT).

    - Toggle status(reviewer only)
    - Update an action while reviewing (user or reviewer)
        and this can be done on already published action
    - get the action (reviewer + user)
    """
    action = get_object_or_404(PublicAction, id=action_id)

    if request.method == "POST" and request.user.has_perm(CAN_REVIEW):
        # toggle status
        if action.admin_status == DRAFT:
            return Response(None, status=status.HTTP_403_FORBIDDEN)
        # toggle
        if action.admin_status == SUBMITTED:
            action.admin_status = PUBLISHED
        elif action.admin_status == PUBLISHED:
            action.admin_status = SUBMITTED
        action.save(update_fields=["admin_status"])
        notify_publish_action(request, action)
        serializer = PublicActionSerializer(action, context={"request": None})
        return Response(serializer.data)

    if request.method == "PUT" and can_act(request.user, action):
        admin_status = action.admin_status
        # get the entity of the action (must be set)
        new_action = _update_or_create(action.entity, request.data.copy())
        # if we aren' the owner (meaning we're reviewing other people action)
        if request.user != action.owner:
            new_action.admin_status = admin_status
            new_action.save(update_fields=["admin_status"])
        serializer = PublicActionSerializer(new_action, context={"request": None})
        return Response(serializer.data)

    if request.method == "GET" and can_get(request.user, action):
        serializer = PublicActionSerializer(action, context={"request": None})
        return Response(serializer.data)

    raise PermissionDenied()


@api_view(["GET"])
@permission_classes([])
def has_reviewer_permissions(request):
    if request.user.is_authenticated:
        user = L1P5User.objects.get(email=request.user)
        return Response({"has_reviewer_permissions": user.has_perm(CAN_REVIEW)})

    return Response({"has_reviewer_permissions": False})


@api_view(["GET"])
@permission_classes([])
def has_submitted_ghgis(request):
    """Check if the user has at least one submitted GHGI module."""

    if not request.user.is_authenticated:
        return Response({"has_submitted_ghgis": False})

    entity = entity_get(referent=request.user)
    if entity is None:
        return Response({"has_submitted_ghgis": False})
    result = False
    try:
        # get all ghgis and get the submitted modules
        ghgis = GHGI.objects.filter(object_id=entity.id)
        submitteds = [ghgi.submitted for ghgi in ghgis]
        # at least one ghgi has at least one submitted module
        result = any([any(s.values()) for s in submitteds if s is not None])
        return Response({"has_submitted_ghgis": result})
    except Exception as e:
        logger.error("Error when checking submitted ghgis:", e)
        result = False

    return Response({"has_submitted_ghgis": result})


@api_view(["GET"])
@permission_required([CAN_REVIEW], raise_exception=True)
def reviewer_reminders(request):
    """Get all action reminder."""
    actions = reminded_actions()
    serializer = PublicActionSerializer(actions, many=True, context={"request": None})
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET", "POST"])
@permission_required([CAN_REVIEW], raise_exception=True)
def reviewer_reminder(request, action_id):
    """Get, set or reset a reminder date."""
    action = get_object_or_404(PublicAction, id=action_id)

    if request.method == "GET":
        return Response(dict(reminder=action.reminder), status=status.HTTP_200_OK)

    if request.method == "POST":
        # infer origin
        reminder = request.data.get("reminder")
        action.reminder = reminder
        # don't change the status of the action for this
        action.save(update_fields=["admin_status"])
        serializer = PublicActionSerializer(action, context={"request": None})
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET", "POST"])
@permission_classes([IsAuthenticated])
@renderer_classes([JSONRenderer])
def reviewer_messages(request, action_id):
    """Used by a reviewer or a user"""
    action = get_object_or_404(PublicAction, id=action_id)

    if not can_act(request.user, action):
        raise PermissionDenied

    if request.method == "GET":
        messages = Message.objects.filter(action__id=action.id)
        serializer = MessageSerializer(messages, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "POST":
        # infer origin
        origin = request.user.email
        # use contact email if the request user is the owner
        if action.owner == request.user:
            origin = action.display_contact
            notify_user_add_message(request, action)
        else:
            notify_reviewer_add_message(request, action)
        message = Message.objects.create(action=action, origin=origin, **request.data)
        serializer = MessageSerializer(message)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_required([CAN_REVIEW], raise_exception=True)
def reviewer_counts(request):
    """Get some counts of actions."""

    result = dict(
        reviewable=reviewable_actions().count(),
        reminded=reminded_actions().count(),
        reminded_late=reminded_actions()
        .filter(reminder__lte=datetime.datetime.now())
        .count(),
        published=published_actions().count(),
    )
    return Response(result, status=status.HTTP_200_OK)
