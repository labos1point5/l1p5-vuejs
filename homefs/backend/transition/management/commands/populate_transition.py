from django.core.management.base import BaseCommand
from backend.test_utils import (
    create_test_users,
)
from backend.transition.models import DRAFT, PUBLISHED, SUBMITTED, PublicAction, Tag
from backend.core.models import Administration, Discipline, LaboratoryDiscipline


STATUSES = [DRAFT, SUBMITTED, PUBLISHED]
TAGS = [
    "tag:ges:buildings",
    "tag:ges:devices",
    "tag:ges:travels",
]

LOREM = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non
risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies
sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim
est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu
massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut
in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue.
Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed
dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci
luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed
pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales
hendrerit."""


class Command(BaseCommand):
    help = """Create a pool of users.

    l1p5-test-adming@l1p5.org: super powered user !
    l1p5-test-lab-XXX@l1p5.org: user with a lab associated
    l1p5-test-nolab-XXX@l1p5.org: user with no lab associated

    Note: this deletes any existing users (based on the email)
    """

    def handle(self, *args, **options):
        # create some tests and assign them with some initiatives
        _, users, _ = create_test_users(users_nb=10)
        all_disciplines_data = [
            dict(id=d.id, percentage=50) for d in Discipline.objects.all()
        ]
        all_administrations_data = Administration.objects.all()
        actions_number = 10
        for i_user, u in enumerate(users):
            # create some public actions
            for i in range(20):
                laboratory = u.laboratory.first()
                # add 2 disciplines + 2 administrations
                if i == 0:
                    disciplines_data = [
                        all_disciplines_data[(i + i_user) % len(all_disciplines_data)]
                    ] + [
                        all_disciplines_data[
                            (i + i_user + 1) % len(all_disciplines_data)
                        ]
                    ]
                    for discipline_data in disciplines_data:
                        d, created = LaboratoryDiscipline.objects.update_or_create(
                            discipline_id=discipline_data["id"],
                            laboratory_id=laboratory.id,
                            percentage=discipline_data["percentage"],
                        )
                    administrations_data = [
                        all_administrations_data[
                            (i + i_user) % len(all_administrations_data)
                        ]
                    ] + [
                        all_administrations_data[
                            (i + i_user + 1) % len(all_administrations_data)
                        ]
                    ]
                    laboratory.administrations.set(administrations_data)

                tags_descriptors = list(
                    set(
                        [TAGS[i % len(TAGS)]]
                        + [TAGS[i_user % len(TAGS)]]
                        + [TAGS[(i_user + 1) % len(TAGS)]]
                    )
                )
                public_action = PublicAction(
                    laboratory=laboratory,
                    title=f"Public action {i} - {laboratory.name}",
                    text=f"Action {i}. {LOREM}",
                    admin_status=STATUSES[i % len(STATUSES)],
                )
                tags = set()
                for descriptor in tags_descriptors:
                    tag, _ = Tag.objects.get_or_create(descriptor=descriptor)
                    tags.add(tag)
                public_action.save(update_fields="admin_status")
                public_action.tags.set(tags)
