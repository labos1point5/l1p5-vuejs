"""
curl': curl -H "accept: application/json" -q https://apps.labos1point5.org/api/get_initiatives/ > initiatives.json
Then populate with this
"""
import copy
import json
from pathlib import Path

from django.core.management.base import BaseCommand

from backend.core.models import (
    Administration,
    Discipline,
    LaboratoryDiscipline,
    Laboratory,
)
from backend.transition.models import BaseInitiative
from backend.users.models import L1P5User


L1P5_USER_PASSWORD = "l1p5-test"


def ensure_user_created(*, email) -> L1P5User:
    """Ensure that the test user is created.

    Idempotent
    """
    u, _ = L1P5User.objects.get_or_create(email=email, defaults=dict(is_active=True))
    u.set_password(L1P5_USER_PASSWORD)
    u.save()
    return u


def ensure_lab_created(
    *,
    referent: L1P5User,
    name: str,
    latitude,
    longitude,
) -> Laboratory:
    """Ensure a lab is created.

    Idempotent
    """
    l, _ = Laboratory.objects.get_or_create(
        name=name,
        defaults=dict(referent=referent, latitude=latitude, longitude=longitude),
    )
    return l


class Command(BaseCommand):
    help = """Create a pool of users.

    l1p5-test-adming@l1p5.org: super powered user !
    l1p5-test-lab-XXX@l1p5.org: user with a lab associated
    l1p5-test-nolab-XXX@l1p5.org: user with no lab associated

    Note: this deletes any existing users (based on the email)
    """

    def handle(self, *args, **options):
        # create some tests and assign them with some initiatives
        HERE = Path(__file__).parent.absolute()
        initiatives = json.loads((HERE / "initiatives.json").read_text())

        ids = [initiative["id"] for initiative in initiatives]
        print(max(ids))

        for initiative in initiatives:
            raw_lab = initiative["laboratory"]
            # create a user with the same email
            u = ensure_user_created(email=raw_lab["referent"]["email"])
            # create the lab
            administrations = []
            for a in raw_lab["administrations"]:
                adm_model, _ = Administration.objects.get_or_create(name=a["name"])
                administrations.append(adm_model)
            lab = ensure_lab_created(
                referent=u,
                name=raw_lab["name"],
                longitude=raw_lab["longitude"],
                latitude=raw_lab["latitude"],
            )
            lab.administrations.set(administrations)
            # create the associated disciplines
            disciplines = [
                LaboratoryDiscipline(
                    discipline=Discipline.objects.get(name=d["name"]),
                    laboratory=lab,
                    percentage=d["percentage"],
                )
                for d in raw_lab["disciplines"]
            ]
            for d in disciplines:
                d.save()

            # feed initiatives
            to_save = copy.deepcopy(initiative)
            to_save.pop("laboratory")
            to_save.pop("id")
            model_to_save = BaseInitiative(laboratory=lab, **to_save)
            model_to_save.save()
