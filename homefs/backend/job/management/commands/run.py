from datetime import datetime
import os
import subprocess
import uuid
import sys

from django.core.management.base import BaseCommand
from django.conf import settings


from backend.users.models import L1P5User
from backend.job.models import Job
from backend.job.constants import SUPPORTED_TASKS


def end(res=None, exit_msg=0):
    """Terminate the process.

    Taken from Ansible (GPLv3)
    """
    import json
    import sys

    if res is not None:
        print(json.dumps(res))
    sys.stdout.flush()
    sys.exit(exit_msg)


def daemonize_self():
    """Daemonize the process.

    Taken from Ansible (GPLv3)
    """
    import os
    import sys

    # daemonizing code: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/66012
    try:
        pid = os.fork()
        if pid > 0:
            # exit first parent
            end()
    except OSError:
        e = sys.exc_info()[1]
        end(
            {
                "msg": "fork #1 failed: %d (%s)\n" % (e.errno, e.strerror),
                "failed": True,
            },
            1,
        )

    os.setsid()
    os.umask(int("022", 8))

    try:
        pid = os.fork()
        if pid > 0:
            end()
    except OSError:
        e = sys.exc_info()[1]
        end(
            {
                "msg": "fork #2 failed: %d (%s)\n" % (e.errno, e.strerror),
                "failed": True,
            },
            1,
        )

    # in case we want to redirect to nowhere the standard descriptor

    # dev_null = open('/dev/null', 'w')
    # os.dup2(dev_null.fileno(), sys.stdin.fileno())
    # os.dup2(dev_null.fileno(), sys.stdout.fileno())
    # os.dup2(dev_null.fileno(), sys.stderr.fileno())


def run(cmd, timeout=60, **kwargs):
    result = subprocess.run(
        cmd,
        capture_output=True,
        timeout=timeout,
        shell=True,
        cwd=settings.BASE_DIR,
        **kwargs,
    )
    return result


def _django_as_admin():
    from rest_framework_simplejwt.tokens import RefreshToken

    # get a random admin
    admin = L1P5User.objects.filter(is_superuser=True).first()
    if not admin:
        return
    refresh = RefreshToken.for_user(admin)
    token = refresh.access_token

    return token


def carbon_synthesis(base_url):
    token = _django_as_admin()
    return run(
        f"npm run db:sync:carbonIntensity",
        timeout=600,
        env=dict(BASE_URL=base_url, TOKEN=str(token), PATH=os.environ.get("PATH")),
    )


def scenario_synthesis(base_url):
    token = _django_as_admin()
    return run(
        f"npm run db:sync:scenario:carbonIntensity",
        timeout=600,
        env=dict(BASE_URL=base_url, TOKEN=str(token), PATH=os.environ.get("PATH")),
    )


def sleep(base_url):
    """Dummy fonction thats sleep some time."""
    return run(
        f"sleep 10",
        env=dict(PATH=os.environ.get("PATH")),
    )


def oversleep(base_url):
    """Dummy fonction thats sleep some time."""
    return run(
        f"sleep 10",
        timeout=5,
        env=dict(PATH=os.environ.get("PATH")),
    )


def store(uuid):
    def inner(f):
        def wrapped(*args, **kwargs):
            error = ""
            r = None
            attributes = dict()
            job, _ = Job.objects.get_or_create(name=f.__name__, uuid=uuid)
            try:
                r = f(*args, **kwargs)
                return r
            except Exception as e:
                error = str(e)
            finally:
                if r:
                    attributes.update(
                        stdout=r.stdout.decode(),
                        stderr=r.stderr.decode(),
                        rc=r.returncode,
                    )
                attributes.update(error=error, finished=datetime.now())
                job, _ = Job.objects.update_or_create(id=job.id, defaults=attributes)
                job.save()

        return wrapped

    return inner


import subprocess


class Command(BaseCommand):
    help = ""

    def add_arguments(self, parser):
        parser.add_argument(
            "--daemon",
            action="store_true",
            help="Daemonize the process",
        )
        parser.add_argument(
            "--record", action="store_true", help="Record job info in the database"
        )

        parser.add_argument(
            "--origin", type=str, help="Origin server (where we'll find Django)"
        )

        parser.add_argument("--uuid", type=str, help="force UUID4 for the job")

        parser.add_argument(
            "job",
            choices=SUPPORTED_TASKS,
            help="job to run, supported tasks are defined in constants.py",
        )

    def handle(self, *args, **options):
        if options["daemon"]:
            # beyond that point we're in a fresh process
            daemonize_self()
        # record start
        uid = str(uuid.uuid4())
        if options["uuid"]:
            uid = options["uuid"]

        job = getattr(sys.modules[__name__], options["job"])
        if options["record"]:
            job = store(uid)(job)

        # run it
        r = job(options["origin"])
        if r:
            print(r.stdout.decode())
            print(r.stderr.decode())
            print(f"Job exited with status {r.returncode}")
