# Dumps the data

For use in `ges1point5-data-analyze`.

```
export TOKEN=<your admin token>
export BASE_URL=https://apps.labos1point5.org 

# extract the data (one directory per year)
./01_extract.sh

# fix some ill-formatting fields
./02_fix.sh

# aggregate the yearly files into single one (need pandas)
# remove some fields to (better) anonymize the data
python3 03_anonymize.py
```

Once done, copy the `out` directory to the `ges1point5-data-analyze/data`