/**
 * This task iterates over all Scenario
 *  - It apply and compute the corresponding emissions
 *
 * Note that this task is supposed to be one-shot once the migrations that
 * introduce the CarboIntensity summary in the database is ran.
 *
 * Implementation:
 *  We use jest framework, initially designed for running test.
 *  But jest also incudes a lot of preprocessor/module loader that allows us to
 *  use some of our frontend code without too much pain.
 *
 */

import axios from 'axios'
import Modules from '@/models/Modules.js'
import GHGI from '@/models/carbon/GHGI.js'
import Scenario from '@/models/scenario/Scenario.js'
import { DEFAULT_SETTINGS } from '@/stores/constants.js'

const TOKEN = process.env.TOKEN
if (!TOKEN) {
  console.log(
    'No Token provided: get one with python manage.py token ADMIN_EMAIL'
  )
  process.exit(1)
}

const BASE_URL = process.env.BASE_URL || 'localhost:80'
console.log(`BASE_URL=${BASE_URL} should target the webserver`)

describe('db:sythesis', () => {
  test('db:synthesis:gen', async () => {
    const http = axios.create({
      baseURL: `http://${BASE_URL}/api`,
      timeout: 60 * 4 * 100000,
      headers: {
        'Content-Type': 'application/json'
      }
    })

    http.interceptors.request.use(
      function (config) {
        const token = TOKEN
        if (token) config.headers.Authorization = `Bearer ${token}`
        return config
      },
      function (error) {
        return Promise.reject(error)
      }
    )
    let settings = await http
      .post('get_settings/', { settings: DEFAULT_SETTINGS })
      .then((response) => response.data)
    let commuteSettings = settings.filter((obj) => obj.section === 'commute')
    let activeModules = settings.filter((obj) => obj.section === 'global' && obj.name === 'ACTIVE_MODULES').value
    console.log(commuteSettings)

    let scenarios = await http
      .post('get_scenarios_admin/')
      .then((response) => response.data)

    for (let i in scenarios) {
      let scenarioObj = Scenario.createFromObj(scenarios[i])
      console.log(`Checking scenario.id=${scenarioObj.id}`)
      try {
        let ghgiData = await http
          .post('get_ghgi_consumptions/', { ghgi_id: scenarioObj.ghgi.id })
          .then((response) => response.data)
        // flatten the response
        for (let module of Modules.getModules(
          false,
          activeModules
        )) {
          ghgiData['ghgi'][module] = ghgiData[module]
        }
        let ghgiObj = GHGI.createFromObj(ghgiData['ghgi'])
        scenarioObj.setGHGI(ghgiObj)
        // compute the emissins we want to store
        scenarioObj.compute(commuteSettings)
        console.log('Syncing ', scenarioObj.synthesis)
        await http
          .post('update_scenario_synthesis/', {
            scenario_id: scenarioObj.id,
            synthesis: scenarioObj.synthesis
          })
          .then((response) => response.data)
      } catch (error) {
        console.error(`Error with ${scenarioObj.id}`, error)
        continue
      }
    } // for (let i in scenarios)
  })
})
