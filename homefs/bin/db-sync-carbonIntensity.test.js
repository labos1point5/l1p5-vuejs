/**
 * This task iterates over all GHGI
 *  - It compute the corresponding emissions
 *  - and for any submitted  module this (re-)submit the data
 *
 * Note that this task is supposed to be one-shot once the migrations that
 * introduce the CarboIntensity summary in the database is ran.
 *
 * TODO(msimonin):
 * - compute the GHGI only if it has been submitted
 * - getting all the GHGIs can last a long time, make sure to increase the timeout
 *  => optimization use offset + limit in the API
 *
 * Implementation:
 *  We use jest framework, initially designed for running test.
 *  But jest also incudes a lot of preprocessor/module loader that allows us to
 *  use some of our frontend code without too much pain.
 *
 */

import axios from 'axios'
import GHGI from '@/models/carbon/GHGI.js'
import { DEFAULT_SETTINGS } from '@/stores/constants.js'
import Modules from '@/models/Modules.js'

const TOKEN = process.env.TOKEN
if (!TOKEN) {
  console.log(
    'No Token provided: get one with python manage.py token ADMIN_EMAIL'
  )
  process.exit(1)
}

const BASE_URL = process.env.BASE_URL || 'localhost:80'
console.log(`BASE_URL=${BASE_URL} should target the webserver`)

describe('db:sythesis', () => {
  test('db:synthesis:gen', async () => {
    const http = axios.create({
      baseURL: `http://${BASE_URL}/api`,
      timeout: 60 * 4 * 100000,
      headers: {
        'Content-Type': 'application/json'
      }
    })

    http.interceptors.request.use(
      function (config) {
        const token = TOKEN
        if (token) config.headers.Authorization = `Bearer ${token}`
        return config
      },
      function (error) {
        return Promise.reject(error)
      }
    )
    let settings = await http
      .post('get_settings/', { settings: DEFAULT_SETTINGS })
      .then((response) => response.data)
    let commuteSettings = settings.filter((obj) => obj.section === 'commute')
    let activeModules = settings.filter((obj) => obj.section === 'global' && obj.name === 'ACTIVE_MODULES').value
    console.log(commuteSettings)

    let ghgis = await http
      .post('get_all_ghgi_admin/')
      .then((response) => response.data)

    for (let i in ghgis) {
      let ghgi = ghgis[i]
      console.log(`Checking ghgi.id=${ghgi.id}`)
      try {
        let ghgiData = await http
          .post('get_ghgi_consumptions/', { ghgi_id: ghgi.id })
          .then((response) => response.data)
        // flatten the response
        for (let module of Modules.getModules(
          false,
          activeModules
        )) {
          ghgiData['ghgi'][module] = ghgiData[module]
        }
        let ghgiObj = GHGI.createFromObj(ghgiData['ghgi'])
        // compute the emissins we want to store
        ghgiObj.compute(commuteSettings)
        console.log('Syncing ', ghgiObj.synthesis)
        await http
          .post('update_ghgi_synthesis/', {
            ghgi_id: ghgi.id,
            synthesis: ghgiObj.synthesis
          })
          .then((response) => response.data)
      } catch (error) {
        console.error(`Error with ${ghgi.id}`, error)
        continue
      }
    } // for (let i in ghgis)
  })
})
