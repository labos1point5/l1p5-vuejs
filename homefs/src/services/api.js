import axios from 'axios'
import Cookies from 'js-cookie'
import { useAuthenticationStore } from '@/stores/authentication.js'

const http = axios.create({
  baseURL: '/api',
  timeout: 60 * 4 * 100000,
  headers: {
    'Content-Type': 'application/json',
    'X-CSRFToken': Cookies.get('csrftoken')
  }
})

http.interceptors.request.use(
  function (config) {
    const authStore = useAuthenticationStore()
    const token = authStore.token
    if (token) config.headers.Authorization = `Bearer ${token}`
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

http.interceptors.response.use(undefined, function (error) {
  return new Promise(function (resolve, reject) {
    const authStore = useAuthenticationStore()
    if (error.response.data.code === 'token_not_valid') {
      authStore.authLogout()
    }
    throw error
  })
})

export default http
