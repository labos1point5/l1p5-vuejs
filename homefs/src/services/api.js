import axios from 'axios'
import Cookies from 'js-cookie'

const http = axios.create({
  baseURL: '/api',
  timeout: 60 * 4 * 100000,
  headers: {
    'Content-Type': 'application/json',
    'X-CSRFToken': Cookies.get('csrftoken')
  }
})

// interceptors configuration is moved in the vue plugins dir
// to avoid *service to depends on Vue component (through the store)
export default http
