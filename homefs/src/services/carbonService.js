import api from '@/services/api'

export default {
  getFakeGHGI () {
    return api.get('fakeGHGI/').then((response) => response.data)
  },
  getCounts () {
    return api.get(`get_counts/`).then((response) => response.data)
  },
  saveGHGI (payload) {
    return api.post(`save_ghgi/`, payload).then((response) => response.data)
  },
  deleteGHGI (payload) {
    return api.post(`delete_ghgi/`, payload).then((response) => response.data)
  },
  getAllGHGI () {
    return api.get(`get_all_ghgi/`).then((response) => response.data)
  },
  getAllGHGIWithoutConsumptions () {
    return api
      .get('get_all_ghgi_without_consumptions/')
      .then((response) => response.data)
  },
  getGHGIsWithSynthesis () {
    return api
      .get(`get_ghgis_with_carbon_intensity/`)
      .then((response) => response.data)
  },
  saveVehicles (payload) {
    return api.post(`save_vehicles/`, payload).then((response) => response.data)
  },
  saveComputerDevices (payload) {
    return api
      .post(`save_computer_devices/`, payload)
      .then((response) => response.data)
  },
  savePurchases (payload) {
    return api
      .post(`save_purchases/`, payload)
      .then((response) => response.data)
  },
  saveResearchActivities (payload) {
    return api
      .post(`save_research_activities/`, payload)
      .then((response) => response.data)
  },
  saveBuildings (payload) {
    return api
      .post(`save_buildings/`, payload)
      .then((response) => response.data)
  },
  saveCommutes (payload) {
    return api.post(`save_commutes/`, payload).then((response) => response.data)
  },
  saveFoods (payload) {
    return api.post(`save_foods/`, payload).then((response) => response.data)
  },
  saveSurvey (payload) {
    return api.post(`save_survey/`, payload).then((response) => response.data)
  },
  saveSurveyConfig (payload) {
    return api
      .post(`save_survey_config/`, payload)
      .then((response) => response.data)
  },
  getGHGISurveyInfo (payload) {
    return api
      .get(`get_ghgi_survey_info/`, { params: payload })
      .then((response) => response.data)
  },
  saveTravels (payload) {
    return api.post(`save_travels/`, payload).then((response) => response.data)
  },
  getGHGIConsumptions (payload) {
    return api
      .get(`get_ghgi_consumptions/`, { params: payload })
      .then((response) => response.data)
  },
  getGHGIConsumptionsByUUID (payload) {
    return api
      .get(`get_ghgi_consumptions_by_uuid/`, { params: payload })
      .then((response) => response.data)
  },
  getAllVehicles () {
    return api.get(`get_all_vehicles/`).then((response) => response.data)
  },
  getAllBuildings () {
    return api.get(`get_all_buildings/`).then((response) => response.data)
  },
  getAllGHGIAdmin (payload) {
    return api
      .post(`get_all_ghgi_admin/`, payload)
      .then((response) => response.data)
  },
  getGHGIsConsumptions (payload) {
    return api
      .post(`get_ghgis_consumptions/`, payload)
      .then((response) => response.data)
  },
  getAstroAlphas (params) {
    return api
      .get(`get_astro_alphas/`, { params: params })
      .then((response) => response.data)
  },
  getAstroYears () {
    return api.get(`get_astro_years/`).then((response) => response.data)
  },
  updateSubmitted (payload) {
    return api
      .post(`update_submitted/`, payload)
      .then((response) => response.data)
  },
  updateSurveyActive (payload) {
    return api
      .post(`update_survey_active/`, payload)
      .then((response) => response.data)
  },
  cloneSurvey (payload) {
    return api.post(`clone_survey/`, payload).then((response) => response.data)
  },
  copyData (payload) {
    return api.post(`copy_data/`, payload).then((response) => response.data)
  }
}
