import Travel from '@/models/carbon/Travel.js'

export function getInterval (x) {
  if (x < 100) {
    return [0, 100]
  }
  let x10 = Math.log10(x)
  return [getBin(x), getBin(x + 10 ** Math.floor(x10))]
}

export function getBins (maxValue) {
  // [0, 100, ..., 1000, 2000, ... 10000, 20000]
  // corresponding distances -> [0, 100], ...
  let bins = [0]
  if (maxValue < 100) {
    return bins
  }
  let maxValue10 = Math.log10(maxValue)
  for (let i = 2; i <= Math.ceil(maxValue10); i++) {
    for (let j = 1; j < 10; j++) {
      let binValue = j * 10 ** i
      if (binValue > maxValue) {
        return bins
      }
      bins.push(binValue)
    }
  }
}

export function initializeHistogram (maxValue) {
  let bins = getBins(maxValue)
  let histogram = {}
  for (let b of bins) {
    histogram[b] = 0
  }
  return histogram
}

export function getBin (x) {
  if (x < 100) {
    return 0
  }
  let x10 = Math.floor(Math.log10(x))
  return Math.floor(x / 10 ** x10) * 10 ** x10
}

export function getMaxDistance (data) {
  let max = 0
  for (let travel of data) {
    for (let section of travel.sections) {
      let distance = section.getOneWayCorrectedDistance()
      if (distance > max) {
        max = distance
      }
    }
  }
  return max
}

export function initializeHistograms (max) {
  let histograms = {}
  for (let transportation of TRANSPORTATION) {
    histograms[transportation] = initializeHistogram(max)
  }
  return histograms
}

export function histogramIsZero (histogram) {
  return Object.values(histogram).reduce((a, b) => a + b, 0) === 0
}

export const TRANSPORTATION = Travel.transportations
