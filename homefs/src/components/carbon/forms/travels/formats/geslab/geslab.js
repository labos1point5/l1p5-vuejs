/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Travel from '@/models/carbon/Travel'
import { intConverter, booleanConverter } from '@/utils/parser.js'

export default function (positionDictionary, purposeDictionary) {
  let positiond = positionDictionary
  let purposed = purposeDictionary
  return {
    meta: {
      name: 'GESlab',
      check: function (fields) {
        return fields.includes('Groupe labo')
      },
      delimiters: [',', '\t', ';']
    },
    names: {
      pattern: /(Num.ro mission)/i,
      required: true,
      converter: function (value) {
        return [value]
      }
    },
    date: {
      pattern: /(date|departure date|date de d.part)/i,
      required: true
    },
    departureCity: {
      pattern: /(departure city|ville de départ)/i,
      required: true
    },
    departureCountry: {
      pattern: /(departure country|country of departure|pays de départ)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    destinationCity: {
      pattern: /(destination city|ville de destination)/i,
      required: true
    },
    destinationCountry: {
      pattern:
        /(destination country|country of destination|pays de destination)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    transportation: {
      pattern: /(moyens de transport)/i,
      required: true,
      converter: function (value) {
        function _convertGeslabModeDeplacement (value) {
          let mode = ''
          if (value === 'Avion') {
            mode = Travel.MODE_PLANE
          } else if (value === 'Bateau') {
            mode = Travel.MODE_FERRY
          } else if (value === 'Bus') {
            mode = Travel.MODE_BUS
          } else if (value === 'Transports en commun') {
            mode = Travel.MODE_BUS
          } else if (value === 'Location de véhicule') {
            mode = Travel.MODE_CAR
          } else if (value === 'Véhicule de location') {
            mode = Travel.MODE_CAR
          } else if (value === 'Covoiturage') {
            mode = Travel.MODE_CAR
          } else if (value === 'Metro') {
            mode = Travel.MODE_SUBWAY
          } else if (value === 'Rer') {
            mode = Travel.MODE_RER
          } else if (value === 'Taxi') {
            mode = Travel.MODE_CAB
          } else if (value === 'Train') {
            mode = Travel.MODE_TRAIN
          } else if (value === 'Véhicule personnel') {
            mode = Travel.MODE_CAR
          } else if (value === 'Véhicule de service') {
            mode = Travel.MODE_CAR
          }
          return mode
        }
        function _getDominantModeDeplacement (modes) {
          let mode = ''
          let modesOrder = [
            Travel.MODE_PLANE,
            Travel.MODE_TRAIN,
            Travel.MODE_FERRY,
            Travel.MODE_BUS,
            Travel.MODE_CAR,
            Travel.MODE_RER,
            Travel.MODE_SUBWAY,
            Travel.MODE_CAB
          ]
          for (let m of modesOrder) {
            if (modes.includes(m)) {
              mode = m
              break
            }
          }
          return mode
        }
        let allModes = value.split(',')
        let finalMode = ''
        if (allModes.length === 1) {
          finalMode = _convertGeslabModeDeplacement(value)
        } else {
          let convertedAllModes = allModes.map((value) => {
            try {
              return _convertGeslabModeDeplacement(value)
            } catch (e) {
              return ''
            }
          })
          finalMode = _getDominantModeDeplacement(convertedAllModes)
        }
        finalMode = Travel.getTransportation(finalMode)
        return finalMode
      }
    },
    carpooling: {
      pattern: /(Nb de pers. dans la voiture)/i,
      default: 1,
      required: true,
      converter: intConverter
    },
    isRoundTrip: {
      pattern: /(roundtrip|aller.*retour)/i,
      converter: booleanConverter,
      required: true
    },
    purpose: {
      pattern: /(purpose of the trip|purpose|motif|motif du déplacement)/i,
      converter: function (value) {
        return Travel.getPurpose(value, purposed)
      },
      default: Travel.PURPOSE_UNKNOWN
    },
    status: {
      pattern: /(agent position|position|statut.*agent|statut)/i,
      converter: function (value) {
        return Travel.getStatus(value, positiond)
      },
      default: Travel.STATUS_UNKNOWN
    },
    amount: {
      pattern: /(quantit.|nombre|amount|number)/i,
      default: 1,
      converter: intConverter
    },
    tags: {
      pattern: /(tag|tags|label|labels)/i,
      converter: function (value) {
        return value.split(',').map((val) => val.trim())
      },
      default: []
    }
  }
}
