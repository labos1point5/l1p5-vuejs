/* eslint-disable */

import { hasFlag } from "./helpers";

const sorted = 1
const optional = 2

export const Sorted = sorted
export const Optional = optional


export default (parentCmp, flags = Sorted) => ({
    inject: {parent: {from: 'm' + parentCmp, default: false}},
    props: {
        label: String,
        icon: String,
        iconPack: String,
        visible: {
            type: Boolean,
            default: true
        },
        value: {
            type: String,
            default() { return this._uid.toString() }
        },
        headerClass: {
            type: [String, Array, Object],
            default: null
        }
    },
    data() {
        return {
            transitionName: null,
            elementClass: 'item',
            elementRole: null,
            index: null
        }
    },
    computed: {
        isActive() {
            return this.parent.activeItem === this
        }
    },
    methods: {
        /**
         * Activate element, alter animation name based on the index.
         */
        activate(oldIndex) {
            this.transitionName = this.index < oldIndex
                ? this.parent.vertical ? 'slide-down' : 'slide-next'
                : this.parent.vertical ? 'slide-up' : 'slide-prev'
        },

        /**
         * Deactivate element, alter animation name based on the index.
         */
        deactivate(newIndex) {
            this.transitionName = newIndex < this.index
                ? this.parent.vertical ? 'slide-down' : 'slide-next'
                : this.parent.vertical ? 'slide-up' : 'slide-prev'
        }
    },
    render(createElement) {
        // if destroy apply v-if
        if (this.parent.destroyOnHide) {
            if (!this.isActive || !this.visible) {
                return
            }
        }
        const vnode = createElement('div', {
            directives: [{
                name: 'show',
                value: this.isActive && this.visible
            }],
            attrs: {
                'class': this.elementClass,
                'role': this.elementRole,
                'id': `${this.value}-content`,
                'aria-labelledby': this.elementRole ? `${this.value}-label` : null,
                'tabindex': this.isActive ? 0 : -1
            }
        }, this.$slots.default)
        // check animated prop
        if (this.parent.animated) {
            return createElement('transition', {
                props: {
                    'name': this.parent.animation || this.transitionName,
                    'appear': this.parent.animateInitially === true || undefined
                },
                on: {
                    'before-enter': () => { this.parent.isTransitioning = true },
                    'after-enter': () => { this.parent.isTransitioning = false }
                }
            }, [vnode])
        }
        return vnode
    },
    created() {
        if (!this.parent) {
            if (!hasFlag(flags, optional)) {
                this.$destroy()
                throw new Error('You should wrap ' + this.$options.name + ' in a ' + parentItemName)
            }
        } else if (this.parent._registerItem) {
            this.parent._registerItem(this)
        }
    },
    beforeDestroy() {
        if (this.parent && this.parent._unregisterItem) {
            this.parent._unregisterItem(this)
        }
    }
})
