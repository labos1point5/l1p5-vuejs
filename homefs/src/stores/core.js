/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import { defineStore } from 'pinia'
import coreService from '@/services/coreService'
import Modules from '@/models/Modules.js'
import { ActiveModules } from '@/models/ActiveModules'
import { ActiveApps, ALL_APPS } from '@/models/ActiveApps'
import { buildPositionLabelMapping } from '@/models/carbon/Boundary'

import { Configuration } from '../models/carbon/Configuration'

import _ from 'lodash'

import { i18n } from '../plugins/i18n'

export const useCoreStore = defineStore('core', {
  state: () => {
    return {
      conf: null,
      loadedOnce: false
    }
  },
  getters: {
    entityClass: (state) => state.conf.ENTITY_CLASS.value,
    positionTitles: (state) => state.conf.POSITION_TITLES.value,
    options: (state) => state?.conf.toOptions(),
    surveyLabelMapping: (state) => {
      return buildPositionLabelMapping(
        state.conf.CF_LABEL_MAPPING.value,
        state.positionTitles
      )
    },
    travelPositionLabels: (state) => state.conf.TRAVEL_POSITION_LABELS.value,
    travelPositionDictionary: (state) =>
      state.conf.TRAVEL_POSITION_DICTIONARY.value,
    travelPurposeLabels: (state) => state.conf.TRAVEL_PURPOSE_LABELS.value,
    travelPurposeDictionary: (state) =>
      state.conf.TRAVEL_PURPOSE_DICTIONARY.value,
    surveyPositionTitles: (state) => {
      return _.uniq(Object.values(state.surveyLabelMapping))
    },
    setting: (state) => {
      return (section, name) => {
        return state.conf.setting(section, name)
      }
    },
    settings: (state) => state.conf.settings,
    // flat list of available apps
    availableApps: (state) => {
      let appsObj = ALL_APPS.filter((obj) => !obj.isSimulator)
      return appsObj
    },
    // flat list of available simulators
    availableSimulators: (state) => {
      let appsObj = ALL_APPS.filter((obj) => obj.isSimulator)
      return appsObj
    },
    // flat list of active apps
    activeApps: (state) => {
      let appsObj = ActiveApps.fromSettings(state.conf.settings).apps.filter(
        (obj) => !obj.isSimulator
      )
      return appsObj
    },
    // flat list of inactive apps
    inactiveApps: (state) => {
      return state.availableApps.filter(
        (obj) => !state.activeApps.map((s) => s.name).includes(obj.name)
      )
    },
    // flat list of active simulators
    activeSimulators: (state) => {
      let appsObj = ActiveApps.fromSettings(state.conf.settings).apps.filter(
        (obj) => obj.isSimulator
      )
      return appsObj
    },
    // flat list of inactive simulators
    inactiveSimulators: (state) => {
      return state.availableSimulators.filter(
        (obj) => !state.activeSimulators.map((s) => s.name).includes(obj.name)
      )
    },
    // flat list of active apps with documentation
    activeAppsWithDocumentation: (state) => {
      let appsObj = ActiveApps.fromSettings(state.conf.settings).apps.filter(
        (obj) => obj.hasDocumentation
      )
      return appsObj
    },
    // flat list of actual top level modules objets
    activeModulesObjs: (state) => {
      let ml = ActiveModules.fromSettings(state.conf.settings)
      return ml
    },
    // get one specific module
    module: (state) => (type) => {
      return state.activeModulesObjs.byType(type)
    },
    // flat list of module types (sub types included)
    // mostly kept fot backward compatibility
    // FIXME(msimonin): change this name at some point (beware of the impact)
    activeModules:
      (state) =>
        (includesSubModules = false) => {
          let ams = state.activeModulesObjs.types
          return Modules.getModules(includesSubModules, ams)
        },
    modulesColors () {
      let colors = this.setting('global', 'MODULES_COLORS').value
      for (let module of this.activeModules(true)) {
        if (!Object.keys(colors).includes(module)) {
          colors[module] = Modules.DEFAULT_COLORS[module]
        }
      }
      return colors
    },
    sections: (state) => {
      return state.conf.sections
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    // set a custom configuration
    setConf (conf) {
      this.loadedOnce = false
      this.conf = Configuration.fromDatabase(conf.settings)
      for (let lang in this.conf.I18N.value) {
        i18n.mergeLocaleMessage(lang, this.conf.I18N.value[lang])
      }
    },
    getSettings () {
      if (this.loadedOnce) {
        return new Promise((resolve, reject) => {
          resolve(this.conf)
        })
      }
      return coreService
        .getSettings()
        .then((conf) => {
          this.setConf(conf)
          this.loadedOnce = true
          return this.conf
        })
        .catch((error) => {
          throw error
        })
    },
    saveSettings () {
      return coreService
        .saveSettings({ settings: this.settings })
        .then((conf) => {
          this.setConf(conf)
          return conf
        })
        .catch((error) => {
          throw error
        })
    },
    updateSettings (setting) {
      let csetting = this.settings.filter((obj) => obj.id === setting.id)[0]
      if (Array.isArray(csetting.value) && !Array.isArray(setting.value)) {
        csetting.value = setting.value.split(',')
      } else {
        csetting.value = setting.value
      }
    }
  }
})
