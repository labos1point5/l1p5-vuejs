import { defineStore } from 'pinia'
import { useCoreStore } from './core'

import carbonService from '@/services/carbonService'
import scenarioService from '@/services/scenarioService'
import userService from '@/services/userService'
import Scenario from '@/models/scenario/Scenario.js'
import GHGI from '@/models/carbon/GHGI.js'
import { User } from '@/models/user/user.js'

export const useSuperadminStore = defineStore('superadmin', {
  state: () => ({
    allGHGI: [],
    scenarios: [],
    allUsers: [],
    usersComputed: false
  }),
  getters: {
    settings: (state) => {
      const core = useCoreStore()
      return core.setting('commute')
    },
    superusers: (state) => {
      return state.allUsers.filter((u) => u.isSuperUser)
    },
    reviewers: (state) => {
      return state.allUsers.filter(
        (u) => u.roles.map((r) => r.descriptor).indexOf('reviewer') >= 0
      )
    },
    users: (state) => {
      return state.allUsers
    },
    inactives: (state) => {
      return state.allUsers.filter((u) => !u.isActive)
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    getAllGHGI (data) {
      let toCompute = false
      return carbonService
        .getAllGHGIAdmin(data)
        .then((allGHGI) => {
          this.setAllGHGI(allGHGI, toCompute)
          return allGHGI
        })
        .catch((error) => {
          throw error
        })
    },
    setAllGHGI (allGHGI, toCompute) {
      this.allGHGI = []
      for (let ghgi of allGHGI) {
        let cghgi = GHGI.createFromObj(ghgi)
        if (toCompute) {
          cghgi.compute(this.settings)
        }
        this.allGHGI.unshift(cghgi)
      }
    },
    getScenarios (data) {
      return scenarioService
        .getScenariosAdmin(data)
        .then((scenarios) => {
          this.scenarios = []
          for (let scenario of scenarios) {
            let csenario = Scenario.createFromObj(scenario)
            this.scenarios.unshift(csenario)
          }
          return scenarios
        })
        .catch((error) => {
          throw error
        })
    },
    unsubmitData (ghgi, module) {
      ghgi.unsubmit(module)
      return carbonService
        .updateSubmitted({
          ghgi_id: ghgi.id,
          submitted: ghgi.submitted
        })
        .then((data) => {
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    /**
     * Get the all users
     * The data is cached
     * Might throw
     */
    getUsers () {
      // we cache the data anyway
      if (this.usersComputed) {
        return this.allUsers
      } else {
        return userService
          .getUsers()
          .then((data) => {
            let users = data.map((d) => User.createFromObj(d))
            this.allUsers = users
            this.usersComputed = true
            return this.allUsers
          })
          .catch((error) => {
            throw error
          })
      }
    },
    updateUserIsAdmin (data) {
      return userService.updateIsAdmin(data)
    },
    sendActivationEmail (email) {
      return userService.sendActivationEmail(email)
    },
    updateRoles (data) {
      return userService.updateRoles(data).then((data) => {
        let user = User.createFromObj(data)
        let index = this.allUsers.findIndex((u) => u.email === user.email)
        if (index >= 0) {
          this.allUsers[index] = user
          return this.allUsers[index]
        }
      })
    }
  }
})
