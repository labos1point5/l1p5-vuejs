import Vue from 'vue'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import loadDrillDown from 'highcharts/modules/drilldown'
import exporting from 'highcharts/modules/exporting'
import histogramBellcurve from 'highcharts/modules/histogram-bellcurve'
import more from 'highcharts/highcharts-more'
import stockInit from 'highcharts/modules/stock'

loadDrillDown(Highcharts)
exporting(Highcharts)
histogramBellcurve(Highcharts)
more(Highcharts)
stockInit(Highcharts)

Vue.use(HighchartsVue, {
  Highcharts
})

Highcharts.theme = {
  credits: {
    enabled: false
  },
  exporting: {
    buttons: {
      contextButton: {
        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
      }
    },
    chartOptions: {
      chart: {
        width: 750,
        height: 400
      }
    }
  }
}
// Apply the theme
Highcharts.setOptions(Highcharts.theme)
