import http from '@/services/api'

import { useAuthenticationStore } from '@/stores/authentication.js'

http.interceptors.request.use(
  function (config) {
    const authStore = useAuthenticationStore()
    const token = authStore.token
    if (token) config.headers.Authorization = `Bearer ${token}`
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

http.interceptors.response.use(undefined, function (error) {
  return new Promise(function (resolve, reject) {
    const authStore = useAuthenticationStore()
    if (error.response.data.code === 'token_not_valid') {
      authStore.authLogout()
    } else if (error.response.data.code === 'user_not_found') {
      authStore.authLogout()
    }
    throw error
  })
})
