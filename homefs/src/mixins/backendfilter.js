import { TAGS } from '@/models/transition/transition.js'

import { Background } from '@/mixins/background.js'

/**
 * Common filtering logic
 */
export const BackendFilter = {
  mixins: [Background],
  data: function () {
    return {
      availableTags: TAGS,
      filteredTags: TAGS,
      disciplinesTree: [],
      filters: {},
      items: [],
      currentItem: null,
      next: null,
      count: 0
    }
  },
  methods: {
    itemOnMouseOver (item) {},
    filtered (filters) {
      Object.assign(this.filters, filters)
      this.filterAll()
    },
    loadMore () {
      if (this.next !== null) {
        // fetch new batch of data from remote server
        return this.getNextFnc(this.next).then((data) => {
          this.items = this.items.concat(
            data.results.map((a) => this.ItemClass().fromDatabase(a))
          )
          this.next = data.next
          this.count = data.count
          this.localPage = this.localPage + 1
        })
      }
    },
    filterAll () {
      // make it fully async
      this.getItems(this.filters).then((data) => {
        this.items = data.results.map((a) => this.ItemClass().fromDatabase(a))
        this.next = data.next
        this.count = data.count
      })
    },
    /**
     * For filtering on the backend
     * @param {*} bounds
     */
    boundsUpdated (bounds) {
      let [lat1, lon1] = [bounds._southWest.lat, bounds._southWest.lng]
      let [lat2, lon2] = [bounds._northEast.lat, bounds._northEast.lng]
      this.filters.bounds = [
        [lat1, lon1],
        [lat2, lon2]
      ]
      this.filterAll()
    },
    resetBounds () {
      this.filters.bounds = null
      this.filterAll()
    }
  }
}
