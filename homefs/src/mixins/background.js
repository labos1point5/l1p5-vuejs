export const Background = {
  methods: {
    /** Call the function pass if not null. */
    callIfNotNull (f) {
      if (f != null) {
        f()
      }
    },
    /**
     * Show a toast
     * @param {*} success
     * @param {*} text
     */
    toast (success, text) {
      return () => {
        let message = ''
        if (success) {
          message = `${
            text
          } <strong><span style="color:green";>${this.$t(
            'success'
          )}</span></strong>.`
        } else {
          message = `${this.$t(
            text
          )} <strong><span style="color:red";>${this.$t(
            'error'
          )}</span></strong>.`
        }
        this.$buefy.toast.open({
          type: 'is-light',
          message: message
        })
      }
    },

    /**
     * Generic way of calling a user action encapsulated in some common logic
     * Common logic includes, form validation displaying a loading screen, handling errors ...
     *
     * The workflow is the following
     * - it displays a loading screen just before the action
     * - it runs the user action (cb) and its awaited for
     * - some generic error handling is performed (set the this.erreur correcty)
     * - it finally remove the loading screen (regardless the status of the user action)
     * - a custom user cleanup user action is then performed (if any).
     */

    async bgUI (userAction, onResolve, onReject, onFinally) {
      this.erreur = null
      const loadingComponent = this.$buefy.loading.open({
        container: null
      })
      try {
        // note that even if userAction doesn't return a Promise
        // it return value will be wrapped in a fulfilled Promise
        // so we pay the overhead of this extra-wrapping but we don't loose
        // the execution flow order in that case
        let value = await userAction()
        this.callIfNotNull(onResolve)
        return value
      } catch (error) {
        console.info('Error in bgUI', error) // eslint-disable-line no-console
        this.erreur = 'error'
        if (error.response) {
          this.erreur = error.response.data
        } else if (error.request) {
          this.erreur = 'An error occured when making a remote request'
        } else {
          this.erreur = error.message
        }
        this.callIfNotNull(onReject)
        throw error
      } finally {
        loadingComponent.close()
        this.callIfNotNull(onFinally)
      }
    },
    /**
     * Same as doInBackground but validate the forms prior to anything
     */
    async validateBgUi (userAction, onResolve, onReject, onFinally) {
      // clean previous error
      try {
        let result = await this.$validator.validateAll()
        if (result) {
          // proceed with the call
          return await this.bgUI(userAction, onResolve, onReject, onFinally)
        }
      } catch (error) {
        // note this error handling is unlikely called because we
        // catch all in bgUI
        console.warn('Error in validateBgUi', error) // eslint-disable-line no-console
        if (error.response) {
          this.erreur = error.response.data
        } else if (error.request) {
          this.erreur = 'An error occured when making a remote request'
        } else {
          this.erreur = error.message
        }
      }
    }
  }
}
