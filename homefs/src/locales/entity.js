export const DEFAULT_STRUCTURE_KEY = 'default'
const DEFAULT_STRUCTURE_I18N_EN = {
  any: 'any',
  my: 'my',
  mys: 'my',
  the: 'the',
  thes: 'the',
  this: 'this',
  one: 'one',
  done: 'of the',
  dones: 'of the',
  tothe: 'to the',
  tothes: 'to the',
  structure: 'structure',
  structures: 'structures'
}
const DEFAULT_STRUCTURE_I18N_FR = {
  any: 'toute',
  my: 'ma',
  mys: 'mes',
  the: 'la',
  thes: 'les',
  this: 'cette',
  one: 'une',
  done: 'de la',
  dones: 'des',
  tothe: 'à la',
  tothes: 'aux',
  structure: 'structure',
  structures: 'structures'
}

export const structureMessages = {
  Laboratory: {
    en: {
      any: 'any',
      my: 'my',
      mys: 'my',
      the: 'the',
      thes: 'the',
      this: 'this',
      one: 'one',
      done: 'to the',
      dones: 'to the',
      tothe: 'to the',
      tothes: 'to the',
      structure: 'laboratory',
      structures: 'laboratories'
    },
    fr: {
      any: 'tout',
      my: 'mon',
      mys: 'mes',
      the: 'le',
      thes: 'les',
      this: 'ce',
      one: 'un',
      done: 'du',
      dones: 'des',
      tothe: 'au',
      tothes: 'aux',
      structure: 'laboratoire',
      structures: 'laboratoires'
    }
  },
  Entity: {
    en: DEFAULT_STRUCTURE_I18N_EN,
    fr: DEFAULT_STRUCTURE_I18N_FR
  },
  [DEFAULT_STRUCTURE_KEY]: {
    en: DEFAULT_STRUCTURE_I18N_EN,
    fr: DEFAULT_STRUCTURE_I18N_FR
  }
}
