import { Validator } from 'vee-validate'

import _ from 'lodash'

Validator.extend('quota', {
  getMessage (field, args) {
    return 'quota-exceeded'
  },
  validate (files, [remainingQuota]) {
    // Account only for in memory files
    let inMem = files.filter((f) => f instanceof File)
    let totalSize = _.sumBy(inMem, (f) => f.size)
    return totalSize < remainingQuota
  }
})

Validator.extend('afternow', {
  getMessage () {
    return 'not-afternow'
  },
  validate (date) {
    if (date === null) {
      // null is accepted as a sign of resetting the reminder
      return true
    }
    let isAfter = date > new Date()
    return isAfter
  }
})
