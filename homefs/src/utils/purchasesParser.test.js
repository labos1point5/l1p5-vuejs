import { readFile } from 'fs/promises'

import { parseFile } from './purchasesParser'

const path = require('node:path')

describe('parsesPurchases', () => {
  test('purchasesTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/purchasesTemplate.csv'
      ),
      'utf8'
    )
    parseFile(datafile)
      .then(([content, headerMap, error]) => {
        expect(error).toBe('ok')
        expect(content.length).toBe(7)
        expect(content[0][headerMap.icode]).toBe('AA64')
        expect(content[0][headerMap.iamount]).toBe('1350.00')
      })
  })

  test('purchasesTemplate.tsv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/purchasesTemplate.tsv'
      ),
      'utf8'
    )
    parseFile(datafile)
      .then(([content, headerMap, error]) => {
        expect(error).toBe('ok')
        expect(content.length).toBe(7)
        expect(content[0][headerMap.icode]).toBe('AA64')
        expect(content[0][headerMap.iamount]).toBe('1350.00')
      })
  })
})
