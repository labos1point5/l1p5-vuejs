import Modules from '@/models/Modules.js'
import * as Papa from 'papaparse'

const keysToHeatingType = {
  réseau: 'urban.network',
  reseau: 'urban.network',
  gaz: 'naturalgas',
  biomethane: 'biomethane',
  propane: 'propane',
  fioul: 'heating.oil',
  granulés: 'wood.pellets',
  granules: 'wood.pellets',
  plaquettes: 'wood.chips',
  bûches: 'wood.logs',
  buches: 'wood.logs'
}

function _parseHeaders (header, filetype) {
  function _findHeader (regex) {
    for (let name of header) {
      if (name.search(regex) >= 0) {
        return name
      }
    }
    return null
  }
  if (filetype === 'energies') {
    return {
      name: _findHeader(/nom|name|b.timent|building/i),
      year: _findHeader(/ann.e|year/i),
      area: _findHeader(/sub|surface|utile|brute|area|gfa/i),
      share: _findHeader(/part|fraction|occupation|share/i),
      heatings: {
        electric: _findHeader(/chauffage.*.lectrique|electric.*heating/i),
        urbanNetwork: _findHeader(/id.*(r.seau|chaleur|urbain)|(urban|network).*id/i),
        urbanConsumption: _findHeader(/conso.*(r.seau|chaleur|urbain)|(urban|network).*cons/i),
        naturalgas: _findHeader(/gaz.*naturel|natural.*gas/i),
        propane: _findHeader(/propane/i),
        'heating.oil': _findHeader(/fioul|oil/i),
        biomethane: _findHeader(/biom.thane/i),
        'wood.pellets': _findHeader(/(granul.s|pellets)/i),
        'wood.chips': _findHeader(/(plaquette|foresti.re|chip)/i),
        'wood.logs': _findHeader(/b.che|log/i)
      },
      isOwned: _findHeader(/propri.*taire|owner/i),
      electricity: _findHeader(/.lectricit./i),
      water: _findHeader(/eau|water/i),
      selfConsumption: _findHeader(/(auto|self).*(production|consumption|conso)/i),
      site: _findHeader(/(site|campus)/i)
    }
  } else {
    return {
      name: _findHeader(/nom|b.timent|name|building/i),
      refrigerants: {
        name: _findHeader(/r.frig.rant|clim|fluide|gaz|gas|frigorig.ne/i),
        total: _findHeader(/cons|qtt.|quantit./i)
      }
    }
  }
}

const parseFile = (file) => new Promise((resolve, reject) => {
  Papa.parse(file, {
    header: true,
    worker: true,
    delimitersToGuess: [',', '\t', ';'],
    skipEmptyLines: true,
    complete: function (result) {
      if (result.errors.length > 0) {
        resolve(['', [], {}, result.errors])
      } else {
        if (result.meta.fields.length <= 3) {
          let filetype = Modules.REFRIGERANTS
          let headerMap = _parseHeaders(result.meta.fields, filetype)
          if (!headerMap.name || !headerMap.refrigerants.name || !headerMap.refrigerants.total) {
            resolve([filetype, [], {}, 'missing columns'])
          } else {
            resolve([filetype, result.data, headerMap, 'ok'])
          }
        } else {
          let filetype = 'energies'
          let headerMap = _parseHeaders(result.meta.fields, filetype)
          if (!headerMap.name || !headerMap.area) {
            resolve([filetype, [], {}, 'missing columns'])
          } else {
            resolve([filetype, result.data, headerMap, 'ok'])
          }
        }
      }
    },
    error: function (error) {
      reject(error)
    }
  })
})

export { parseFile, keysToHeatingType }
