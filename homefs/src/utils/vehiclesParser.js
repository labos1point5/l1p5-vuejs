import * as Papa from 'papaparse'

const keysToType = {
  voiture: 'car',
  car: 'car',
  moto: 'motorbike',
  motorbike: 'motorbike',
  velo: 'bike',
  vélo: 'bike',
  bike: 'bike',
  bicycle: 'bike',
  trotinette: 'scooter',
  trottinette: 'scooter',
  scooter: 'scooter'
}

const keysToEngine = {
  essence: 'gasoline',
  gasoline: 'gasoline',
  diesel: 'diesel',
  inconnue: 'unknown.engine',
  inconnu: 'unknown.engine',
  unknown: 'unknown.engine',
  GPL: 'lpg',
  LPG: 'lpg',
  GNV: 'cng',
  CNG: 'cng',
  E85: 'e85',
  hybride: 'hybrid',
  hybrid: 'hybrid',
  electrique: 'electric',
  électrique: 'electric',
  electric: 'electric',
  musculaire: 'muscular',
  muscular: 'muscular'
}

function _parseHeaders (header) {
  function _findHeader (regex) {
    for (let name of header) {
      if (name.search(regex) >= 0) {
        return name
      }
    }
    return null
  }
  return {
    name: _findHeader(/nom|name|identifiant|id|vehicule|vehicle/i),
    type: _findHeader(/type/i),
    engine: _findHeader(/motorisation|engine/i),
    distance: _findHeader(/distance|km/i)
  }
}

const parseFile = (file) => new Promise((resolve, reject) => {
  Papa.parse(file, {
    header: true,
    worker: true,
    delimitersToGuess: [',', '\t', ';'],
    skipEmptyLines: true,
    complete: function (result) {
      if (result.errors.length > 0) {
        resolve([[], {}, result.errors])
      } else {
        let headerMap = _parseHeaders(result.meta.fields)
        if (!headerMap.name || !headerMap.type || !headerMap.engine || !headerMap.distance) {
          resolve([[], {}, 'missing columns'])
        } else {
          resolve([result.data, headerMap, 'ok'])
        }
      }
    },
    error: function (error) {
      reject(error)
    }
  })
})

export { parseFile, keysToType, keysToEngine }
