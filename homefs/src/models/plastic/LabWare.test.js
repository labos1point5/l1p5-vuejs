import { PlasticLabWare, GlassLabWare } from './LabWare'

function polystyreneLabWare () {
  return PlasticLabWare.createFromObj({
    type: 'polystyrene',
    weight: 10,
    manufacturingArea: 'europe',
    sterilized: true,
    decontamination: true
  })
}

function polycarbonateLabWare () {
  return PlasticLabWare.createFromObj({
    type: 'polycarbonate',
    weight: 10,
    manufacturingArea: 'asia',
    sterilized: true,
    decontamination: true
  })
}

function polypropeneLabWare () {
  return PlasticLabWare.createFromObj({
    type: 'polypropene',
    weight: 10,
    manufacturingArea: 'america',
    sterilized: false,
    decontamination: true
  })
}

function tube30LabWare () {
  return GlassLabWare.createFromObj({
    name: 'tube.30',
    weight: 10,
    manufacturingArea: 'europe'
  })
}

function erlenmeyer2LabWare () {
  return GlassLabWare.createFromObj({
    name: 'erlenmeyer.2000',
    weight: 10,
    manufacturingArea: 'america'
  })
}

function erlenmeyer2LPrewashedLabWare () {
  return GlassLabWare.createFromObj({
    name: 'erlenmeyer.2000',
    weight: 10,
    manufacturingArea: 'america',
    sterilized: true,
    country: 'fr',
    decontamination: true,
    filling: 90,
    prewash: true,
    hotWaterUsed: true,
    hotWaterVolume: 2.5,
    heating: 'gaz'
  })
}

function pipette10LabWare () {
  return GlassLabWare.createFromObj({
    name: 'pipette.10',
    weight: 10,
    manufacturingArea: 'europe',
    sterilized: true,
    country: 'fr',
    decontamination: true,
    filling: 80,
    prewash: false,
    hotWaterUsed: false,
    hotWaterVolume: 2.5,
    heating: 'electric',
    ngloves: 10,
    nautoclaveCycleAWeek: 5,
    autoclaveWeight: 1000
  })
}

describe('Compute plastic labware emissions', () => {
  test('Polystyrene 10g', () => {
    let labware = polystyreneLabWare()
    expect(labware.getManufacturingImpact().intensity['carbon']).toBe(0.03654)
    expect(labware.getEOLImpact().intensity['carbon']).toBe(0.03191)
    expect(labware.getMouldingImpact().intensity['carbon']).toBeCloseTo(
      0.008870000000000001,
      10
    )
    expect(labware.getSterilisationImpact().intensity['carbon']).toBe(0.000957)
    expect(labware.getTransportImpact().intensity['carbon']).toBeCloseTo(
      0.004808,
      10
    )
    expect(labware.getDecontaminationImpact().intensity['carbon']).toBeCloseTo(
      0.000016745076923076924,
      10
    )
    expect(labware.getImpact().intensity['carbon']).toBeCloseTo(
      0.0831017450769231,
      10
    )
  })
  test('Polycarbonate 10g', () => {
    let labware = polycarbonateLabWare()
    expect(labware.getManufacturingImpact().intensity['carbon']).toBe(0.069)
    expect(labware.getEOLImpact().intensity['carbon']).toBeCloseTo(0.02613, 10)
    expect(labware.getMouldingImpact().intensity['carbon']).toBe(0.01518)
    expect(labware.getSterilisationImpact().intensity['carbon']).toBe(0.002668)
    expect(labware.getTransportImpact().intensity['carbon']).toBeCloseTo(
      0.007768,
      10
    )
    expect(labware.getDecontaminationImpact().intensity['carbon']).toBeCloseTo(
      0.000016745076923076924,
      10
    )
    expect(labware.getImpact().intensity['carbon']).toBeCloseTo(
      0.12076274507692308,
      10
    )
  })
  test('Polypropene 10g', () => {
    let labware = polypropeneLabWare()
    expect(labware.getManufacturingImpact().intensity['carbon']).toBeCloseTo(
      0.036789999999999996,
      10
    )
    expect(labware.getEOLImpact().intensity['carbon']).toBeCloseTo(0.02613, 10)
    expect(labware.getMouldingImpact().intensity['carbon']).toBe(0.01518)
    expect(labware.getSterilisationImpact().intensity['carbon']).toBe(0)
    expect(labware.getTransportImpact().intensity['carbon']).toBeCloseTo(
      0.007728,
      10
    )
    expect(labware.getDecontaminationImpact().intensity['carbon']).toBeCloseTo(
      0.000016745076923076924,
      10
    )
    expect(labware.getImpact().intensity['carbon']).toBeCloseTo(
      0.08584474507692308,
      10
    )
  })
})

describe('Compute glass labware emissions', () => {
  test('tube 30ml 10g', () => {
    let labware = tube30LabWare()
    expect(labware.getManufacturingImpact().intensity['carbon']).toBeCloseTo(
      0.0007122580645161291,
      10
    )
    expect(labware.getEOLImpact().intensity['carbon']).toBeCloseTo(
      0.000005483870967741936,
      10
    )
    expect(labware.getSterilisationImpact().intensity['carbon']).toBeCloseTo(
      0.002796852363782051,
      10
    )
    expect(labware.getWashingImpact().intensity['carbon']).toBeCloseTo(
      0.0011788738461538462,
      10
    )
    expect(labware.getDippingImpact().intensity['carbon']).toBeCloseTo(
      0.009318000000000002,
      10
    )
    expect(labware.getDecontaminationImpact().intensity['carbon']).toBeCloseTo(
      0.002796852363782051,
      10
    )
    expect(labware.getPrewashImpact().intensity['carbon']).toBe(0)
    expect(labware.getWaterHeatingImpact().intensity['carbon']).toBe(0)
    expect(labware.getTransportImpact().intensity['carbon']).toBeCloseTo(
      0.00015509677419354837,
      10
    )
    expect(labware.getImpact().intensity['carbon']).toBeCloseTo(
      0.01696341728339537,
      10
    )
  })
  test('erlenmayer 2l 10g', () => {
    let labware = erlenmeyer2LabWare()
    expect(labware.getManufacturingImpact().intensity['carbon']).toBeCloseTo(
      0.000807741935483871,
      10
    )
    expect(labware.getEOLImpact().intensity['carbon']).toBeCloseTo(
      0.000005483870967741936,
      10
    )
    expect(labware.getSterilisationImpact().intensity['carbon']).toBeCloseTo(
      0.15408104395604394,
      10
    )
    expect(labware.getWashingImpact().intensity['carbon']).toBeCloseTo(
      0.05729122623408337,
      10
    )
    expect(labware.getDippingImpact().intensity['carbon']).toBeCloseTo(
      0.31060000000000004,
      10
    )
    expect(labware.getDecontaminationImpact().intensity['carbon']).toBeCloseTo(
      0.15408104395604394,
      10
    )
    expect(labware.getPrewashImpact().intensity['carbon']).toBe(0)
    expect(labware.getWaterHeatingImpact().intensity['carbon']).toBe(0)
    expect(labware.getTransportImpact().intensity['carbon']).toBeCloseTo(
      0.00024929032258064513,
      10
    )
    expect(labware.getImpact().intensity['carbon']).toBeCloseTo(
      0.6771158302752036,
      10
    )
  })
  test('erlenmayer 2l 10g', () => {
    let labware = erlenmeyer2LPrewashedLabWare()
    expect(labware.getManufacturingImpact().intensity['carbon']).toBeCloseTo(
      0.000807741935483871,
      10
    )
    expect(labware.getEOLImpact().intensity['carbon']).toBeCloseTo(
      0.000005483870967741936,
      10
    )
    expect(labware.getSterilisationImpact().intensity['carbon']).toBeCloseTo(
      0.14273870573870573,
      10
    )
    expect(labware.getWashingImpact().intensity['carbon']).toBeCloseTo(
      0.05729122623408337,
      10
    )
    expect(labware.getDippingImpact().intensity['carbon']).toBeCloseTo(
      0.31060000000000004,
      10
    )
    expect(labware.getDecontaminationImpact().intensity['carbon']).toBeCloseTo(
      0.14273870573870573,
      10
    )
    expect(labware.getPrewashImpact().intensity['carbon']).toBeCloseTo(
      0.4789,
      10
    )
    expect(labware.getWaterHeatingImpact().intensity['carbon']).toBeCloseTo(
      0.023999999999999997,
      10
    )
    expect(labware.getTransportImpact().intensity['carbon']).toBeCloseTo(
      0.00024929032258064513,
      10
    )
    expect(labware.getImpact().intensity['carbon']).toBeCloseTo(
      1.157331153840527,
      10
    )
  })
  test('pipette 10ml 10g', () => {
    let labware = pipette10LabWare()
    expect(labware.getManufacturingImpact().intensity['carbon']).toBe(
      0.0007122580645161291,
      10
    )
    expect(labware.getEOLImpact().intensity['carbon']).toBeCloseTo(
      0.000005483870967741936,
      10
    )
    expect(labware.getSterilisationImpact().intensity['carbon']).toBeCloseTo(
      0.004538422626836921,
      10
    )
    expect(labware.getWashingImpact().intensity['carbon']).toBeCloseTo(
      0.07478873197115385,
      10
    )
    expect(labware.getDippingImpact().intensity['carbon']).toBeCloseTo(
      0.004659000000000001,
      10
    )
    expect(labware.getDecontaminationImpact().intensity['carbon']).toBeCloseTo(
      0.004538422626836921,
      10
    )
    expect(labware.getPrewashImpact().intensity['carbon']).toBe(0)
    expect(labware.getWaterHeatingImpact().intensity['carbon']).toBe(0)
    expect(labware.getTransportImpact().intensity['carbon']).toBe(
      0.00015509677419354837,
      10
    )
    expect(labware.getImpact().intensity['carbon']).toBeCloseTo(
      0.0893974159345051,
      10
    )
  })
})
