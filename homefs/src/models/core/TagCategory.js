/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

export class Tag {
  constructor (id = null, name = null, color = null, category) {
    this.id = id
    this.name = name
    this.color = color
    this.category = category
  }

  get isUntagged () {
    return this.name === Tag.UNTAGGED_LABEL
  }

  isEqual (tag) {
    if (this.id && tag.id) {
      return this.id === tag.id
    } else {
      return (
        this.name.trim() === tag.name.trim() &&
        this.color === tag.color &&
        this.category === tag.category
      )
    }
  }

  getCategory (categories) {
    let results = categories.filter((category) => this.category === category.id)
    if (results.length === 1) {
      return results[0]
    } else {
      return null
    }
  }

  toDatabase () {
    return {
      id: this.id,
      name: this.name
    }
  }

  static get UNTAGGED_LABEL () {
    return 'untagged'
  }

  static createFromObj (data) {
    if (typeof data === 'string') {
      return new Tag(null, data, null, null)
    } else {
      return new Tag(data.id, data.name, data.color, data.category)
    }
  }
}

export default class TagCategory {
  constructor (
    id = null,
    name = null,
    description = null,
    color = null,
    tags = []
  ) {
    this.id = id
    this.name = name
    this.description = description
    this.color = color
    this.tags = tags.map((obj) => Tag.createFromObj(obj))
  }

  isEqual (category) {
    if (this.id && category.id) {
      return this.id === category.id
    } else {
      return this.name === category.name && this.color === category.color
    }
  }

  toDatabase () {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      color: this.color,
      tags: this.tags.map((obj) => obj.toDatabase())
    }
  }

  removeTag (tag) {
    let index = this.tags.findIndex((obj) => obj.isEqual(tag))
    this.tags.splice(index, 1)
  }

  addTag (tag) {
    if (tag instanceof Tag) {
      this.tags.push(tag)
      return tag
    } else {
      let t = Tag.createFromObj(tag)
      this.tags.push(t)
      return t
    }
  }

  static createFromObj (data) {
    return new TagCategory(
      data.id,
      data.name,
      data.description,
      data.color,
      data.tags
    )
  }
}
