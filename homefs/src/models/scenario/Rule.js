/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { v4 as uuidv4 } from 'uuid'

export default class Rule {
  name = 'Rule'
  module = null
  target = Rule.TARGET_ELEMENT
  lang = {}
  references = {}

  level2 = {}

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    if (id) {
      this.id = id
    } else {
      this.id = uuidv4()
    }
    this.scenario = scenario
    this.level1 = level1
  }

  get year () {
    return this.scenario.year
  }

  get laboratory () {
    return this.scenario.laboratory
  }

  get data () {
    if (this.scenario) {
      return this.scenario.getData(this.module)
    } else {
      return null
    }
  }

  get level1Min () {
    return 0
  }

  get level1Max () {
    return 100
  }

  get level1MaxValue () {
    return this.level1Max
  }

  get level1Step () {
    return 1
  }

  get level1Unit () {
    return '%'
  }

  get level1Prefix () {
    return ''
  }

  get tickslimit () {
    return null
  }

  get ticks () {
    return [0, 25, 50, 75, 100]
  }

  get level1Ticks () {
    return this.ticks.map(val => {
      return {
        value: val,
        display: val
      }
    })
  }

  get hasLevel2Parameters () {
    return Object.keys(this.level2).length > 0
  }

  get level2Parameters () {
    return this.level2
  }

  get forceAfterRule () {
    return null
  }

  get graphics () {
    return []
  }

  initiateLevel1 () {
    if (!this.level1) {
      this.level1 = this.level1Min
    }
  }

  initiateLevel2 (level2) {
    if (Object.keys(level2).length > 0) {
      for (let key2 of Object.keys(this.level2)) {
        if (Object.keys(level2).includes(key2)) {
          if (level2[key2]) {
            if (Array.isArray(level2[key2])) {
              this.level2[key2].value = level2[key2]
            } else {
              if (this.getDynamicLevel2Values(key2).length === this.getLevel2Values(key2).length) {
                // if no values are selected, init with empty array
                if (!this.isUnique(key2)) {
                  this.level2[key2].value = []
                }
              } else if (this.isUnique(key2)) {
                // else if unique init with first left value
                this.level2[key2].value = [this.getDynamicLevel2Values(key2)[0]]
              } else {
                // else init with values left
                this.level2[key2].value = this.getDynamicLevel2Values(key2)
              }
            }
          }
        }
      }
    }
  }

  isUnique (key) {
    if (this.hasLevel2Parameters) {
      if ('unique' in this.level2[key]) {
        return this.level2[key].unique
      } else {
        return false
      }
    } else {
      return false
    }
  }

  isHierarchical (key) {
    return this.getLevel2Objects(key).filter(obj => 'children' in obj).length !== 0
  }

  getLevel2Objects (key) {
    if (this.hasLevel2Parameters) {
      if (this.data) {
        return this.level2[key].values(this.data)
      } else {
        return []
      }
    } else {
      return []
    }
  }

  getDynamicLevel2Objects (key) {
    let otherrules = this.scenario.rules.filter(obj => obj.name === this.name && obj.id !== this.id)
    if (this.hasLevel2Parameters) {
      if (this.data) {
        return this.level2[key].values(
          this.data,
          otherrules.map(obj => obj.level2)
        )
      } else {
        return []
      }
    } else {
      return []
    }
  }

  getDynamicLevel2Values (key) {
    return this.getDynamicLevel2Objects(key).filter(obj => !obj.isDisabled).map(obj => obj.id)
  }

  getLevel2Values (key) {
    return this.getLevel2Objects(key).map(obj => obj.id)
  }

  allValuesSelected (key) {
    let allValuesSelected = true
    if (this.level2[key].value.length !== 0) {
      if (this.isHierarchical(key)) {
        for (let pvalue of this.getLevel2Objects(key)) {
          if (!this.level2[key].value.includes(pvalue.id)) {
            allValuesSelected = false
          }
        }
      } else {
        allValuesSelected = this.level2[key].value.length === this.getLevel2Values(key).length
      }
    }
    return allValuesSelected
  }

  resetLevel2Values () {
    if (this.hasLevel2Parameters) {
      for (let key2 of Object.keys(this.level2)) {
        if (!this.isUnique(key2)) {
          this.level2[key2].value = []
        }
      }
      for (let key2 of Object.keys(this.level2)) {
        if (this.getDynamicLevel2Values(key2).length !== this.getLevel2Values(key2).length) {
          // init with values left if some already used
          if (this.isUnique(key2)) {
            this.level2[key2].value = [this.getDynamicLevel2Values(key2)[0]]
          } else {
            this.level2[key2].value = this.getDynamicLevel2Values(key2)
          }
        }
      }
    }
  }

  hasSibblings () {
    return this.scenario.rules.filter(obj => obj.name === this.name).length > 1
  }

  compute (items) {
    return items
  }

  setScenario (scenario) {
    this.scenario = scenario
    this.initiateLevel1()
  }

  getTargetIntensity (settings) {
    return this.scenario.getGHGIIntensity(this.module).intensity
  }

  labelFormatter (value) {
    return this.level1Prefix + value + ' ' + this.level1Unit
  }

  tickFormatter (value) {
    return this.level1Prefix + value + this.level1Unit
  }

  toDatabase () {
    let toReturn = {
      'name': this.name,
      'level1': this.level1
    }
    if (this.hasLevel2Parameters) {
      let self = this
      toReturn['level2'] = {}
      for (let key2 of Object.keys(this.level2)) {
        toReturn['level2'][key2] = self.level2[key2].value
      }
    }
    return toReturn
  }

  static translate (rule, key, lang, returnNull = false) {
    if (key in rule.lang[lang]) {
      return rule.lang[lang][key]
    } else if (returnNull) {
      return null
    } else {
      return key
    }
  }

  static get TARGET_ELEMENT () {
    return 'target'
  }

  static get TARGET_EMISSIONS () {
    return 'emissions'
  }

  humanTitle (lang) {
    return Rule.translate(this, 'humanTitle', lang)(this.level1, this.level1Unit)
  }
}
