/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Purchase from '../../carbon/Purchase'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Durabilité des instruments',
    level1help: 'Augmenter la durée de vie des instruments et machines du laboratoire achetés sur des crédits gérés par le laboratoire.',
    description: 'Les instruments scientifiques et machines peuvent représenter une part importante des investissements du laboratoire, et par suite <strong>contribuer significativement à son empreinte carbone</strong>. <br /> Augmenter la durée de vie de ces matériels peut  permettre de réduire leurs empreintes.',
    otherbenefits: 'Réduire les investissements associés',
    limits: 'Augmenter la durée de vie des matériels peut impliquer une <strong>augmentation des dépenses de maintenance et des émissions liées aux réparations</strong> éventuelles. Celles-ci sont toutefois plus faibles que l’investissement dans un matériel neuf. Elles ne sont pas prises en compte dans la version actuelle de l’outil.',
    rebounds: 'Ne pas renouveler des instruments dégage une part du budget qui peut être engagé dans de <strong>nouvelles dépenses potentiellement plus émettrices</strong> en termes de GES. <br /> Dans tous les cas, si les crédits non dépensés sont utilisés pour d’autres usages, cela induit une émission nouvelle de GES qui vient diminuer l’efficacité de la mesure. Cette bascule n’est pas prise en compte dans le calcul.',
    manual: 'Cette mesure ne concerne que les instruments de laboratoire achetés par le laboratoire. Le curseur permet de choisir une augmentation relative entre 0 (pas de changement) à 100% (doublement de la durée de vie). Aucune configuration avancée n’est disponible. Ceci signifie en particulier qu’il n’est pas possible de spécifier plus finement les matériels dont la durée de vie est augmenté, ni de différencier les augmentations. Une augmentation relative est proposée ici car la durée de vie absolue des matériels peut être très différente selon leur nature : 3 ans pour les smartphones, 3-7 ans pour les ordinateurs, 10 ans voire plus pour le mobilier.'
  },
  en: {
    title: 'Durability of the instruments',
    level1help: 'Increase the lifetime of the laboratory\'s instruments and machines purchased with funds managed by the laboratory.',
    description: 'Scientific instruments and machines can represent a significant part of the laboratory\'s investment, and therefore <strong>contribute significantly to its carbon footprint</strong>. <br /> Increasing the lifetime of these materials can help reduce their footprint.',
    otherbenefits: 'Reduce associated investments',
    limits: 'Increasing the lifetime of equipment may involve <strong>increased expenditure on maintenance and emissions related to potential repairs</strong>. However, these are lower than investing in new equipment. They are not taken into account in the current version of the tool.',
    rebounds: 'Not renewing instruments can <strong>free up new credits</strong> that can be spent on new, potentially <strong>more GHG emitting</strong> expenditure. In any case, if the unspent credits are used for other purposes, this leads to a new GHG emission which reduces the effectiveness of the measure. This changeover is not taken into account in the calculation.',
    manual: 'This measure only concerns laboratory instruments purchased by the laboratory. The slider allows you to choose a relative increase between 0 (no change) and 100% (doubling of the lifetime). No advanced settings are available. In particular, this means that it is not possible to further specify the materials whose lifetime is increased, nor to differentiate the increases. A relative increase is proposed here because the absolute lifetime of equipment can be very different depending on its nature: 3 years for smartphones, 3-7 years for computers, 10 years or more for furniture.'
  }
}

export default class IncreaseInstrumentsLifetime extends Rule {
  name = 'IncreaseInstrumentsLifetime'
  module = Modules.PURCHASES
  target = Rule.TARGET_EMISSIONS
  lang = LANG
  references = {}

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
  }

  get graphics () {
    return [
      'PurchasesInstrumentsPie'
    ]
  }

  compute (intensity, levels1, levels2, purchases) {
    let ointensities = new CarbonIntensities()
    let eintensities = new CarbonIntensities()
    for (let purchase of purchases) {
      let intensity = purchase.getCarbonIntensity(this.year)
      if (purchase.module === Purchase.MODULE_NAME) {
        if (purchase.category === 'lab.equipment') {
          eintensities.add(intensity)
        } else {
          ointensities.add(intensity)
        }
      }
    }
    let oci = ointensities.sum()
    let eci = eintensities.sum()
    intensity.intensity = oci.intensity + eci.intensity * (100 / (100 + levels1[0]))
    intensity.intensitywc = oci.intensitywc + eci.intensitywc * (100 / (100 + levels1[0]))
    intensity.uncertainty = oci.uncertainty + eci.uncertainty * (100 / (100 + levels1[0]))
    intensity.uncertaintywc = oci.uncertaintywc + eci.uncertaintywc * (100 / (100 + levels1[0]))
    return intensity
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let purchase of this.data) {
      if (purchase.category === 'lab.equipment') {
        let cintensity = purchase.getCarbonIntensity(this.year)
        intensity.add(cintensity)
      }
    }
    return intensity
  }
}
