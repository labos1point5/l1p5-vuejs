/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Purchase from '../../carbon/Purchase'
import Modules from '@/models/Modules.js'

const LANG = {
  fr: {
    title: 'Réduire les achats',
    all: 'Toutes',
    category: 'Métacatégorie de l\'achat',
    code: 'Code NACRES',
    level1help: 'Réduire la quantité de biens et de services achetés par le laboratoire.',
    level2label: '(catégories : <strong>__category__</strong>)',
    level2help: 'Selectionner une métacatégorie ou un code NACRES d\'achat sur lequel filtrer l\'application de la mesure de réduction.',
    description: 'Cette mesure s’inscrit dans une <strong>démarche de sobriété</strong>. La quantité de tout ou partie des biens et des services achetés par le laboratoire est réduite. Différentes stratégies peuvent le permettre : une plus grande mutualisation des équipements, une optimisation des usages, la réduction ou l’arrêt de certaines activités, ...',
    otherbenefits: 'Réduire les achats nécessite de repenser les pratiques de recherche. Ceci peut amener à de <strong>meilleurs pratiques</strong> et à des relations au sein du laboratoire renouvelées.',
    rebounds: 'La réduction des achats induit des dépenses moins importantes par le laboratoire. Les <strong>crédits</strong> ainsi <strong>dégagés peuvent être utilisés pour engager d’autres dépenses</strong>, potentiellement plus émettrices en terme de GES que celles du périmètre concerné par la mesure. Dans tous les cas, si les crédits non dépensés sont utilisés pour d’autres usages, cela induit une émission nouvelle de GES qui vient diminuer l’efficacité de la mesure.',
    manual: 'Cette mesure concerne l’ensemble des achats de biens et de services du laboratoire. Le curseur permet de spécifier la réduction relative des achats entre 0 et -100%. La configuration avancée permet de définir une métacatégorie ou un code NACRES impacté par cette mesure.',
    humanTitle: function (level1Human, level2Human) {
      let s = `Réduire de ${level1Human} % la quantité de biens et services achetés`
      if (level2Human) {
        s = s + `(code NACRES ${level2Human})`
      }
      return s
    }
  },
  en: {
    title: 'Reduce Purchases',
    all: 'All',
    category: 'Purchase metacategory',
    code: 'NACRES Code',
    level1help: 'Reduce the quantity of goods and services purchased by the laboratory.',
    level2label: '(categories: <strong>__category__</strong>)',
    level2help: 'Select a purchase metacategory or a NACRES code on which to filter the application of the mitigation measure.',
    description: 'This measure is part of an <strong>approach to sufficiency</strong>. The quantity of all or part of the goods and services purchased by the laboratory is reduced. Various strategies can enable this, such as greater equipment sharing, optimisation of usage, or reduction/cessation of certain activities.',
    otherbenefits: 'Reducing purchases requires rethinking research practices. This can lead to <strong>improved practices</strong> and renewed relationships within the laboratory.',
    rebounds: 'Reducing purchases leads to lower expenses for the laboratory. The resulting <strong>savings can be used to engage in other expenditures</strong>, which may potentially have higher GHG emissions than those within the scope of the measure. In any case, if the unspent credits are allocated to other purposes, this leads to a new GHG emission which reduces the effectiveness of the measure.',
    manual: 'This measure applies to all purchases of goods and services made by the laboratory. The slider allows you to specify the relative reduction of purchases between 0% and -100%. The advanced configuration enables you to define a metacategory or NACRES code affected by this measure.',
    humanTitle: function (level1Human, level2Human) {
      let s = `Reduce the quantity of goods and services purchased by ${level1Human}`
      if (level2Human) {
        s = s + `(code NACRES ${level2Human})`
      }
      return s
    }
  }
}

export default class ReducePurchase extends Rule {
  name = 'ReducePurchase'
  module = Modules.PURCHASES
  lang = LANG
  references = {}

  level2 = {
    category: {
      values: function (purchases, otherrules = null) {
        let categories = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.category.value)
          }
        }
        for (let purchase of purchases) {
          let node = categories.filter(obj => obj.id === purchase.category)
          if (node.length === 0) {
            categories.push({
              id: purchase.category,
              label: purchase.category,
              isDisabled: used.includes(purchase.category),
              children: [
                {
                  id: purchase.code,
                  label: purchase.code.toUpperCase() + ' - ' + Purchase.getDescription(purchase.code),
                  isDisabled: used.includes(purchase.code)
                }
              ]
            })
          } else {
            let leaf = node[0].children.filter(obj => obj.id === purchase.code)
            if (leaf.length === 0) {
              node[0].children.push({
                id: purchase.code,
                label: purchase.code.toUpperCase() + ' - ' + Purchase.getDescription(purchase.code),
                isDisabled: used.includes(purchase.code)
              })
            }
          }
        }
        for (let category of categories) {
          category.children.sort((a, b) => (a.label > b.label) || -(a.label < b.label))
        }
        return categories.sort((a, b) => (a.label > b.label) || -(a.label < b.label))
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get graphics () {
    return [
      'PurchasesPie'
    ]
  }

  compute (purchases) {
    let categriesUsed = []
    let codesUsed = []
    for (let item of this.level2.category.value) {
      if (Purchase.isACode(item)) {
        codesUsed.push(item)
      } else {
        categriesUsed.push(item)
      }
    }
    for (let purchase of purchases) {
      if (this.level2.category.value.length > 0) {
        if (categriesUsed.includes(purchase.category) || codesUsed.includes(purchase.code)) {
          purchase.amount = (1 - (this.level1 / 100)) * purchase.amount
        }
      } else {
        purchase.amount = (1 - (this.level1 / 100)) * purchase.amount
      }
    }
    return purchases
  }

  humanTitle (lang) {
    let level2Human = this.level2.category.value.join(', ')
    return Rule.translate(this, 'humanTitle', lang)(this.level1, level2Human)
  }
}
