/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Purchase from '../../carbon/Purchase'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Achat d\'occasion',
    all: 'Tous',
    category: 'Métacatégorie de l\'achat',
    code: 'Code NACRES',
    level1help: 'Basculer une partie des achats neufs en achats d\'occasion.',
    level2label: '(catégories : <strong>__category__</strong>)',
    level2help: 'Selectionner une métacatégorie ou un code NACRES d\'achat sur lequel filtrer l\'application de la mesure de réduction.',
    description: 'Le matériel acheté d’occasion a une <strong>empreinte carbone</strong> liée à sa fabrication qui est <strong>nulle</strong> par définition. En effet, seul le primo-acheteur se voit attribuer cette empreinte carbone, et ceci dans son intégralité afin d’éviter les double-comptages.',
    otherbenefits: 'Allonger la durée de vie des matériels, en achetant d’occasion, permet de <strong>réduire la quantité de déchets produits</strong>.',
    limits: 'Attention, cette mesure ne concerne que le matériel acheté d’occasion sans reconditionnement qui peut induire une empreinte carbone significative (matériel numérique par exemple). <br /> Les dépenses supplémentaires éventuelles de <strong>réparation et de maintenance ne sont pas prises en compte</strong>.',
    rebounds: 'Les achats d’occasion sont généralement moins onéreux que les achats de matériel neuf. Les <strong>crédits</strong> ainsi <strong>dégagés peuvent être utilisés pour engager d’autres dépenses</strong>, potentiellement plus émettrices en terme d’effets de GES que l’achat de matériel neuf. <br /> Dans tous les cas, si les crédits non dépensés sont utilisés pour d’autres usages, cela induit une émission nouvelle de GES qui vient diminuer l’efficacité de la mesure. Une <strong>filière d’occasion</strong> peut entraîner une augmentation de la quantité de produits achetés ainsi qu’<strong>une compensation de conscience</strong> [1].',
    manual: 'Cette mesure concerne trois méta-catégories des achats (Matériels et instruments de laboratoire, Informatique et audiovisuel, et une partie de la méta-catégorie Vie du laboratoire). Le curseur permet de spécifier la proportion des achats totaux concernés par cette mesure qui est achetée d’occasion. La configuration avancée permet de définir une méta-catégorie ou un code NACRES impacté par cette mesure. Cette mesure peut également être utilisée pour quantifier l’impact d’une réduction de la quantité de matériels achetés.',
    humanTitle: function (level1Human, level2Human) {
      let s = `Basculer ${level1Human} % des achats neufs en achats d'occasion`
      if (level2Human) {
        s = s + `(code NACRES ${level2Human})`
      }
      return s
    }
  },
  en: {
    title: 'Second-hand purchase',
    all: 'All',
    category: 'Purchase metacategory',
    code: 'NACRES code',
    level1help: 'Switch a portion of new purchases to second-hand purchases.',
    level2label: '(categories: <strong>__category__</strong>)',
    level2help: 'Select a purchase metacategory or a NACRES code on which to filter the application of the mitigation measure.',
    description: 'Second-hand equipment has, by definition, a <strong>zero carbon footprint</strong> related to its manufacturing. Only the initial purchaser is attributed this carbon footprint, in entirety, to avoid double counting.',
    otherbenefits: 'Extending the lifespan of equipment by purchasing second-hand helps to <strong>reduce the quantity of waste produced</strong>.',
    limits: 'Careful, this measure only applies to second-hand purchases without refurbishment, which can have a significant carbon footprint (e.g., digital equipment). <br /> Possible additional expenses for <strong>repair and maintenance are not taken into account</strong>.',
    rebounds: 'Second-hand purchases are generally less expensive than new purchases. The resulting <strong>savings can be used to engage in other expenditures</strong>, which may potentially have higher GHG emissions than buying new equipment. In any case, if the unspent credits are allocated to other purposes, this leads to a new GHG emission which reduces the effectiveness of the measure. A <strong>second-hand market</strong> can result in an increase in the quantity of purchased products and a <strong>sense of compensation</strong> [1].',
    manual: 'This measure applies to three meta-categories of purchases (Laboratory equipment and instruments, IT and audiovisual, and a portion of the Laboratory life meta-category). The slider allows you to specify the proportion of total purchases covered by this measure that are purchased second-hand. The advanced configuration allows you to define a meta-category or NACRES code affected by this measure. This measure can also be used to quantify the impact of a reduction in the quantity of purchased equipment.',
    humanTitle: function (level1Human, level2Human) {
      let s = `Switch ${level1Human} % of new purchases to second-hand purchases`
      if (level2Human) {
        s = s + `(code NACRES ${level2Human})`
      }
      return s
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Encouragement de l’innovation et de la technologie',
    year: '2007',
    authors: 'Office fédéral de l’énergie',
    link: 'https://pubdb.bfe.admin.ch/fr/publication/download/3806'
  }
}

export default class SecondHandPurchase extends Rule {
  name = 'SecondHandPurchase'
  module = Modules.PURCHASES
  lang = LANG
  references = REFERENCES

  level2 = {
    category: {
      values: function (purchases, otherrules = null) {
        const ALLOWED_CATEGORIES = [
          'lab.life',
          'lab.equipment',
          'info'
        ]
        const ALLOWED_CODES = [
          'aa43', 'aa44', 'aa52', 'aa53', 'ab02', 'ab03', 'ac21', 'ac22', 'ac23', 'ad01',
          'ad02', 'ad04', 'ad11', 'ad12', 'ad13', 'ad14', 'ae01', 'ae02', 'ae03', 'ae11',
          'ae21', 'ae22', 'ae23', 'ae24', 'ae25', 'ae26', 'ae27', 'af01', 'af11', 'ba11',
          'bb01', 'bb02', 'bb03', 'bb04', 'bd04', 'bd05', 'bd11', 'bd15', 'bd22', 'bd23',
          'bd24', 'bd25', 'bd26', 'bd27', 'bg04', 'ce01', 'ce03', 'ce11', 'ce12', 'ce31',
          'ce32', 'ce33', 'ce34', 'ce41', 'ce42', 'ce43', 'cg11', 'cg12', 'cg21'
        ]
        let categories = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.category.value)
          }
        }
        for (let purchase of purchases) {
          let node = categories.filter(obj => obj.id === purchase.category)
          if (node.length === 0) {
            if (ALLOWED_CATEGORIES.includes(purchase.category)) {
              categories.push({
                id: purchase.category,
                label: purchase.category,
                isDisabled: used.includes(purchase.category),
                children: []
              })
              if (purchase.category === 'lab.life') {
                if (ALLOWED_CODES.includes(purchase.code)) {
                  let node = categories.filter(obj => obj.id === purchase.category)
                  node[0].children.push({
                    id: purchase.code,
                    label: purchase.code.toUpperCase() + ' - ' + Purchase.getDescription(purchase.code),
                    isDisabled: used.includes(purchase.code)
                  })
                }
              } else {
                let node = categories.filter(obj => obj.id === purchase.category)
                node[0].children.push({
                  id: purchase.code,
                  label: purchase.code.toUpperCase() + ' - ' + Purchase.getDescription(purchase.code),
                  isDisabled: used.includes(purchase.code)
                })
              }
            }
          } else {
            let leaf = node[0].children.filter(obj => obj.id === purchase.code)
            if (leaf.length === 0) {
              if (purchase.category === 'lab.life') {
                if (ALLOWED_CODES.includes(purchase.code)) {
                  node[0].children.push({
                    id: purchase.code,
                    label: purchase.code.toUpperCase() + ' - ' + Purchase.getDescription(purchase.code),
                    isDisabled: used.includes(purchase.code)
                  })
                }
              } else {
                node[0].children.push({
                  id: purchase.code,
                  label: purchase.code.toUpperCase() + ' - ' + Purchase.getDescription(purchase.code),
                  isDisabled: used.includes(purchase.code)
                })
              }
            }
          }
        }
        for (let category of categories) {
          category.children.sort((a, b) => (a.label > b.label) || -(a.label < b.label))
        }
        return categories.sort((a, b) => (a.label > b.label) || -(a.label < b.label))
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get graphics () {
    return [
      'PurchasesPieSecondHand'
    ]
  }

  compute (purchases) {
    const ALLOWED_CATEGORIES = [
      'lab.life',
      'lab.equipment',
      'info'
    ]
    const ALLOWED_CODES = [
      'aa43', 'aa44', 'aa52', 'aa53', 'ab02', 'ab03', 'ac21', 'ac22', 'ac23', 'ad01',
      'ad02', 'ad04', 'ad11', 'ad12', 'ad13', 'ad14', 'ae01', 'ae02', 'ae03', 'ae11',
      'ae21', 'ae22', 'ae23', 'ae24', 'ae25', 'ae26', 'ae27', 'af01', 'af11', 'ba11',
      'bb01', 'bb02', 'bb03', 'bb04', 'bd04', 'bd05', 'bd11', 'bd15', 'bd22', 'bd23',
      'bd24', 'bd25', 'bd26', 'bd27', 'bg04', 'ce01', 'ce03', 'ce11', 'ce12', 'ce31',
      'ce32', 'ce33', 'ce34', 'ce41', 'ce42', 'ce43', 'cg11', 'cg12', 'cg21'
    ]
    let categriesUsed = []
    let codesUsed = []
    for (let item of this.level2.category.value) {
      if (Purchase.isACode(item)) {
        codesUsed.push(item)
      } else {
        categriesUsed.push(item)
      }
    }
    for (let purchase of purchases) {
      if (this.level2.category.value.length > 0) {
        if (categriesUsed.includes(purchase.category) || codesUsed.includes(purchase.code)) {
          purchase.amount = (1 - (this.level1 / 100)) * purchase.amount
        }
      } else if (ALLOWED_CATEGORIES.includes(purchase.category)) {
        if (purchase.category === 'lab.life') {
          if (ALLOWED_CODES.includes(purchase.code)) {
            purchase.amount = (1 - (this.level1 / 100)) * purchase.amount
          }
        } else {
          purchase.amount = (1 - (this.level1 / 100)) * purchase.amount
        }
      }
    }
    return purchases
  }

  getTargetIntensity (settings) {
    const ALLOWED_CATEGORIES = [
      'lab.life',
      'lab.equipment',
      'info'
    ]
    const ALLOWED_CODES = [
      'aa43', 'aa44', 'aa52', 'aa53', 'ab02', 'ab03', 'ac21', 'ac22', 'ac23', 'ad01',
      'ad02', 'ad04', 'ad11', 'ad12', 'ad13', 'ad14', 'ae01', 'ae02', 'ae03', 'ae11',
      'ae21', 'ae22', 'ae23', 'ae24', 'ae25', 'ae26', 'ae27', 'af01', 'af11', 'ba11',
      'bb01', 'bb02', 'bb03', 'bb04', 'bd04', 'bd05', 'bd11', 'bd15', 'bd22', 'bd23',
      'bd24', 'bd25', 'bd26', 'bd27', 'bg04', 'ce01', 'ce03', 'ce11', 'ce12', 'ce31',
      'ce32', 'ce33', 'ce34', 'ce41', 'ce42', 'ce43', 'cg11', 'cg12', 'cg21'
    ]
    let intensity = new CarbonIntensities()
    for (let purchase of this.data) {
      if (ALLOWED_CATEGORIES.includes(purchase.category)) {
        if (purchase.category === 'lab.life') {
          if (ALLOWED_CODES.includes(purchase.code)) {
            intensity.add(purchase.getCarbonIntensity(this.year))
          }
        } else {
          intensity.add(purchase.getCarbonIntensity(this.year))
        }
      }
    }
    return intensity
  }

  humanTitle (lang) {
    let level2Human = this.level2.category.value.join(', ')
    return Rule.translate(this, 'humanTitle', lang)(this.level1, level2Human)
  }
}
