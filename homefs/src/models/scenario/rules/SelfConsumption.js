/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Autoconso. d\'éléctricité',
    level1help: 'Définir une quantité d’énergie électrique renouvelable auto-consommée par le laboratoire en MWh par an.',
    description: 'La réglementation indique que l’<strong>auto-consommation d’énergie produite</strong> par des installations sur place doit être <strong>déduite de la consommation totale</strong> de la structure. En d’autres termes, le facteur d’émission de l’électricité auto-consommée est nul. En revanche, l’électricité auto-produite qui est revendue sur le réseau ne bénéficie pas de cet avantage [1].<br /> En moyenne en France, <strong>1m2 de panneau solaire produit de 85 à 280 kWh/an/m2</strong>. Cela dépend de la localisation géographique (un facteur 2 existe entre le nord et le sud de la France) et la technologie utilisée (rendement allant de moins de 10% à 20% max) [2][3].',
    otherbenefits: 'L’autoconsommation et l’autoproduction permettent de faire des économies budgétaires à la structure qui possède les installations.',
    limits: 'L’électricité auto-produite n’est pas comptabilisée dans les émissions de la structure alors qu’en raison de la <strong>fabrication</strong> et de l’<strong>entretien des panneaux solaires</strong> (et éoliennes), le facteur d’émission réglementaire d’une électricité produite sur le réseau par ces moyens n’est pas nul : <strong>43.9 gCO2e/kWh pour des panneaux fabriqués en Chine</strong> [4].',
    rebounds: 'Passer à la consommation d’une énergie très décarbonée peut amener à une baisse de la culpabilité vis-à-vis des émissions de gaz à effet de serre et entraîner une hausse de la consommation d’électricité, par exemple par des investissements nouveaux (climatisation, équipements connectés, ...) ou par des efforts moindres de sobriété. Par ailleurs, les économies budgétaires éventuelles peuvent être ré-investies dans d’autres achats qui n’ont en général pas une empreinte carbone nulle.',
    manual: 'Le curseur permet de spécifier la quantité envisagée d’autoproduction d’électricité par pas de 10 MWh, jusqu’à un maximum de 1000 MWh. Aucune configuration avancée n’est disponible pour cette mesure.',
    humanTitle: function (level1Human, level1Unit) {
      return `Atteindre ${level1Human} ${level1Unit} d'énergie électrique renouvelable auto-consommée par an`
    }
  },
  en: {
    title: 'Self-consumption of electricity',
    level1help: 'Define a quantity of self-consumed renewable electrical energy for the laboratory in MWh per year.',
    description: 'Regulations indicate that <strong>self-consumed energy</strong> produced by on-site installations must be <strong>deducted from the total consumption</strong> of the facility. In other words, the emission factor of self-consumed electricity is zero. However, self-produced electricity that is sold back to the grid does not benefit from this advantage. On average in France, <strong>1m2 of solar panel produces from 85 to 280 kWh/year/m2</strong>. This depends on the geographical location (a factor of 2 exists between the north and south of France) and the technology used (efficiency ranging from less than 10% to a maximum of 20%)[2][3].',
    otherbenefits: 'Self-consumption and self-production lead to budget savings for the facility that owns the installations.',
    limits: 'Self-produced electricity is not counted in the emissions of the facility, but due to the <strong>manufacturing</strong> and <strong>maintenance of solar panels</strong> (and wind turbines), the regulatory emission factor of electricity produced on the grid using these means is not zero: <strong>43.9 gCO2e/kWh for panels manufactured in China</strong> [4].',
    rebounds: 'Switching to the consumption of highly decarbonized energy can reduce guilt about greenhouse gas emissions and lead to an increase in electricity consumption, for example, through new investments (air conditioning, connected devices, etc.) or reduced efforts in energy sufficiency. Furthermore, any potential budget savings can be reinvested in other purchases that generally have a non-zero carbon footprint.',
    manual: 'The slider allows you to specify the intended quantity of electricity self-production in steps of 10 MWh, up to a maximum of 1000 MWh. There are no advanced configurations available for this measure.',
    humanTitle: function (level1Human, level1Unit) {
      return `Reach ${level1Human} ${level1Unit} of self-consumed renewable electrical energy per year`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Méthodes de calcul d\'un bilan',
    year: '2022',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://bilans-ges.ademe.fr/fr/accueil/contenu/index/page/m%25C3%25A9thodes%2Bde%2Bcalcul/siGras/0'
  },
  '[2]': {
    title: 'Cellule photovoltaïque',
    year: '2021',
    authors: 'Wikipedia',
    link: 'https://fr.wikipedia.org/wiki/Cellule_photovolta%C3%AFque'
  },
  '[3]': {
    title: 'Photovoltaic geographical information system',
    year: '2019',
    authors: 'European commission',
    link: 'https://re.jrc.ec.europa.eu/pvg_download/map_index.html#!%5D.'
  },
  '[4]': {
    title: 'Renouvelables',
    year: '2020',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element/categorie/71'
  }
}

export default class SelfConsumption extends Rule {
  name = 'SelfConsumption'
  module = Modules.ELECTRICITY
  lang = LANG
  references = REFERENCES

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
  }

  get level1Max () {
    return 1000
  }

  get level1Unit () {
    return 'MWh'
  }

  get level1Step () {
    return 10
  }

  get ticks () {
    return [0, 250, 500, 750, 1000]
  }

  tickFormatter (value) {
    return value
  }

  compute (buildings) {
    let totalConsumption = 0
    let total2Reduce = this.level1 * 1000
    for (let building of buildings) {
      totalConsumption += building.getRawElectricityConsumption()
    }
    if (total2Reduce > totalConsumption) {
      total2Reduce = totalConsumption
    }
    for (let building of buildings) {
      let total = building.getRawElectricityConsumption()
      let share = total / totalConsumption
      if (total > 0) {
        if (building.electricity.isMonthly) {
          building.electricity.january -= building.electricity.january / total * share * total2Reduce
          building.electricity.february -= building.electricity.february / total * share * total2Reduce
          building.electricity.march -= building.electricity.march / total * share * total2Reduce
          building.electricity.april -= building.electricity.april / total * share * total2Reduce
          building.electricity.may -= building.electricity.may / total * share * total2Reduce
          building.electricity.june -= building.electricity.june / total * share * total2Reduce
          building.electricity.july -= building.electricity.july / total * share * total2Reduce
          building.electricity.august -= building.electricity.august / total * share * total2Reduce
          building.electricity.septembre -= building.electricity.septembre / total * share * total2Reduce
          building.electricity.octobre -= building.electricity.octobre / total * share * total2Reduce
          building.electricity.novembre -= building.electricity.novembre / total * share * total2Reduce
          building.electricity.decembre -= building.electricity.decembre / total * share * total2Reduce
        } else {
          building.electricity.total = total - share * total2Reduce
        }
      }
    }
    return buildings
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let building of this.data) {
      intensity.add(building.getElectricityCarbonIntensity(this.laboratory.area, this.laboratory.country, this.year))
    }
    return intensity
  }
}
