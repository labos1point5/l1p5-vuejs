/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '@/models/scenario/Rule.js'
import { rulesFactory, getBuiltRules } from '@/models/scenario/RulesFactory.js'
import Modules from '@/models/Modules.js'
import Synthesis from '@/models/carbon/Synthesis'

const clonedeep = require('lodash.clonedeep')
const SCENARIO_DEFAULT_TIMELINE = 2030

export default class Scenario {
  constructor (id, name, timeline, description, uuid, rules, synthesis = null, ghgi = null) {
    this.id = id
    if (name) {
      this.name = name
    } else {
      this.name = null
    }
    if (timeline) {
      this.timeline = timeline
    } else {
      this.timeline = SCENARIO_DEFAULT_TIMELINE
    }
    this.description = description
    this.uuid = uuid
    this.setGHGI(ghgi)
    this.setRules(rules)
    this.synthesis = Synthesis.createFromObj(synthesis)
  }

  get year () {
    return this.__ref.year
  }

  get laboratory () {
    return this.__ref.laboratory
  }

  getGHGIIntensity (module) {
    return this.__ref.emissions[Modules.getParentModule(module)]
  }

  setGHGI (ghgi) {
    if (ghgi !== null) {
      this.ghgi = clonedeep(ghgi)
      this.__ref = clonedeep(ghgi)
      this.synthesis = clonedeep(ghgi.synthesis)
    } else {
      this.ghgi = null
      this.__ref = null
    }
  }

  reinitGHGI (module = null) {
    if (module !== null) {
      this.ghgi[Modules.getParentModule(module)] = clonedeep(this.__ref[Modules.getParentModule(module)])
    } else {
      this.ghgi = clonedeep(this.__ref)
    }
  }

  addRule (rule) {
    let nrule = Scenario.createRuleFromObj(rule, this, true)
    nrule.setScenario(this)
    // check the rule has constrained position
    if (nrule.forceAfterRule) {
      let index = this.rules.findLastIndex(obj => obj.name === nrule.forceAfterRule)
      if (index === -1) {
        this.rules.unshift(nrule)
      } else {
        this.rules.splice(index + 1, 0, nrule)
      }
    } else {
      this.rules.unshift(nrule)
    }
  }

  ruleCanBeAdded (rule, settings) {
    rule.setScenario(this)
    let canBeAdded = false
    let sameRules = this.rules.filter(obj => obj.name === rule.name)
    if (sameRules.length === 0) {
      // do not allow the rule if no emissions,
      // ReplacePlaneNational got an infinite loop if called from here
      // when accessing section.type
      if (rule.name !== 'ReplacePlaneNational') {
        canBeAdded = rule.getTargetIntensity(settings).intensity > 0
      } else {
        canBeAdded = this.getGHGIIntensity(rule.module).intensity.intensity > 0
      }
    } else if (rule.hasLevel2Parameters) {
      for (let key of Object.keys(rule.level2)) {
        let selectedValues = []
        // get selected values
        for (let r of sameRules) {
          selectedValues = selectedValues.concat(r.level2[key].value)
        }
        // check if 'all' has already been selected
        if (selectedValues.length > 0) {
          // if all elements have been selected
          if (rule.isHierarchical(key)) {
            let missingNodes = []
            for (let pvalue of rule.getLevel2Objects(key)) {
              if (!selectedValues.includes(pvalue.id)) {
                missingNodes.push(pvalue)
              }
            }
            // missing nodes, check leaves
            if (missingNodes.length !== 0) {
              let allLeavesValuesSelected = true
              for (let node of missingNodes) {
                allLeavesValuesSelected = true
                for (let leaf of node.children) {
                  if (!selectedValues.includes(leaf.id)) {
                    allLeavesValuesSelected = false
                    break
                  }
                }
                if (!allLeavesValuesSelected) { break }
              }
              if (!allLeavesValuesSelected) {
                canBeAdded = true
              }
            }
          } else {
            if (selectedValues.length !== sameRules[0].getLevel2Values(key).length) {
              canBeAdded = true
            }
          }
        }
      }
    }
    return canBeAdded
  }

  removeRule (rule) {
    let index = this.rules.findIndex(obj => obj.id === rule.id)
    this.rules.splice(index, 1)
  }

  setRules (rules) {
    this.rules = rules.map((obj) => {
      let nrule = Scenario.createRuleFromObj(obj, this)
      nrule.setScenario(this)
      return nrule
    })
  }

  getData (module) {
    return this.__ref[Modules.getParentModule(module)]
  }

  compute (commuteSettings, module = null) {
    if (this.ghgi) {
      this.reinitGHGI(module)
      // apply the TARGET_ELEMENT rules
      let allModules = Modules.getModules(true)
      if (module) {
        allModules = [module]
      }
      for (let cmodule of allModules) {
        for (let rule of this.rules.filter(obj => Modules.getParentModule(obj.module) === Modules.getParentModule(cmodule) && obj.target === Rule.TARGET_ELEMENT)) {
          this.ghgi[Modules.getParentModule(cmodule)] = rule.compute(
            this.ghgi[Modules.getParentModule(cmodule)]
          )
        }
      }
      // compute the ghgi
      this.ghgi.compute(commuteSettings, this.ghgi.year, module)
      /*
       * apply the TARGET_EMISSIONS rules
       * level1 and level2 provided to the main rule function are an array
       * of paramters of all the same rules, ie., with the same name.
       */
      for (let cmodule of allModules) {
        let moduleRules = this.rules.filter(obj =>
          Modules.getParentModule(obj.module) === Modules.getParentModule(cmodule) &&
          obj.target === Rule.TARGET_EMISSIONS
        )
        for (let ruleName of moduleRules.map(obj => obj.name)) {
          let allRules = this.rules.filter(obj => obj.name === ruleName)
          this.ghgi.emissions[Modules.getParentModule(cmodule)].intensity = allRules[0].compute(
            this.ghgi.emissions[Modules.getParentModule(cmodule)].intensity,
            allRules.map(obj => obj.level1),
            allRules.map(obj => obj.level2),
            this.ghgi[Modules.getParentModule(cmodule)]
          )
        }
      }
      // Update synthesis
      this.synthesis = new Synthesis({
        heatings: this.ghgi.emissions.buildings.heatings.intensity,
        electricity: this.ghgi.emissions.buildings.electricity.intensity.toCarbonIntensity(),
        refrigerants: this.ghgi.emissions.buildings.refrigerants.intensity,
        water: this.ghgi.emissions.buildings.water.intensity,
        construction: this.ghgi.emissions.buildings.construction.intensity,
        commutes: this.ghgi.emissions.commutes.intensity,
        foods: this.ghgi.emissions.foods.intensity,
        travels: this.ghgi.emissions.travels.intensity,
        vehicles: this.ghgi.emissions.vehicles.intensity,
        devices: this.ghgi.emissions.devices.intensity,
        purchases: this.ghgi.emissions.purchases.intensity
      })
    }
  }

  toDatabase () {
    let rules = []
    for (let rule of this.rules) {
      rules.push(rule.toDatabase())
    }
    return {
      'id': this.id,
      'name': this.name,
      'timeline': this.timeline,
      'description': this.description,
      'uuid': this.uuid,
      'rules': rules,
      'synthesis': this.synthesis
    }
  }

  static getRules () {
    return getBuiltRules()
  }

  static get DEFAULT_TIMELINE () {
    return SCENARIO_DEFAULT_TIMELINE
  }

  // Cannot be in Rule class
  static createRuleFromObj (rule, scenario, forceNullID = false) {
    let level2 = rule.level2
    if (level2 === null) {
      level2 = {}
    }
    let ruleID = rule.id
    if (forceNullID) {
      ruleID = null
    }
    const ClassRule = rulesFactory(rule.name)
    return new ClassRule(
      ruleID,
      scenario,
      rule.level1,
      level2
    )
  }

  static createFromObj (scenario) {
    let cscenario = new Scenario(
      scenario.id,
      scenario.name,
      scenario.timeline,
      scenario.description,
      scenario.uuid,
      scenario.rules,
      scenario.synthesis,
      scenario.ghgi
    )
    return cscenario
  }
}
