/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/
import { v4 as uuidv4 } from 'uuid'

import { Tag } from '@/models/core/TagCategory.js'

/**
 * Base class for all of our models.
 */
class Model {
  /**
   *
   * @param {string[]} tags - the list of tag
   */
  constructor ({ tags = [], source = null } = {}) {
    this.tags = tags.map((obj) => Tag.createFromObj(obj))
    // keep track of the provenance of the data
    this.source = source
    // use to keep track of copy of the same object accross the app
    this.uuid = uuidv4()
  }

  /**
   * Descriptor returns a string.
   * It is used by default to hash element in Collections.
   *
   * @type {string}
   */
  get descriptor () {
    throw new Error('Model.descriptor getter must be implemented')
  }

  /**
   * isSimilarTo implements a weak equality test
   *
   * It must match two elements that are potentially incomplete.
   *
   * Use case: user add an item (might be
   * incomplete) using the form. This is added to the collection. Then user wants
   * to edit the item. We must match the right(best) item to update it.
   *
   * So a strategy to implement this is to check for different equality
   * semantics from the strongest to the weakest
   * - same object (same ref)
   * - same uuid (when it applies, we try to associate a temporary id to object
   *   we create to keep track of copies with possibly different fields values)
   * - same subset of values (e.g descriptor)
   *
   *
   * @param {Model} other
   * @returns {boolean}
   */
  isSimilarTo (other) {
    throw new Error('Model.isSimilarTo must be implemented')
  }

  /**
   *
   * Check if the two item can be aggregated.
   * In other doesn't make sense to do this + other ?
   *
   * Items can be aggregated before sending them to the backend (e.g we
   * aggregates purchases by code).
   *
   * Default implementation instructs to not aggregate
   *
   * @param {Model} other
   * @returns {boolean}
   */
  sameAggregateAs (other) {
    return false
  }

  /**
   * Generic function to aggregate two items
   * @param {Model} other
   * @throw {Error} if the two items can't be aggregated
   *
   * @returns {Model} the resuting model (new reference)
   *
   */
  aggregate (other) {
    if (this.sameAggregateAs(other)) {
      let newObj = this._doAggregate(other)
      return newObj
    }
    throw new Error('Impossible Aggregation')
  }

  /**
   * Actually do the aggregation
   * Implementers should not do the aggregation in-place
   *
   * @param {Model} other the Model to aggregate with this
   * @returns {Model} resulting Model
   */
  _doAggregate (other) {
    return this
  }

  /**
   * Delete data from source contained in the model
   * @param {*} source
   */
  deleteSource (source) {}

  /**
   * Check if activity can be submitted
   *
   * A (sub)module can be specified for instance for building (heating,
   * refrigerants ...)
   * The default implementation requires at least one item un the list
   *
   * @param {Model[]} items list of items
   * @param {str} module
   *
   * @returns {boolean} true iff the current module can be submitted
   */
  static okToSubmit (items, module = null) {
    return items.length > 0
  }

  get score () {
    return 1
  }

  get isGrouped () {
    return false
  }

  get tagsHash () {
    return this.tags
      .map((obj) => obj.name.toLowerCase())
      .sort()
      .join('')
  }

  hasTag (tag) {
    if (this.tags.length === 0) {
      return tag.isUntagged
    } else if (tag.isUntagged) {
      return (
        this.tags.filter((obj) => obj.category === tag.category).length === 0
      )
    } else {
      return this.tags.filter((obj) => obj.isEqual(tag)).length > 0
    }
  }

  hasTags (tags = []) {
    let hasTags = false
    if (tags.length === 0) {
      hasTags = true
    }
    for (let tag of tags) {
      if (this.hasTag(tag)) {
        hasTags = true
        break
      }
    }
    return hasTags
  }

  addTags (tags) {
    for (let tag of tags) {
      let ctag = tag
      if (!(tag instanceof Tag)) {
        ctag = Tag.createFromObj(tag)
      }
      // First remove tags with the same category
      this.tags = this.tags.filter((obj) => obj.category !== tag.category)
      // Add the new one
      this.tags.push(ctag)
    }
  }

  static getTagsFromNames (tags, availableTags) {
    let ftags = []
    let ctag
    let toCheck = ''
    for (let tag of tags) {
      if (tag instanceof Tag) {
        toCheck = tag.name
      } else {
        toCheck = tag.trim()
      }
      ctag = availableTags.filter((obj) => obj.name.trim() === toCheck.trim())
      if (ctag.length === 1) {
        ftags.push(ctag[0])
      }
    }
    return ftags
  }
}

export { Model as default }
