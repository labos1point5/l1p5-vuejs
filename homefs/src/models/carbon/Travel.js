/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import TravelSection from '@/models/carbon/TravelSection'
import Model from '@/models/carbon/Model.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

import ISO3166 from '@/../data/utils/ISO3166.json'

// NOTE(msimonin): There's a remaining convention with the frontend regarding
// the UNKNOWN="unknown" label for the purpose and position
export const PURPOSE_UNKNOWN = 'unknown'
export const STATUS_UNKNOWN = 'travel.unknown'

export const MODE_PLANE = 'plane'
export const MODE_TRAIN = 'train'
export const MODE_CAR = 'car'
export const MODE_CAB = 'cab'
export const MODE_BUS = 'busintercity'
export const MODE_TRAM = 'tram'
export const MODE_RER = 'rer'
export const MODE_SUBWAY = 'subway'
export const MODE_FERRY = 'ferry'

// transportation mode and correction to apply on the geodesic distance
const MODES_DEPLACEMENTS = {
  [MODE_PLANE]: (d) => d + 95,
  [MODE_TRAIN]: (d) => 1.2 * d,
  [MODE_CAR]: (d) => 1.3 * d,
  [MODE_CAB]: (d) => 1.3 * d,
  [MODE_BUS]: (d) => 1.5 * d,
  [MODE_TRAM]: (d) => 1.5 * d,
  [MODE_RER]: (d) => 1.2 * d,
  [MODE_SUBWAY]: (d) => 1.7 * d,
  [MODE_FERRY]: (d) => 1.0 * d
}

const MODE_DICTIONARY = {
  avion: MODE_PLANE,
  plane: MODE_PLANE,
  train: MODE_TRAIN,
  voiture: MODE_CAR,
  'location de vehicule': MODE_CAR,
  'vehicule personnel': MODE_CAR,
  car: MODE_CAR,
  taxi: MODE_CAB,
  cab: MODE_CAB,
  bus: MODE_BUS,
  busintercity: MODE_BUS,
  'bus intercity': MODE_BUS,
  'autocar - intercity travels': MODE_BUS,
  'intercity travels': MODE_BUS,
  'bus intercite': MODE_BUS,
  'autocar - trajets intercites': MODE_BUS,
  autocar: MODE_BUS,
  'trajets intercites': MODE_BUS,
  tram: MODE_TRAM,
  tramway: MODE_TRAM,
  rer: MODE_RER,
  metro: MODE_SUBWAY,
  subway: MODE_SUBWAY,
  ferry: MODE_FERRY
}

const ICONS = {
  [MODE_PLANE]: 'plane',
  [MODE_TRAIN]: 'train',
  [MODE_CAR]: 'car',
  [MODE_CAB]: 'cab',
  [MODE_BUS]: 'bus',
  [MODE_TRAM]: 'tram',
  [MODE_RER]: 'rer',
  [MODE_SUBWAY]: 'subway',
  [MODE_FERRY]: 'ship'
}

const ICONS_PACK = {
  [MODE_PLANE]: 'fa',
  [MODE_TRAIN]: 'fa',
  [MODE_CAR]: 'fa',
  [MODE_CAB]: 'fa',
  [MODE_BUS]: 'fa',
  [MODE_TRAM]: 'icomoon',
  [MODE_RER]: 'icomoon',
  [MODE_SUBWAY]: 'icomoon',
  [MODE_FERRY]: 'fa'
}

class TravelBase extends Model {
  constructor ({
    names = [],
    date = '',
    sections = [],
    amount = 1,
    purpose = null,
    status = null,
    tags = [],
    source = null
  } = {}) {
    super({ tags, source })
    if (Array.isArray(names)) {
      this.names = names
    } else {
      this.names = [names]
    }

    this.setDate(date)
    this.sections = sections.map((obj) => TravelSection.createFromObj(obj))
    this.setAmount(amount)

    // leave purpose and status as they are, assuming the status is in the right
    // set one must call getStatus beforehand to make sure the value lands in
    // the right set of possible values
    this.purpose = purpose
    this.status = status

    if (this.isFromDatabase()) {
      this.source = 'database'
    }
  }

  get descriptor () {
    return this.names.join('-') + '_' + this.amount
  }

  isSimilarTo (other) {
    return (
      this === other ||
      this.uuid === other.uuid ||
      this.descriptor === other.descriptor
    )
  }

  setDate (value) {
    /* let date = null
    try {
      let dateParts = value.split('/')
      if (dateParts.length === 3) {
        date = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0]
      } else {
        date = value
      }
      let d = new Date(date)
      if (isNaN(d.getTime())) {
        this.isValid = false
      }
    } catch (error) {
      this.isValid = false
    }
    this.date = date */
    this.date = value
  }

  setAmount (value) {
    this.amount = 1
    if (!isNaN(value) && value !== '') {
      this.amount = parseInt(value)
    }
  }

  addSection (section) {
    if (section instanceof TravelSection) {
      this.sections.push(section)
    } else {
      this.sections.push(TravelSection.createFromObj(section))
    }
  }

  sortSections () {
    if (this.isFromDatabase()) {
      return
    }
    let self = this
    // Keep the number of predecessors in cache (used by findStartingPoint)
    let nbPredecessors = self.sections.map(
      (s1) =>
        self.sections.filter((s2) => s2.destinationCity === s1.departureCity)
          .length
    )

    // Among the subset of sections indexed by sectionIds,
    // returns the index of first section having no predecessors if any,
    // or simply the index of the first section otherwise
    function findStartingPoint (sectionIds) {
      for (let i of sectionIds) {
        if (nbPredecessors[i] === 0) {
          return i
        }
      }
      return sectionIds[0]
    }
    // Recursively push the next sections indices to the current chain.
    // The array 'visited' is a boolean mask having the same size as this.sections.
    // It is used to speed up testing whether a section index is already present in the current chain.
    function formLongestChain (
      chain = [],
      visited = undefined,
      noRestart = false
    ) {
      if (chain.length === 0) {
        // Init
        let start = findStartingPoint(self.sections.map((v, i) => i))
        visited = self.sections.map(() => false)
        visited[start] = true
        return formLongestChain([start], visited)
      }

      let lastSection = self.sections[chain[chain.length - 1]]
      let successors = self.sections
        .map(function (sec, i) {
          return { id: i, departureCity: sec.departureCity }
        })
        .filter(
          (sec, i) =>
            !visited[i] && sec.departureCity === lastSection.destinationCity
        )

      if (successors.length === 1) {
        let nextId = successors[0].id
        visited[nextId] = true
        chain.push(nextId)
        return formLongestChain(chain, visited)
      } else if (successors.length > 0) {
        // If there are multiple branches, explore them all
        // and keep the one leading to longest connected chain
        let newChain = chain
        let newVisited = visited
        for (let next of successors) {
          let tmpChain = [...chain, next.id]
          let tmpVisited = [...visited]
          tmpVisited[next.id] = true
          tmpChain = formLongestChain(tmpChain, tmpVisited, (noRestart = true))
          if (tmpChain.length > newChain.length) {
            newChain = tmpChain
            newVisited = tmpVisited
          }
        }
        return formLongestChain(newChain, newVisited)
      } else if (!noRestart && chain.length < self.sections.length) {
        // restart from a new starting point
        let start = findStartingPoint(
          self.sections.map((v, i) => i).filter((i) => !visited[i])
        )
        visited[start] = true
        chain.push(start)
        return formLongestChain(chain, visited)
      }
      return chain
    }
    let sortedChain = formLongestChain()
    this.sections = this.sections.map((e, i) => this.sections[sortedChain[i]])
  }

  get score () {
    let score = 1
    if (!this.isFromDatabase()) {
      for (let section of this.sections) {
        if (section.score === 2) {
          score = 2
          break
        }
      }
    }
    return score
  }

  isOther () {
    return false
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return false
  }

  isInvalid () {
    return this.score === 2
  }

  latLngIsValid () {
    let isValid = true
    for (let section of this.sections) {
      if (!section.latLngIsValid()) {
        isValid = false
      }
    }
    return isValid
  }

  /**
   * Check if two travels can be aggregated.
   * Use case: before sending to the DB
   * @param {*} travel
   * @returns
   */
  sameAggregateAs (travel) {
    return (
      this.getDistance() === travel.getDistance() &&
      this.purpose === travel.purpose &&
      this.status === travel.status &&
      this.source === travel.source && // FIXME(msimonin): why / legacy code ?
      this.sections.length === travel.sections.length &&
      this.getMainTransportation() === travel.getMainTransportation() &&
      this.tagsHash === travel.tagsHash
    )
    // FIXME: transportations aren't checked so same distance by car and by plane will be considered equal
    // depending on the context this might or might not be ok
  }

  _doAggregate (travel) {
    let newTravel = this.constructor.createFromObj(this)
    newTravel.names.push(...travel.names)
    // NOTE there's an invariant here that amount is the aggregated count of
    // travels and each travel as a name so the amount here is always must
    // remain equal to the length of names
    // this should work too
    // reducedTravels[index].amount = reducedTravels[index].names.length
    newTravel.amount += travel.amount
    return newTravel
  }

  isComplete () {
    let isComplete = true
    for (let section of this.sections) {
      if (
        section.departureCity === null ||
        section.destinationCity === null ||
        section.transportation === null
      ) {
        isComplete = false
      }
    }
    return isComplete
  }

  /**
   * Get the distance of this travels taking into account
   * only all its sections (and account for round trips)
   *
   * @returns {number}
   */
  getDistance () {
    let distance = 0
    for (let section of this.sections) {
      distance += section.getRoundTripCorrectedDistance()
    }
    return distance
  }

  /**
   * Get the total distance of this travels taking into account
   * - round trip nature of the sections
   * - amount
   *
   * @returns {number}
   */
  getCumulativeDistance () {
    let distance = this.getDistance()
    return distance * this.amount
  }

  hasSectionWithPlane () {
    let withPlane = false
    for (let section of this.sections) {
      if (section.transportation === MODE_PLANE) {
        withPlane = true
        break
      }
    }
    return withPlane
  }

  getMainTransportation () {
    let transportation = null
    let maxDistance = 0
    for (let section of this.sections) {
      // FIXME(msimonin): Account for round trips ? => use getRoundTripCorrectedDistance
      // FIXME(msimonin): Use cumsum for each transportation to find out the main transportation ?
      if (section.getOneWayCorrectedDistance() > maxDistance) {
        maxDistance = section.getOneWayCorrectedDistance()
        transportation = section.transportation
      }
    }
    return transportation
  }

  getDepartureCity () {
    return this.sections[0].departureCity
  }

  getDepartureCountry () {
    return this.sections[0].departureCountry
  }

  getDestinationCity () {
    let last = this.sections.length - 1
    return this.sections[last].destinationCity
  }

  getDestinationCountry () {
    let last = this.sections.length - 1
    return this.sections[last].destinationCountry
  }

  getCarbonIntensity (year) {
    let intensities = new CarbonIntensities()
    for (let section of this.sections) {
      let nSection = TravelSection.createFromObj(section)
      // beware that this will recompute the geodesic distance with lat lng if needed
      intensities.add(nSection.getCarbonIntensity(year).multiply(this.amount))
    }
    return intensities.sum()
  }

  toDatabase (withLocation = false) {
    let toDatabase = {
      names: this.names,
      purpose: this.purpose,
      status: this.status,
      amount: this.amount,
      sections: [],
      tags: this.tags
    }
    for (let section of this.sections) {
      toDatabase.sections.push(section.toDatabase(withLocation))
    }
    return toDatabase
  }

  isFromDatabase () {
    let isFromDatabase = false
    if (this.sections.length > 0) {
      isFromDatabase = this.sections[0].isFromDatabase()
    }
    return isFromDatabase
  }

  static get MODE_PLANE () {
    return MODE_PLANE
  }

  static get MODE_TRAIN () {
    return MODE_TRAIN
  }

  static get MODE_CAR () {
    return MODE_CAR
  }

  static get MODE_CAB () {
    return MODE_CAB
  }

  static get MODE_BUS () {
    return MODE_BUS
  }

  static get MODE_TRAM () {
    return MODE_TRAM
  }

  static get MODE_RER () {
    return MODE_RER
  }

  static get MODE_SUBWAY () {
    return MODE_SUBWAY
  }

  static get MODE_FERRY () {
    return MODE_FERRY
  }

  static get PURPOSE_UNKNOWN () {
    return PURPOSE_UNKNOWN
  }

  static get STATUS_UNKNOWN () {
    return STATUS_UNKNOWN
  }

  /**
   * Check if the travels have a purpose set.
   * Return false iff none of the travels has the purpose field set.
   *
   * @param {Travel[]} travels list of travels
   * @param {string} source consider only travel from that source, if set.
   *                          Otherwise scan all travels.
   * @returns {boolean} false iff none of the travels has the purpose field set.
   */
  static hasPurposeOption (travels, source = null) {
    let nbWithPurpose = 0
    if (source === null) {
      for (let travel of travels) {
        if (
          travel.purpose !== null &&
          travel.purpose !== Travel.PURPOSE_UNKNOWN
        ) {
          nbWithPurpose += parseInt(travel.amount)
        }
      }
    } else {
      for (let travel of travels) {
        if (
          travel.purpose !== null &&
          travel.purpose !== Travel.PURPOSE_UNKNOWN &&
          travel.source === source
        ) {
          nbWithPurpose += parseInt(travel.amount)
        }
      }
    }
    return nbWithPurpose > 0
  }

  static hasStatusOption (travels, source = null) {
    let nbWithStatus = 0
    if (source === null) {
      for (let travel of travels) {
        if (travel.status !== null && travel.status !== Travel.STATUS_UNKNOWN) {
          nbWithStatus += parseInt(travel.amount)
        }
      }
    } else {
      for (let travel of travels) {
        if (
          travel.status !== null &&
          travel.status !== Travel.STATUS_UNKNOWN &&
          travel.source === source
        ) {
          nbWithStatus += parseInt(travel.amount)
        }
      }
    }
    return nbWithStatus > 0
  }

  static getTransportation (transportation) {
    let ctransportation = Travel.removeAccents(
      transportation,
      false
    ).toLowerCase()
    if (ctransportation in MODE_DICTIONARY) {
      return MODE_DICTIONARY[ctransportation]
    } else {
      return null
    }
  }

  static getPurpose (purpose, purposeDictionary) {
    let modifiedPurpose = Travel.removeAccents(purpose, false).toLowerCase()
    try {
      if (modifiedPurpose in purposeDictionary) {
        return purposeDictionary[modifiedPurpose]
      } else {
        return PURPOSE_UNKNOWN
      }
    } catch {
      return PURPOSE_UNKNOWN
    }
  }

  static getStatus (status, positionDictionary) {
    let modifiedStatus = Travel.removeAccents(status, false).toLowerCase()
    // be defensive, return STATUS_UNKNOWN if there's any problem
    try {
      if (modifiedStatus in positionDictionary) {
        return positionDictionary[modifiedStatus]
      } else {
        return STATUS_UNKNOWN
      }
    } catch {
      return STATUS_UNKNOWN
    }
  }

  static getIcon (mode) {
    return ICONS[mode]
  }

  static getIconPack (mode) {
    return ICONS_PACK[mode]
  }

  static get transportations () {
    return Object.keys(MODES_DEPLACEMENTS)
  }

  static get transportationsCorrectionFactors () {
    return MODES_DEPLACEMENTS
  }

  static removeAccents (str, tiret = true) {
    let fstr = str
    try {
      let accents =
        'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž'
      let accentsOut =
        'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz'
      str = str.split('')
      let strLen = str.length
      for (let i = 0; i < strLen; i++) {
        let x = accents.indexOf(str[i])
        if (x !== -1) {
          str[i] = accentsOut[x]
        }
      }
      if (tiret) {
        fstr = str.join('').replace('-', ' ')
      } else {
        fstr = str.join('')
      }
    } catch (error) {
      fstr = ''
    }
    return fstr
  }

  static iso3166Country (country) {
    let finalCountry = null
    if (typeof country === 'string') {
      if (country.length !== 2) {
        try {
          finalCountry = ISO3166.filter((obj) =>
            obj.names.some(
              (name) =>
                Travel.removeAccents(name.toLowerCase()) ===
                Travel.removeAccents(country.toLowerCase())
            )
          )[0].iso
        } catch (e) {
          finalCountry = null
        }
      } else {
        try {
          finalCountry = ISO3166.filter(
            (obj) =>
              Travel.removeAccents(obj.iso.toLowerCase()) ===
              Travel.removeAccents(country.toLowerCase())
          )[0].iso
        } catch (e) {
          finalCountry = null
        }
      }
    }
    return finalCountry
  }

  static initiStatutsTable (
    length,
    fillIntensities = false,
    travelPositionLabels
  ) {
    let table = {}
    for (let label of travelPositionLabels) {
      table[label] = new Array(length).fill(0)
    }
    if (fillIntensities) {
      for (let key of Object.keys(table)) {
        for (let i in table[key]) {
          table[key][i] = new CarbonIntensities()
        }
      }
      return table
    } else {
      return table
    }
  }

  static initiEmptyTable (length) {
    let table = new Array(length).fill(0)
    for (let i in table) {
      table[i] = new CarbonIntensities()
    }
    return table
  }

  static createEmpty () {
    let d = new Date()
    let cm = d.getMonth() + 1
    let cdate = d.getDate() + '/' + cm + '/' + d.getFullYear()
    return new this({
      date: cdate,
      sections: [new TravelSection()],
      amount: 1,
      purpose: null,
      status: null,
      tags: [],
      source: null
    })
  }

  /**
   * Build a travel from a js object (with the same property names)
   *
   * @param {*} payload
   */
  static createFromObj (travel) {
    let ctravel = new this({
      names: travel.names,
      date: travel.date,
      sections: [],
      amount: travel.amount,
      purpose: travel.purpose,
      // the assumption here is that status is in the right set of status
      status: travel.status,
      tags: travel.tags,
      source: travel.source
    })
    for (let section of travel.sections) {
      ctravel.addSection(section)
    }
    return ctravel
  }

  /**
   * Load data from the database
   * Handle snake-case/camel-case translation
   * @param {*} payload
   */
  static fromDatabase (payload) {
    return this.createFromObj({
      names: payload.names,
      date: payload.date,
      sections: payload.sections.map((s) => TravelSection.fromDatabase(s)),
      amount: payload.amount,
      purpose: payload.purpose,
      status: payload.status,
      tags: payload.tags,
      source: payload.source
    })
  }

  /**
   * @param {Object[]} lines - The list of travels object to handle
   * @param {String} format - The file format
   * @param {String} tags - The list of tags defined by the user
   * @param {String} source - The source file name
   *
   * @returns {Travel[]} - The list of travels objects
   */
  static async createFromLines (
    lines,
    format,
    tags,
    geoDecoder,
    source,
    positionDictionary,
    purposeDictionary
  ) {
    let sectionsToSave = []

    // decode and warmup the internal cache
    await geoDecoder.warmup(
      lines.map((line) => {
        return { city: line.departureCity, country: line.departureCountry }
      })
    )
    // same for destination (we'll benefit from the previous cache)
    await geoDecoder.warmup(
      lines.map((line) => {
        return { city: line.destinationCity, country: line.destinationCountry }
      })
    )
    for (let line of lines) {
      // request the decoder (its cache is hot)
      let cDeparture = geoDecoder.hit({
        city: line.departureCity,
        country: line.departureCountry
      })
      if (!cDeparture) {
        cDeparture = {
          lat: null,
          lng: null
        }
      }
      let cDestination = geoDecoder.hit({
        city: line.destinationCity,
        country: line.destinationCountry
      })
      if (!cDestination) {
        cDestination = {
          lat: null,
          lng: null
        }
      }

      if (line.departureCountry === null) {
        line.departureCountry = cDeparture['countryCodeAdded']
      }
      if (line.destinationCountry === null) {
        line.destinationCountry = cDestination['countryCodeAdded']
      }

      let itemToSave = this.createFromObj({
        names: line.names,
        date: line.date,
        purpose: Travel.getPurpose(line.purpose, purposeDictionary),
        status: Travel.getStatus(line.status, positionDictionary),
        amount: line.amount,
        sections: [
          {
            type: line.type,
            isRoundTrip: line.isRoundTrip,
            departureCity: line.departureCity,
            departureCountry: line.departureCountry,
            destinationCity: line.destinationCity,
            destinationCountry: line.destinationCountry,
            transportation: line.transportation,
            carpooling: line.carpooling,
            departureCityLat: cDeparture['lat'],
            departureCityLng: cDeparture['lng'],
            destinationCityLat: cDestination['lat'],
            destinationCityLng: cDestination['lng'],
            distance: line.distance
          }
        ],
        source: source
      })
      itemToSave.addTags(Model.getTagsFromNames(line.tags, tags))
      sectionsToSave.push(itemToSave)
    }
    return sectionsToSave
  }

  static exportHeader (sep = '\t') {
    let travelHeader = [
      'name',
      'amount',
      'position', // status
      'purpose',
      'tags'
    ]
    let sectionHeader = TravelSection.exportHeader()

    let computedHeader = [
      'cumulative.distance.km',
      'cumulative.emission.kg.co2e',
      'cumulative.uncertainty.kg.co2e'
    ]

    return travelHeader.concat(sectionHeader, computedHeader).join(sep)
  }

  /**
   * String representation of a travel and its sections
   * This is suposed to be tsv based format with as many lines as travel
   * sections
   * @param {*} extraColValue
   * @param {*} sep
   * @returns String
   */
  toString (sep = '\t', extraColValue = null) {
    let travelCols = [
      this.names.join(','),
      this.amount,
      this.status,
      this.purpose,
      this.tags.map((val) => val.name).join(';')
    ]
    return this.sections
      .map((s) => {
        let secStr = s.toString(sep)
        // computed columns
        // this assumes the intensity is set (by s.getCarbonIntensity(year)) :/
        let si = s.intensity.multiply(this.amount)
        let computedCols = [
          this.amount * s.getCarbonIntensityDistance(),
          si.intensity,
          si.uncertainty
        ]
        let r = [travelCols.join(sep), secStr, computedCols.join(sep)]
        if (extraColValue !== null) {
          // There is an implicit here that extraColValue must go in the first position
          // see superadmin/GES1point5.vue#373 (approx)
          r.unshift(extraColValue)
        }

        return r.join(sep)
      })
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  /**
   * Export a list of travels as a separated value field/values
   *
   * @param {*} items
   * @param {*} header
   * @param {*} extraColValue used to inject the ghgi id in superadmin/GES1point5
   * @param {*} sep
   * @returns
   */
  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let content = items.map((travel) => travel.toString(sep, extraColValue))

    if (header) {
      content.unshift(Travel.exportHeader())
    } else {
      // FIXME keep the previous behaviour which adds an \n at the beginning when
      // header = false
      content.unshift('')
    }
    let s = content.join('\n').replace(/(^\[)|(\]$)/gm, '')
    return s
  }

  static compute (
    items,
    year,
    tags = [],
    travelPositionLabels = [],
    travelPurposeLabels = []
  ) {
    let intensities = new CarbonIntensities()
    let transportationCI = Travel.initiEmptyTable(Travel.transportations.length)
    let transportationD = new Array(Travel.transportations.length).fill(0)
    let totalPurposesCI = Travel.initiEmptyTable(travelPurposeLabels.length)
    let totalPurposesD = new Array(travelPurposeLabels.length).fill(0)
    let statusCI = Travel.initiStatutsTable(
      Travel.transportations.length,
      true,
      travelPositionLabels
    )
    let statusD = Travel.initiStatutsTable(
      Travel.transportations.length,
      false,
      travelPositionLabels
    )
    let purposesCI = Travel.initiStatutsTable(
      travelPurposeLabels.length,
      true,
      travelPositionLabels
    )
    let purposesD = Travel.initiStatutsTable(
      travelPurposeLabels.length,
      false,
      travelPositionLabels
    )
    for (let item of items.filter((obj) => obj.hasTags(tags))) {
      for (let section of item.sections) {
        let intensity = section.getCarbonIntensity(year).multiply(item.amount)
        intensities.add(intensity)
        let index = Travel.transportations.indexOf(section.transportation)
        let indexm = travelPurposeLabels.indexOf(item.purpose)
        transportationCI[index].add(intensity)
        transportationD[index] +=
          item.amount * section.getCarbonIntensityDistance()
        if (indexm !== -1) {
          totalPurposesCI[indexm].add(intensity)
          totalPurposesD[indexm] +=
            item.amount * section.getCarbonIntensityDistance()
        }
        if (item.status) {
          // FIXME(msimonin): handle the impossible => mismatch between
          // item.status and travelPositionLabels element
          statusCI[item.status][index].add(intensity)
          statusD[item.status][index] +=
            item.amount * section.getCarbonIntensityDistance()
          if (indexm !== -1) {
            purposesCI[item.status][indexm].add(intensity)
            purposesD[item.status][indexm] +=
              item.amount * section.getCarbonIntensityDistance()
          }
        }
      }
    }
    Object.keys(statusCI).map(function (key) {
      statusCI[key] = statusCI[key].map((obj) => obj.sum())
    })
    Object.keys(purposesCI).map(function (key) {
      purposesCI[key] = purposesCI[key].map((obj) => obj.sum())
    })
    return {
      intensity: intensities.sum(),
      transportations: {
        intensity: transportationCI.map((obj) => obj.sum()),
        distances: transportationD
      },
      purposes: {
        intensity: totalPurposesCI.map((obj) => obj.sum()),
        distances: totalPurposesD
      },
      status: {
        intensity: statusCI,
        distances: statusD
      },
      statuspurposes: {
        intensity: purposesCI,
        distances: purposesD
      }
    }
  }
}

class Travel extends TravelBase {
  toDatabase () {
    return super.toDatabase(false)
  }
}

class TravelWithLocations extends TravelBase {
  toDatabase () {
    return super.toDatabase(true)
  }
}

export { Travel as default, TravelWithLocations }
