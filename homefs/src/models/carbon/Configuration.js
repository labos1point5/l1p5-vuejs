import _ from 'lodash'
class KeyError extends Error {
  constructor (key) {
    super(`${key} not found`)
    this.name = 'KeyError'
  }
}

class Setting {
  constructor ({
    id = null,
    section = null,
    name = null,
    value = null,
    type = null,
    component = null
  } = {}) {
    this.id = id
    this.section = section
    this.name = name
    this.value = value
    this.type = type
    this.component = component
  }

  isTunable () {
    return this.component !== null
  }

  static fromDatabase (payload) {
    return new this(payload)
  }

  toDatabase () {
    return {
      id: this.id,
      section: this.section,
      name: this.name,
      value: this.value,
      type: this.type,
      component: this.component
    }
  }
}

/**
 * The configuration
 */
class Configuration {
  static fromDatabase (payload) {
    let obj = new this()
    for (let s of payload) {
      let setting = Setting.fromDatabase(s)
      obj[setting.name] = setting
    }
    return obj
  }

  get settings () {
    return Object.keys(this).map((k) => this[k])
  }

  get sections () {
    return _.uniqBy(this.settings.map((s) => s.section))
  }

  tunable () {
    return this.settings.filter((s) => s.isTunable())
  }

  toDatabase () {
    let d = this.settings.map((s) => s.toDatabase())
    return d
  }

  /**
   * Kept for backward compatibility for now
   *
   * @param {*} section
   * @param {*} name
   * @returns
   */
  setting (section, name) {
    if (section && name) {
      return this.settings.filter(
        (obj) => obj.section === section && obj.name === name
      )[0]
    } else if (name) {
      return this.settings.filter((obj) => obj.name === name)[0]
    } else if (section) {
      return this.settings.filter((obj) => obj.section === section)
    }
  }

  get (key) {
    if (key in this) {
      return this[key].value
    }
    throw new KeyError(key)
  }

  toOptions () {
    let r = {}
    for (let k of Object.keys(this)) {
      r[k] = this[k].value
    }
    return r
  }

  tunableSettings (section) {
    return this.setting(section).filter((s) => s.isTunable())
  }
}

export { Configuration }
