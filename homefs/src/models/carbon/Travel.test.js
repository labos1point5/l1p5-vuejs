import { TravelCollection } from './Collection'
import * as T from './Travel'
import Travel from './Travel'

const PURPOSE_OTHER = 'other'
const STATUS_ENGINEER = 'ITA'

function parisToRennes () {
  let section = {
    transportation: T.MODE_PLANE,
    departureCity: 'Paris',
    departureCountry: 'FR',
    destinationCity: 'Rennes',
    destinationCountry: 'FR',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    destinationCityLat: 48.11,
    destinationCityLng: -1.68,
    isRoundTrip: false
  }

  return Travel.createFromObj({
    names: ['paris_rennes'],
    date: '2022-04-01',
    sections: [section],
    amount: 1,
    purpose: PURPOSE_OTHER,
    status: STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

function rennesParisLilleMixingRTs () {
  let section1 = {
    transportation: T.MODE_PLANE,
    departureCity: 'Rennes',
    departureCountry: 'FR',
    destinationCity: 'Paris',
    destinationCountry: 'FR',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.11,
    departureCityLng: -1.68,
    destinationCityLat: 48.8555,
    destinationCityLng: 2.348,
    isRoundTrip: false
  }

  let section2 = {
    transportation: T.MODE_PLANE,
    destinationCity: 'Lille',
    destinationCountry: 'FR',
    departureCity: 'Paris',
    departureCountry: 'FR',
    carpooling: 1,
    distance: 0,
    destinationCityLat: 50.631,
    destinationCityLng: 3.0714,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    isRoundTrip: true
  }

  return Travel.createFromObj({
    names: ['rennes_paris_lille_mixing_rts'],
    date: '2022-04-01',
    sections: [section1, section2],
    amount: 1,
    purpose: PURPOSE_OTHER,
    status: STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

function rennesParisLilleNotMixingRTs () {
  let section1 = {
    transportation: T.MODE_PLANE,
    departureCity: 'Rennes',
    departureCountry: 'FR',
    destinationCity: 'Paris',
    destinationCountry: 'FR',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.11,
    departureCityLng: -1.68,
    destinationCityLat: 48.8555,
    destinationCityLng: 2.348,
    isRoundTrip: false
  }

  let section2 = {
    transportation: T.MODE_PLANE,
    destinationCity: 'Lille',
    destinationCountry: 'FR',
    departureCity: 'Paris',
    departureCountry: 'FR',
    carpooling: 1,
    distance: 0,
    destinationCityLat: 50.631,
    destinationCityLng: 3.0714,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    isRoundTrip: false
  }
  let section3 = {
    transportation: T.MODE_PLANE,
    departureCity: 'Lille',
    departureCountry: 'FR',
    destinationCity: 'Paris',
    destinationCountry: 'FR',
    carpooling: 1,
    distance: 0,
    departureCityLat: 50.631,
    departureCityLng: 3.0714,
    destinationCityLat: 48.8555,
    destinationCityLng: 2.348,
    isRoundTrip: false
  }

  return Travel.createFromObj({
    names: ['rennes_paris_lille_mixing_rts'],
    date: '2022-04-01',
    sections: [section1, section2, section3],
    amount: 1,
    purpose: PURPOSE_OTHER,
    status: STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

function parisToNewYork () {
  let section = {
    transportation: T.MODE_PLANE,
    departureCity: 'Paris',
    departureCountry: 'FR',
    destinationCity: 'NEW YORK',
    destinationCountry: 'US',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    destinationCityLat: 40.68,
    destinationCityLng: -74.04,
    isRoundTrip: false
  }

  return Travel.createFromObj({
    names: ['paris_newyork'],
    date: '2022-04-01',
    sections: [section],
    amount: 1,
    purpose: PURPOSE_OTHER,
    status: STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

function ParisToAuckland () {
  let section1 = {
    transportation: T.MODE_PLANE,
    departureCity: 'Paris',
    departureCountry: 'FR',
    destinationCity: 'ABU DABI',
    destinationCountry: 'UAE',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    destinationCityLat: 24.3867414,
    destinationCityLng: 54.3938126,
    isRoundTrip: false
  }

  let section2 = {
    transportation: T.MODE_PLANE,
    departureCity: 'ABU DABI',
    departureCountry: 'UAE',
    destinationCity: 'AUCKLAND',
    destinationCountry: 'NZ',
    carpooling: 1,
    distance: 0,
    departureCityLat: 24.3867414,
    departureCityLng: 54.3938126,
    destinationCityLat: -36.8596971,
    destinationCityLng: 174.5413149,
    isRoundTrip: false
  }

  return Travel.createFromObj({
    names: ['paris_auckland'],
    date: '2022-04-01',
    sections: [section1, section2],
    amount: 1,
    purpose: PURPOSE_OTHER,
    status: STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

describe('Reduce travels', () => {
  test('Reduce two aggregable travels into one', () => {
    let t1 = parisToRennes()
    t1.names = ['paris_rennes_1']
    let t2 = parisToRennes()
    t2.names = ['paris_rennes_2']

    let collection = new TravelCollection()
    collection.add(t1)
    collection.add(t2)

    let reducedItems = collection.reduceItems()

    expect(reducedItems.length).toBe(1)
    // name must be a list of string
    expect(reducedItems[0].names).toEqual(['paris_rennes_1', 'paris_rennes_2'])
  })

  test('Reduce two different travels', () => {
    let t1 = parisToRennes()
    let t2 = parisToNewYork()

    let collection = new TravelCollection()
    collection.add(t1)
    collection.add(t2)

    let reducedItems = collection.reduceItems()

    expect(reducedItems.length).toBe(2)
    expect(reducedItems[0].names).toEqual(['paris_rennes'])
    expect(reducedItems[1].names).toEqual(['paris_newyork'])
  })

  test('Reduce with already reduced travels', () => {
    let t1 = parisToRennes()
    t1.names = ['paris_rennes_1']
    t1.amount = 3
    let t2 = parisToRennes()
    t1.names = ['paris_rennes_2']
    t2.amount = 4

    let collection = new TravelCollection()
    collection.add(t1)
    collection.add(t2)

    let reducedItems = collection.reduceItems()

    expect(reducedItems.length).toBe(1)
    expect(reducedItems[0].amount).toBe(7)
  })
})

describe('Compute plane distance', () => {
  test('Paris -> Rennes', () => {
    let travel = parisToRennes()
    // google earth distance + hardcoded corrected distance
    expect(travel.getCumulativeDistance()).toBeGreaterThan(380)
    expect(travel.getCumulativeDistance()).toBeLessThan(420)
  })

  test('Paris -> New York', () => {
    let travel = parisToNewYork()
    // google earth distance + hardcoded corrected distance
    expect(travel.getCumulativeDistance()).toBeGreaterThan(5900)
    expect(travel.getCumulativeDistance()).toBeLessThan(6100)
  })
})

describe('Export travels', () => {
  test('Paris -> Rennes', () => {
    let travel = parisToRennes()
    expect(travel.sections[0].toString()).toEqual(
      [
        'NA',
        'plane',
        '1',
        'false',
        '308.18227034329306',
        '403.18227034329306',
        '403.18227034329306',
        '0',
        '0'
      ].join('\t')
    )
  })

  test('Paris -> New York', () => {
    let travel = parisToNewYork()
    expect(travel.sections[0].toString()).toEqual(
      [
        'MX',
        'plane',
        '1',
        'false',
        '5841.189422053022',
        '5936.189422053022',
        '5936.189422053022',
        '0',
        '0'
      ].join('\t')
    )
  })

  test('Paris -> Auckland (2 sections)', () => {
    let travel = ParisToAuckland()
    // we have 2 lines ( one per unique section )
    expect(travel.toString().split('\n').length).toBe(2)
  })

  test('Mixing RT and non RT sections __getDistance()', () => {
    let travel1 = rennesParisLilleMixingRTs()
    let travel2 = rennesParisLilleNotMixingRTs()
    expect(travel1.getCumulativeDistance()).toBe(
      travel2.getCumulativeDistance()
    )
  })

  test('GetTotalDistance account for amount', () => {
    let travel1 = parisToNewYork()
    let travel2 = parisToNewYork()
    travel1.amount = 7
    expect(travel1.getCumulativeDistance()).toBe(
      7 * travel2.getCumulativeDistance()
    )
  })
})

describe('Section distance correction', () => {
  test('Correct for transportation', () => {
    let travel = parisToRennes()
    let section = travel.sections[0]

    let geodesic = section.distance

    section.transportation = Travel.MODE_PLANE
    expect(section.__getDistance({ cTransportation: true })).toBe(geodesic + 95)
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_TRAIN
    expect(section.__getDistance({ cTransportation: true })).toBe(
      1.2 * geodesic
    )
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_CAR
    expect(section.__getDistance({ cTransportation: true })).toBe(
      1.3 * geodesic
    )
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_CAB
    expect(section.__getDistance({ cTransportation: true })).toBe(
      1.3 * geodesic
    )
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_BUS
    expect(section.__getDistance({ cTransportation: true })).toBe(
      1.5 * geodesic
    )
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_TRAM
    expect(section.__getDistance({ cTransportation: true })).toBe(
      1.5 * geodesic
    )
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_RER
    expect(section.__getDistance({ cTransportation: true })).toBe(
      1.2 * geodesic
    )
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_SUBWAY
    expect(section.__getDistance({ cTransportation: true })).toBe(
      1.7 * geodesic
    )
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )

    section.transportation = Travel.MODE_FERRY
    expect(section.__getDistance({ cTransportation: true })).toBe(1 * geodesic)
    expect(section.__getDistance({ cTransportation: true })).toBe(
      section.getOneWayCorrectedDistance()
    )
  })

  test('Correct for carpooling', () => {
    let travel = parisToRennes()
    let section = travel.sections[0]

    let geodesic = section.distance

    section.transportation = Travel.MODE_CAR
    section.carpooling = 2
    expect(section.__getDistance({ cCarpooling: true })).toBe(geodesic / 2)

    section.transportation = Travel.MODE_CAB
    section.carpooling = 2
    expect(section.__getDistance({ cCarpooling: true })).toBe(
      geodesic * (1 + 1 / 2)
    )
  })

  test('Correct for roundtrip', () => {
    let travel = parisToRennes()
    let section = travel.sections[0]

    let geodesic = section.distance

    section.isRoundTrip = true
    expect(section.__getDistance({ cRoundTrip: true })).toBe(2 * geodesic)
  })
})

describe('getStatus', () => {
  test('undefined value', () => {
    expect(Travel.getStatus(undefined, {})).toBe(T.STATUS_UNKNOWN)
    expect(Travel.getStatus()).toBe(T.STATUS_UNKNOWN)
  })
})
