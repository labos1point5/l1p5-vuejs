/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { CarbonIntensity } from '@/models/carbon/CarbonIntensity.js'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/transportsFactors.json'
import EmissionFactor from '@/models/carbon/EmissionFactor.js'

export default class CommuteSection {
  constructor (mode, distance, isDay2, pooling, engine) {
    this.mode = mode
    this.distance = distance
    this.isDay2 = isDay2
    this.pooling = pooling
    this.engine = engine
  }

  get isPooling () {
    return this.pooling > 1
  }

  getEmissionFactor (citySize = null, year = null) {
    let ef = EmissionFactor.createFromObj()
    if (this.mode === 'walking') {
      ef.group = 'walking'
    } else if (this.mode === 'bike') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['bike']['muscular'])
    } else if (this.mode === 'ebike') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['bike']['electric'])
    } else if (this.mode === 'escooter') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['scooter']['electric'])
    } else if (this.mode === 'motorbike') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['motorbike']['gasoline'])
    } else if (this.mode === 'car') {
      ef = EmissionFactor.createFromObj(VEHICLES_FACTORS['car'][this.engine])
    } else if (this.mode === 'bus') {
      if (citySize === 0) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.smallcity'])
      } else if (citySize === 1) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.mediumcity'])
      } else {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.bigcity'])
      }
    } else if (this.mode === 'busintercity') {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['bus']['bus.intercity'])
    } else if (this.mode === 'train') {
      // The survey asks for dailyDistance, to choose the emission
      // factor, we considere its go and back
      let commuteDistance = this.distance / 2
      if (commuteDistance < 200) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['train.shortdistance'])
      } else {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['train.longdistance'])
      }
    } else if (this.mode === 'tram') {
      if (citySize === 0) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['tram.mediumcity'])
      } else if (citySize === 1) {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['tram.mediumcity'])
      } else {
        ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['tram.bigcity'])
      }
    } else if (this.mode === 'expressrailway') {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['rer'])
    } else if (this.mode === 'subway') {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['subway'])
    }
    return ef.getFactor(year)
  }

  getCarbonIntensity (citySize, year = null) {
    let ef = this.getEmissionFactor(citySize, year)
    let distance = this.distance
    if (this.mode === 'car' || this.mode === 'motorbike') {
      distance = distance / this.pooling
    }
    return new CarbonIntensity(
      distance * ef.total.total,
      distance * ef.total.total * ef.total.uncertainty,
      null,
      null,
      ef.group
    )
  }

  static createFromObj (section) {
    return new CommuteSection(
      section.mode,
      section.distance,
      section.isDay2,
      section.pooling,
      section.engine
    )
  }
}
