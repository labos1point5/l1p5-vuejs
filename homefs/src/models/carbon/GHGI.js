/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Building from '@/models/carbon/Building'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'
import Commute from '@/models/carbon/Commute.js'
import ComputerDevice from '@/models/carbon/ComputerDevice'
import Purchase from '@/models/carbon/Purchase'
import Travel from '@/models/carbon/Travel'
import Vehicle from '@/models/carbon/Vehicle'
import Food from '@/models/carbon/Food'
import Modules from '@/models/Modules.js'
import {
  BuildingCollection,
  DeviceCollection,
  PurchaseCollection,
  VehicleCollection,
  TravelCollection,
  CommuteCollection,
  FoodCollection
} from '@/models/carbon/Collection.js'
import Synthesis from '@/models/carbon/Synthesis'

/**
 * Build a submitted object and making sure all modules are in there
 * @param {Object} submitted
 *
 * @return{Object} the submitted Object with all modules as keys
 */
function makeSubmitted (submitted) {
  let s = {}
  for (let module of Modules.getModules(true)) {
    s[module] = false
  }
  if (submitted) {
    Object.assign(s, submitted)
  }
  return s
}

export default class GHGI {
  constructor (id = null, uuid = null, year = null, created = null,
    description = null, nResearcher = 0, nProfessor = 0, nEngineer = 0,
    nStudent = 0, budget = 0, surveyMessage = null, surveyCloneYear = null,
    commutesActive = true, foodsActive = true, laboratory = null,
    submitted = null, buildings = [], purchases = [], devices = [],
    vehicles = [], travels = [], commutes = [], foods = [], synthesis = null,
    emissions = {}) {
    this.id = id
    this.uuid = uuid
    this.year = year
    this.created = created
    this.description = description
    this.nResearcher = nResearcher
    this.nProfessor = nProfessor
    this.nEngineer = nEngineer
    this.nStudent = nStudent
    this.budget = budget

    this.surveyMessage = surveyMessage
    this.surveyCloneYear = surveyCloneYear
    this.commutesActive = commutesActive
    this.foodsActive = foodsActive

    if (laboratory) {
      this.laboratory = laboratory
    } else {
      this.laboratory = {
        'area': null,
        'country': null,
        'citySize': null
      }
    }

    this.submitted = makeSubmitted(submitted)
    // Create collections
    this.buildings = new BuildingCollection()
    this.purchases = new PurchaseCollection()
    this.devices = new DeviceCollection()
    this.vehicles = new VehicleCollection()
    this.travels = new TravelCollection()
    this.commutes = new CommuteCollection()
    this.foods = new FoodCollection()

    // Add objects to collections
    buildings.map((obj) => this.buildings.add(obj))
    purchases.map((obj) => this.purchases.add(obj))
    devices.map((obj) => this.devices.add(obj))
    vehicles.map((obj) => this.vehicles.add(obj))
    travels.map((obj) => this.travels.add(obj))
    commutes.map((obj) => this.commutes.add(obj))
    foods.map((obj) => this.foods.add(obj))

    this.synthesis = Synthesis.createFromObj(synthesis)
    this.emissions = emissions
  }

  get surveyActive () {
    return this.commutesActive || this.foodsActive
  }

  get intensity () {
    let intensity = new CarbonIntensities()
    for (let module of Object.keys(Modules.MODULES)) {
      if (Modules.MODULES[module].length === 0) {
        intensity.add(this.emissions[module].intensity)
      } else {
        for (let submodule of Modules.MODULES[module]) {
          intensity.add(this.emissions[module][submodule].intensity)
        }
      }
    }
    return intensity.sum()
  }

  get workforce () {
    return this.nResearcher + this.nProfessor + this.nEngineer + this.nStudent
  }

  get buildingsSubmitted () {
    let allSubmitted = true
    for (let module of Modules.MODULES[Modules.BUILDINGS]) {
      if (!this.submitted[module]) {
        allSubmitted = false
        break
      }
    }
    return allSubmitted
  }

  get hasOneBuildingsModuleSubmitted () {
    let hasOneSubmitted = false
    for (let module of Modules.MODULES[Modules.BUILDINGS]) {
      if (this.submitted[module]) {
        hasOneSubmitted = true
        break
      }
    }
    return hasOneSubmitted
  }

  get allModulesSubmitted () {
    let allSubmitted = true
    for (let module of Modules.getModules(true)) {
      if (!this.submitted[module]) {
        allSubmitted = false
        break
      }
    }
    return allSubmitted
  }

  submit (module) {
    if (Object.keys(this.submitted).includes(module)) {
      this.submitted[module] = true
    }
  }

  updateFromObj (obj) {
    for (let key of Object.keys(obj)) {
      this[key] = obj[key]
      if (key === 'submitted') {
        this[key] = makeSubmitted(obj[key])
      }
    }
  }

  unsubmit (module) {
    if (Object.keys(this.submitted).includes(module)) {
      this.submitted[module] = false
    }
  }

  activateDesactivateSurvey (module = null) {
    if (module === Modules.FOODS) {
      this.foodsActive = !this.foodsActive
    } else if (module === Modules.COMMUTES) {
      this.commutesActive = !this.commutesActive
    } else {
      this.foodsActive = !this.foodsActive
      this.commutesActive = !this.commutesActive
    }
  }

  toString (sep = '\t') {
    return [
      this.uuid,
      this.created,
      this.year,
      this.laboratory.id,
      this.laboratory.name,
      this.laboratory.referent.email,
      this.laboratory.administrations.map(obj => obj.name).join(';'),
      this.laboratory.disciplines.map(obj => obj.name + ':' + obj.percentage).join(';'),
      this.laboratory.mainSite,
      this.laboratory.country,
      this.laboratory.area,
      this.nResearcher,
      this.nProfessor,
      this.nEngineer,
      this.nStudent,
      this.budget,
      this.submitted.vehicles,
      Math.round(this.synthesis.vehicles.getIntensity()),
      Math.round(this.synthesis.vehicles.getUncertainty()),
      this.submitted.travels,
      Math.round(this.synthesis.travels.getIntensity()),
      Math.round(this.synthesis.travels.getUncertainty()),
      Math.round(this.synthesis.travels.getIntensity(true)),
      Math.round(this.synthesis.travels.getUncertainty(true)),
      this.submitted.commutes,
      Math.round(this.synthesis.commutes.getIntensity()),
      Math.round(this.synthesis.commutes.getUncertainty()),
      this.submitted.foods,
      Math.round(this.synthesis.foods.getIntensity()),
      Math.round(this.synthesis.foods.getUncertainty()),
      this.submitted.heatings,
      Math.round(this.synthesis.heatings.getIntensity()),
      Math.round(this.synthesis.heatings.getUncertainty()),
      this.submitted.electricity,
      Math.round(this.synthesis.electricity.getIntensity()),
      Math.round(this.synthesis.electricity.getUncertainty()),
      this.submitted.refrigerants,
      Math.round(this.synthesis.refrigerants.getIntensity()),
      Math.round(this.synthesis.refrigerants.getUncertainty()),
      this.submitted.water,
      Math.round(this.synthesis.water.getIntensity()),
      Math.round(this.synthesis.water.getUncertainty()),
      this.submitted.construction,
      Math.round(this.synthesis.construction.getIntensity()),
      Math.round(this.synthesis.construction.getUncertainty()),
      this.submitted.devices,
      Math.round(this.synthesis.devices.getIntensity()),
      Math.round(this.synthesis.devices.getUncertainty()),
      this.submitted.purchases,
      Math.round(this.synthesis.purchases.getIntensity()),
      Math.round(this.synthesis.purchases.getUncertainty())
    ].join(sep)
  }

  compute (commuteSettings, year = null, module = null) {
    let cyear = this.year
    if (year !== null) {
      cyear = year
    }
    if (module) {
      if ([Modules.Buildings, Modules.HEATINGS, Modules.ELECTRICITY, Modules.REFRIGERANTS].includes(module)) {
        this.emissions.buildings = Building.compute(
          this.buildings,
          this.laboratory.area,
          this.laboratory.country,
          cyear
        )
      } else if (module === Modules.COMMUTES) {
        this.emissions.commutes = Commute.compute(
          this.commutes,
          this.laboratory.citySize,
          this.nResearcher,
          this.nProfessor,
          this.nEngineer,
          this.nStudent,
          cyear,
          commuteSettings
        )
      } else if (module === Modules.FOODS) {
        this.emissions.foods = Food.compute(
          this.foods,
          this.workforce,
          cyear,
          commuteSettings
        )
      } else if (module === Modules.PURCHASES) {
        this.emissions.purchases = Purchase.compute(
          this.purchases,
          cyear
        )
      } else if (module === Modules.DEVICES) {
        this.emissions.devices = ComputerDevice.compute(
          this.devices
        )
      } else if (module === Modules.VEHICLES) {
        this.emissions.vehicles = Vehicle.compute(
          this.vehicles,
          cyear
        )
      } else if (module === Modules.TRAVELS) {
        this.emissions.travels = Travel.compute(
          this.travels,
          cyear
        )
      }
    } else {
      this.emissions = {
        buildings: Building.compute(
          this.buildings,
          this.laboratory.area,
          this.laboratory.country,
          cyear
        ),
        commutes: Commute.compute(
          this.commutes,
          this.laboratory.citySize,
          this.nResearcher,
          this.nProfessor,
          this.nEngineer,
          this.nStudent,
          cyear,
          commuteSettings
        ),
        foods: Food.compute(
          this.foods,
          this.workforce,
          cyear,
          commuteSettings
        ),
        vehicles: Vehicle.compute(
          this.vehicles,
          cyear
        ),
        travels: Travel.compute(
          this.travels,
          cyear
        ),
        devices: ComputerDevice.compute(
          this.devices
        ),
        purchases: Purchase.compute(
          this.purchases,
          cyear
        )
      }
    }

    // Update synthesis
    this.synthesis = new Synthesis({
      heatings: this.emissions.buildings.heatings.intensity,
      electricity: this.emissions.buildings.electricity.intensity.toCarbonIntensity(),
      refrigerants: this.emissions.buildings.refrigerants.intensity,
      water: this.emissions.buildings.water.intensity,
      construction: this.emissions.buildings.construction.intensity,
      commutes: this.emissions.commutes.intensity,
      foods: this.emissions.foods.intensity,
      travels: this.emissions.travels.intensity,
      vehicles: this.emissions.vehicles.intensity,
      devices: this.emissions.devices.intensity,
      purchases: this.emissions.purchases.intensity
    })
  }

  static exportHeader (sep = '\t') {
    return [
      'ghgi.uuid',
      'created.date',
      'year',
      'laboratory.id',
      'laboratory.name',
      'referent.email',
      'supervisions',
      'disciplines',
      'city',
      'country',
      'region',
      'no.researchers',
      'no.teacher',
      'no.ita',
      'no.postdoc',
      'budget',
      'vehicles.submitted',
      'vehicles.emission.kg.co2e',
      'vehicles.uncertainty.kg.co2e',
      'travels.submitted',
      'travels.emission.kg.co2e',
      'travels.uncertainty.kg.co2e',
      'travelswc.emission.kg.co2e',
      'travelswc.uncertainty.kg.co2e',
      'commutes.submitted',
      'commutes.emission.kg.co2e',
      'commutes.uncertainty.kg.co2e',
      'foods.submitted',
      'foods.emission.kg.co2e',
      'foods.uncertainty.kg.co2e',
      'heating.submitted',
      'heating.emission.kg.co2e',
      'heating.uncertainty.kg.co2e',
      'electricity.submitted',
      'electricity.emission.kg.co2e',
      'electricity.uncertainty.kg.co2e',
      'refrigerants.submitted',
      'refrigerants.emission.kg.co2e',
      'refrigerants.uncertainty.kg.co2e',
      'water.submitted',
      'water.emission.kg.co2e',
      'water.uncertainty.kg.co2e',
      'construction.submitted',
      'construction.emission.kg.co2e',
      'construction.uncertainty.kg.co2e',
      'devices.submitted',
      'devices.emission.kg.co2e',
      'devices.uncertainty.kg.co2e',
      'purchases.submitted',
      'purchases.emission.kg.co2e',
      'purchases.uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = GHGI.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static createFromObj (item) {
    return new GHGI(
      item.id,
      item.uuid,
      item.year,
      item.created,
      item.description,
      item.nResearcher,
      item.nProfessor,
      item.nEngineer,
      item.nStudent,
      item.budget,
      item.surveyMessage,
      item.surveyCloneYear,
      item.commutesActive,
      item.foodsActive,
      item.laboratory,
      item.submitted,
      item.buildings,
      item.purchases,
      item.devices,
      item.vehicles,
      item.travels,
      item.commutes,
      item.foods,
      item.synthesis,
      item.emissions
    )
  }
}
