import { CarbonIntensity } from './CarbonIntensity.js'
import GHGI from './GHGI.js'
import Modules from '../Modules.js'

describe('GHGI is safe without consumption data', () => {
  test('', () => {
    let ghgi = new GHGI()
    // test that's it's safe to call this
    ghgi.compute()

    // and we get some correct computed values
    for (let module of Modules.getModules(true)) {
      expect(ghgi.synthesis[module]).toStrictEqual(new CarbonIntensity())
    }
  })
})
