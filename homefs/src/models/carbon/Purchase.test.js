import Purchase from './Purchase'
import Modules from '@/models/Modules.js'

function pipette1 () {
  return Purchase.createFromObj({
    code: 'nb05',
    amount: '1'
  })
}

function pipette2 () {
  return Purchase.createFromObj({
    code: 'nb05',
    amount: '2'
  })
}

function recharge () {
  return Purchase.createFromObj({
    code: 'ad25',
    amount: '1'
  })
}

describe('Reduce purchases', () => {
  test('Reduce two same purchases into one', () => {
    let p1 = pipette1()
    let p2 = pipette2()
    let p3 = recharge()
    let purchases = Purchase.reduce([p1, p2, p3])
    expect(purchases.length).toBe(2)
    expect(purchases.map(p => p.code)).toEqual(['nb05', 'ad25'])
  })
})

describe('Getters', () => {
  test('Pipette1', () => {
    let p = pipette1()
    expect(p.module).toBe(Modules.PURCHASES)
    expect(p.category).toBe('lab.equipment')
  })
  test('Recharge', () => {
    let r = recharge()
    expect(r.module).toBe(Modules.VEHICLES)
    expect(r.category).toBe('lab.life')
  })
})

describe('Compute carbon intensity', () => {
  test('Pipette1', () => {
    let p = pipette1()
    expect(p.getCarbonIntensity(2022).getIntensity()).toBe(0.63)
  })
  test('Recharge', () => {
    let r = recharge()
    expect(r.getCarbonIntensity(2022).getIntensity()).toBe(0.56)
  })
})

describe('Export purchases', () => {
  test('Pipette1', () => {
    let p = pipette1()
    expect(p.toString()).toEqual('NB05\tpurchases\t3.2\t4.2\tlab.equipment\tPipettes reutilisables\t1\t0\t0')
  })
  test('Recharge', () => {
    let r = recharge()
    expect(r.toString()).toBe('AD25\tvehicles\t2.1\t2.1\tlab.life\tRecharge electrique pour vehicules\t1\t0\t0')
  })
})
