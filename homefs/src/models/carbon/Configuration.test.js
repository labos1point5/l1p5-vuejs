import { readFile } from 'fs/promises'
const path = require('path')

const { Configuration } = require('./Configuration')

const fakeDataDir = path.resolve(__dirname, '../../..', 'backend/carbon/fake')
const FAKE_CONF_PATH = path.join(fakeDataDir, 'fakeConf.json')

describe('Configuration.get', () => {
  test('key not found', () => {
    let c = new Configuration()
    expect(() => c.get('not-found')).toThrow('not-found not found')
  })

  test('realistic conf with fakeConf', async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    let conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)

    expect(conf.get('ENTITY_CLASS')).toBe('Laboratory')
    expect(() => conf.get('ENTTY_CLASS')).toThrow('ENTTY_CLASS not found')
  })
})

describe('Configuration.toKV', () => {
  test('empty conf leads to empy KV', () => {
    let c = new Configuration()
    expect(c.toOptions()).toMatchObject({})
  })

  test('realistic conf with fakeConf', async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    let settingsPayload = JSON.parse(confPayload).settings
    let conf = Configuration.fromDatabase(settingsPayload)

    // test only a subset
    expect(conf.toOptions()).toMatchObject({ ENTITY_CLASS: 'Laboratory' })

    // more generally check that we don't miss any key/val
    let r = {}
    for (let k in Object(settingsPayload).keys()) {
      r[k] = settingsPayload[k].value
    }
    expect(conf.toOptions()).toMatchObject(r)
  })
})
