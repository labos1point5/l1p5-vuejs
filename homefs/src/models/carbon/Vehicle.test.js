import Vehicle from './Vehicle'

function dieselCar () {
  return Vehicle.createFromObj({
    name: 'diesel_car',
    type: 'car',
    engine: 'diesel',
    unit: 'km',
    consumption: {
      january: 1,
      february: 1,
      march: 1,
      april: 1,
      may: 1,
      june: 1,
      july: 1,
      august: 1,
      septembre: 1,
      octobre: 1,
      novembre: 1,
      decembre: 1,
      total: 0,
      isMonthly: true
    }
  })
}

function helicopterPistons () {
  return Vehicle.createFromObj({
    name: 'helicopter_pistons',
    type: 'aircraft',
    engine: 'helicopter.pistons',
    unit: 'liter',
    power: 8800,
    noEngine: 1,
    shp: 0,
    consumption: {
      january: 0,
      february: 0,
      march: 0,
      april: 0,
      may: 0,
      june: 0,
      july: 0,
      august: 0,
      septembre: 0,
      octobre: 0,
      novembre: 0,
      decembre: 0,
      total: 1,
      isMonthly: false
    }
  })
}

describe('Compute vehicles consumption', () => {
  test('Diesel car', () => {
    let car = dieselCar()
    expect(car.getConsumption()).toBe(12)
  })
  test('Helicopter pistons', () => {
    let helicopter = helicopterPistons()
    expect(helicopter.getConsumption()).toBe(3.0023999999999993)
  })
})

describe('Compute carbon intensity', () => {
  test('Diesel car', () => {
    let car = dieselCar()
    expect(car.getCarbonIntensity(2022).getIntensity()).toBe(2.5451999999999995)
  })
  test('Helicopter pistons', () => {
    let helicopter = helicopterPistons()
    expect(helicopter.getCarbonIntensity(2022).getIntensity()).toBe(9.041127119999999)
  })
})

describe('Export vehicles', () => {
  test('Diesel car', () => {
    let car = dieselCar()
    expect(car.toString()).toEqual('diesel_car\tcar\tdiesel\t12\tkm\t0\t0')
  })
  test('Helicopter pistons', () => {
    let helicopter = helicopterPistons()
    expect(helicopter.toString()).toEqual('helicopter_pistons\taircraft\thelicopter.pistons\t3.0023999999999993\tliter\t0\t0')
  })
})
