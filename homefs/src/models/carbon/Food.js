/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import {
  CarbonIntensities,
  CarbonIntensity
} from '@/models/carbon/CarbonIntensity.js'
import Survey from '@/models/carbon/Survey.js'
import Meal from '@/models/carbon/Meal.js'
import FOOD_FACTORS from '@/../data/factors/carbon/foodFactors.json'

export default class Food extends Survey {
  constructor (options) {
    super(options)
    this.mealsIntensity = new CarbonIntensity()
  }

  get nMeals () {
    return this.nWorkingDay * 2
  }

  getAmount (meal) {
    let cmeal = this.meals.filter((obj) => obj.type === meal)
    if (cmeal.length === 0) {
      return 0
    } else {
      return cmeal[0].amount
    }
  }

  toString (sep = '\t') {
    let values = [
      this.seqID,
      this.mdeleted,
      this.nWorkingDay,
      this.position,
      this.nCateringMeal
    ]
    for (let meal of Food.MEALS) {
      values.push(this.getAmount(meal))
    }
    let message = this.message
    if (message) {
      message = message.replace(new RegExp('\\n', 'g'), ' ')
    }
    values.push(Math.round(this.mealsIntensity.intensity))
    values.push(Math.round(this.mealsIntensity.uncertainty))
    values.push(message)
    values.push(this.tags.map((val) => val.name).join(';'))
    return values.join(sep)
  }

  addMeal (meal) {
    if (meal instanceof Meal) {
      this.meals.push(meal)
    } else {
      this.meals.push(Meal.createFromObj(meal))
    }
  }

  getAnualFactor (options, year = null) {
    const nbWeeks = this.getNumberOfWorkedWeeks(options, year)
    let anualFactor = 0
    if (this.nMeals > 0) {
      anualFactor = (this.nWorkingDay * nbWeeks) / this.nMeals
    }
    return anualFactor
  }

  getCarbonIntensity (options, year = null) {
    let intensities = new CarbonIntensities()
    this.mealsIntensity = new CarbonIntensity()
    let anualFactor = this.getAnualFactor(options, year)
    for (let meal of this.meals) {
      intensities.add(meal.getCarbonIntensity(year).multiply(anualFactor))
    }
    this.mealsIntensity = intensities.sum()
    return this.mealsIntensity
  }

  toDatabase () {
    return {
      id: this.id,
      seqID: this.seqID,
      position: this.position,
      meals: this.meals,
      nWorkingDay: this.nWorkingDay,
      nCateringMeal: this.nCateringMeal,
      message: this.message,
      mdeleted: this.mdeleted,
      tags: this.tags
    }
  }

  static get MEALS () {
    let meals = Object.keys(FOOD_FACTORS)
    return Array.from(new Set(meals))
  }

  static get dietsWithoutMeatAndFish () {
    return ['vegan', 'vegetarian']
  }

  static ICONS (meal) {
    return {
      vegan: 'vegan',
      vegetarian: 'vegetarian',
      'classic.meet1': 'meat1',
      'classic.meet2': 'meat2',
      'classic.fish1': 'fish1',
      'classic.fish2': 'fish2'
    }[meal]
  }

  static getDiets (nmeals) {
    let diets = {
      vegan: {
        vegan: nmeals,
        vegetarian: 0,
        'classic.meet1': 0,
        'classic.meet2': 0,
        'classic.fish1': 0,
        'classic.fish2': 0
      },
      vegetarian: {
        vegan: Math.round((3 * nmeals) / 14),
        vegetarian: nmeals - Math.round((3 * nmeals) / 14),
        'classic.meet1': 0,
        'classic.meet2': 0,
        'classic.fish1': 0,
        'classic.fish2': 0
      }
    }
    diets['little.meat'] = {
      vegan: Math.round((1 * nmeals) / 14),
      'classic.meet1': Math.round((4 * nmeals) / 14),
      'classic.meet2': 0,
      'classic.fish1': Math.round((1 * nmeals) / 14),
      'classic.fish2': Math.round((1 * nmeals) / 14)
    }
    diets['little.meat']['vegetarian'] = nmeals - diets['little.meat']['vegan']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.meet1']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.meet2']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.fish1']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.fish2']
    diets['meat.regularly'] = {
      vegan: 0,
      vegetarian: Math.round((4 * nmeals) / 14),
      'classic.meet2': Math.round((2 * nmeals) / 14),
      'classic.fish1': Math.round((1 * nmeals) / 14),
      'classic.fish2': Math.round((1 * nmeals) / 14)
    }
    diets['meat.regularly']['classic.meet1'] =
      nmeals - diets['meat.regularly']['vegetarian']
    diets['meat.regularly']['classic.meet1'] -=
      diets['meat.regularly']['classic.meet2']
    diets['meat.regularly']['classic.meet1'] -=
      diets['meat.regularly']['classic.fish1']
    diets['meat.regularly']['classic.meet1'] -=
      diets['meat.regularly']['classic.fish2']
    diets['lot.of.meat'] = {
      vegan: 0,
      vegetarian: 0,
      'classic.meet2': Math.round((6 * nmeals) / 14),
      'classic.fish1': Math.round((1 * nmeals) / 14),
      'classic.fish2': Math.round((1 * nmeals) / 14)
    }
    diets['lot.of.meat']['classic.meet1'] =
      nmeals - diets['lot.of.meat']['classic.meet2']
    diets['lot.of.meat']['classic.meet1'] -=
      diets['lot.of.meat']['classic.fish1']
    diets['lot.of.meat']['classic.meet1'] -=
      diets['lot.of.meat']['classic.fish2']
    return diets
  }

  static createFromObj (obj) {
    return new Food({
      id: obj.id,
      seqID: obj.seqID,
      position: obj.position,
      sections: obj.sections,
      meals: obj.meals,
      nWorkingDay: obj.nWorkingDay,
      nWorkginDay2: obj.nWorkingDay2,
      nCateringMeal: obj.nCateringMeal,
      message: obj.message,
      cdeleted: obj.cdeleted,
      mdeleted: obj.mdeleted,
      tags: obj.tags
    })
  }

  static createEmpty () {
    return new Food()
  }

  static exportHeader (sep = '\t') {
    let header = [
      'seqid',
      'deleted',
      'commuting.days',
      'position',
      'catering.meals'
    ]
    for (let meal of Food.MEALS) {
      header.push(meal)
    }
    header.push('emission.kg.co2e')
    header.push('uncertainty.kg.co2e')
    header.push('tags')
    return header.join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = Food.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static compute (items, workforce, year, options, tags = []) {
    let intensities = new CarbonIntensities()
    let intensitiesPerMeals = {}
    let nitems = 0
    for (let meal of Food.MEALS) {
      intensitiesPerMeals[meal] = new CarbonIntensities()
    }
    for (let item of items) {
      let intensity = item.getCarbonIntensity(options, year)
      if (!item.mdeleted) {
        nitems += 1
        if (item.hasTags(tags)) {
          let anualFactor = item.getAnualFactor(options, year)
          intensities.add(intensity)
          for (let meal of item.meals) {
            let mintensity = meal.getCarbonIntensity(year).multiply(anualFactor)
            intensitiesPerMeals[meal.type].add(mintensity)
          }
        }
      }
    }
    let normalizationFactor = 0
    if (nitems > 0) {
      normalizationFactor = workforce / nitems
    }
    for (let meal of Object.keys(intensitiesPerMeals)) {
      intensitiesPerMeals[meal] = intensitiesPerMeals[meal]
        .sum()
        .multiply(normalizationFactor)
    }
    return {
      intensity: intensities.sum().multiply(normalizationFactor),
      meals: {
        intensity: intensitiesPerMeals
      }
    }
  }
}
