/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'
import Survey from '@/models/carbon/Survey.js'
import Meal from '@/models/carbon/Meal.js'
import FOOD_FACTORS from '@/../data/foodFactors.json'

export default class Food extends Survey {
  get nMeals () {
    return this.nWorkingDay * 2
  }

  getAmount (meal) {
    let cmeal = this.meals.filter(obj => obj.type === meal)
    if (cmeal.length === 0) {
      return 0
    } else {
      return cmeal[0].amount
    }
  }

  toString (sep = '\t') {
    let values = [
      this.seqID,
      this.nWorkingDay,
      this.position,
      this.nCateringMeal
    ]
    for (let meal of Food.MEALS) {
      values.push(this.getAmount(meal))
    }
    values.push(Math.round(this.mealsIntensity.intensity))
    values.push(Math.round(this.mealsIntensity.uncertainty))
    values.push(this.message)
    return values.join(sep)
  }

  addMeal (meal) {
    if (meal instanceof Meal) {
      this.meals.push(meal)
    } else {
      this.meals.push(Meal.createFromObj(meal))
    }
  }

  getNumberOfWorkedWeeks (settings, year = null) {
    let nbWeeks = settings.filter(obj => obj.name === 'NUMBER_OF_WORKED_WEEK.default')[0].value
    let years = settings.map(a => a.name.slice(-4))
    year = year.toString()
    if (years.includes(year)) {
      nbWeeks = settings.filter(obj => obj.name.endsWith(year))[0].value
    }
    return parseFloat(nbWeeks)
  }

  getAnualFactor (settings, year = null) {
    const nbWeeks = this.getNumberOfWorkedWeeks(settings, year)
    let anualFactor = 0
    if (this.nMeals > 0) {
      anualFactor = this.nWorkingDay * nbWeeks / this.nMeals
    }
    return anualFactor
  }

  getCarbonIntensity (settings, year = null) {
    let intensities = new CarbonIntensities()
    let anualFactor = this.getAnualFactor(settings, year)
    if (!this.deleted) {
      for (let meal of this.meals) {
        intensities.add(
          meal.getCarbonIntensity(year).multiply(anualFactor)
        )
      }
    }
    this.mealsIntensity = intensities.sum()
    return intensities.sum()
  }

  toDatabase () {
    return {
      'id': this.id,
      'seqID': this.seqID,
      'position': this.position,
      'meals': this.meals,
      'nWorkingDay': this.nWorkingDay,
      'nCateringMeal': this.nCateringMeal,
      'message': this.message
    }
  }

  static get MEALS () {
    let meals = Object.keys(FOOD_FACTORS)
    return Array.from(new Set(meals))
  }

  static get dietsWithoutMeatAndFish () {
    return ['vegan', 'vegetarian']
  }

  static ICONS (meal) {
    return {
      'vegan': 'vegan',
      'vegetarian': 'vegetarian',
      'classic.meet1': 'meat1',
      'classic.meet2': 'meat2',
      'classic.fish1': 'fish1',
      'classic.fish2': 'fish2'
    }[meal]
  }

  static getDiets (nmeals) {
    let diets = {
      'vegan': {
        'vegan': nmeals,
        'vegetarian': 0,
        'classic.meet1': 0,
        'classic.meet2': 0,
        'classic.fish1': 0,
        'classic.fish2': 0
      },
      'vegetarian': {
        'vegan': Math.round(3 * nmeals / 14),
        'vegetarian': nmeals - Math.round(3 * nmeals / 14),
        'classic.meet1': 0,
        'classic.meet2': 0,
        'classic.fish1': 0,
        'classic.fish2': 0
      }
    }
    diets['little.meat'] = {
      'vegan': Math.round(1 * nmeals / 14),
      'classic.meet1': Math.round(4 * nmeals / 14),
      'classic.meet2': 0,
      'classic.fish1': Math.round(1 * nmeals / 14),
      'classic.fish2': Math.round(1 * nmeals / 14)
    }
    diets['little.meat']['vegetarian'] = nmeals - diets['little.meat']['vegan']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.meet1']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.meet2']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.fish1']
    diets['little.meat']['vegetarian'] -= diets['little.meat']['classic.fish2']
    diets['meat.regularly'] = {
      'vegan': 0,
      'vegetarian': Math.round(4 * nmeals / 14),
      'classic.meet2': Math.round(2 * nmeals / 14),
      'classic.fish1': Math.round(1 * nmeals / 14),
      'classic.fish2': Math.round(1 * nmeals / 14)
    }
    diets['meat.regularly']['classic.meet1'] = nmeals - diets['meat.regularly']['vegetarian']
    diets['meat.regularly']['classic.meet1'] -= diets['meat.regularly']['classic.meet2']
    diets['meat.regularly']['classic.meet1'] -= diets['meat.regularly']['classic.fish1']
    diets['meat.regularly']['classic.meet1'] -= diets['meat.regularly']['classic.fish2']
    diets['lot.of.meat'] = {
      'vegan': 0,
      'vegetarian': 0,
      'classic.meet2': Math.round(6 * nmeals / 14),
      'classic.fish1': Math.round(1 * nmeals / 14),
      'classic.fish2': Math.round(1 * nmeals / 14)
    }
    diets['lot.of.meat']['classic.meet1'] = nmeals - diets['lot.of.meat']['classic.meet2']
    diets['lot.of.meat']['classic.meet1'] -= diets['lot.of.meat']['classic.fish1']
    diets['lot.of.meat']['classic.meet1'] -= diets['lot.of.meat']['classic.fish2']
    return diets
  }

  static createFromObj (obj) {
    return new Food(
      obj.id,
      obj.seqID,
      obj.position,
      obj.sections,
      obj.meals,
      obj.nWorkingDay,
      obj.nWorkingDay2,
      obj.nCateringMeal,
      obj.message,
      obj.deleted
    )
  }

  static createEmpty () {
    return new Food(
      null,
      null,
      null,
      [],
      [],
      null,
      null,
      null,
      null,
      false
    )
  }

  static exportHeader (sep = '\t') {
    let header = [
      'seqid',
      'commuting.days',
      'position',
      'catering.meals'
    ]
    for (let meal of Food.MEALS) {
      header.push(meal)
    }
    header.push('emission.kg.co2e')
    header.push('uncertainty.kg.co2e')
    header.push('message')
    return header.join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = Food.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static okToSubmit (items, module = null) {
    return items.length > 0
  }

  static compute (
    foods,
    workforce,
    year,
    settings
  ) {
    let intensities = new CarbonIntensities()
    let normalizationFactor = 0
    if (foods.length > 0) {
      normalizationFactor = workforce / foods.length
    }
    let intensitiesPerMeals = {}
    for (let meal of Food.MEALS) {
      intensitiesPerMeals[meal] = new CarbonIntensities()
    }
    for (let food of foods) {
      let anualFactor = food.getAnualFactor(settings, year)
      let intensity = food.getCarbonIntensity(settings, year)
      intensities.add(intensity)
      for (let meal of food.meals) {
        let mintensity = meal.getCarbonIntensity(year).multiply(anualFactor)
        intensitiesPerMeals[meal.type].add(mintensity)
      }
    }
    for (let meal of Object.keys(intensitiesPerMeals)) {
      intensitiesPerMeals[meal] = intensitiesPerMeals[meal].sum().multiply(normalizationFactor)
    }
    return {
      'intensity': intensities.sum().multiply(normalizationFactor),
      'meals': {
        'intensity': intensitiesPerMeals
      }
    }
  }
}
