import Food from './Food'
import { DEFAULT_SETTINGS } from '@/stores/constants.js'

function foodObj (modifier = undefined) {
  let food = {
    'seqID': null,
    'position': null,
    'meals': [],
    'nWorkingDay': null,
    'nWorkingDay2': null,
    'message': null,
    'deleted': false
  }

  if (modifier === undefined) {
    modifier = {}
  }

  for (let [key, value] of Object.entries(modifier)) {
    food[key] = value
  }
  return Food.createFromObj(food)
}

describe('getNumberOfWorkedWeeks', () => {
  test('2021 number of weeks', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: []
    })
    expect(food.getNumberOfWorkedWeeks(DEFAULT_SETTINGS, 2021)).toBe(37)
  })
  test('2022 number of weeks', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: []
    })
    expect(food.getNumberOfWorkedWeeks(DEFAULT_SETTINGS, 2022)).toBe(41)
  })
})

describe('getAnualFactor', () => {
  test('2021 annual factor', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: []
    })
    expect(food.getAnualFactor(DEFAULT_SETTINGS, 2021)).toBe(37 * 5 / 10)
  })
})

describe('getCarbonIntensity', () => {
  test('10 Vegetarian meals', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: [{
        type: 'vegetarian',
        amount: 10
      }]
    })
    let annualFactor = 37 * 5 / 10
    expect(food.getCarbonIntensity(DEFAULT_SETTINGS, 2021).intensity).toBe(1.115 * 10 * annualFactor)
  })
  test('5 Vegetarian meals + 5 Classic meal with meet 1', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: [{
        type: 'vegetarian',
        amount: 5
      }, {
        type: 'classic.meet1',
        amount: 5
      }]
    })
    let annualFactor = 37 * 5 / 10
    expect(food.getCarbonIntensity(DEFAULT_SETTINGS, 2021).intensity).toBe(1.115 * 5 * annualFactor + 2.098 * 5 * annualFactor)
  })
})

describe('Food.compute', () => {
  test('10 vegetarian meals for 20 lab members', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: [{
        type: 'vegetarian',
        amount: 10
      }]
    })
    let annualFactor = 37 * 5 / 10

    let r = Food.compute([food], 20, 2021, DEFAULT_SETTINGS)
    expect(r).toEqual(expect.objectContaining({
      intensity: expect.objectContaining({
        intensity: 1.115 * 10 * 20 * annualFactor,
        uncertainty: 1.115 * 10 * 20 * annualFactor * 0.5,
        intensitywc: 1.115 * 10 * 20 * annualFactor,
        uncertaintywc: 1.115 * 10 * 20 * annualFactor * 0.5,
        group: null
      })
    }))
  })
})
