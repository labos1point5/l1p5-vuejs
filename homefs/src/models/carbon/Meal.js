/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import {
  CarbonIntensity
} from '@/models/carbon/CarbonIntensity.js'
import EmissionFactor from '@/models/carbon/EmissionFactor.js'
import FOOD_FACTORS from '@/../data/foodFactors.json'

export default class Meal {
  constructor (type, amount) {
    this.type = type
    this.amount = amount
  }

  getEmissionFactor (year = null) {
    let ef = EmissionFactor.createFromObj(FOOD_FACTORS[this.type])
    return ef.getFactor(year)
  }

  getCarbonIntensity (year = null) {
    let ef = this.getEmissionFactor(year)
    return new CarbonIntensity(
      this.amount * ef.total.total,
      this.amount * ef.total.total * ef.total.uncertainty,
      null,
      null,
      ef.group
    )
  }

  static createFromObj (obj) {
    return new Meal(
      obj.type,
      obj.amount
    )
  }
}
