/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

const clonedeep = require('lodash.clonedeep')
const DECOMPOSITION = {
  'co2': 0.0,
  'ch4': 0.0,
  'n2o': 0.0,
  'others': 0.0,
  'total': 0.0,
  'uncertainty': 0.0
}

export default class EmissionFactor {
  constructor (
    decomposition, category, subcategory, subsubcategory, descriptionFR,
    descriptionEN, unit, group
  ) {
    this.decomposition = decomposition
    this.category = category
    this.subcategory = subcategory
    this.subsubcategory = subsubcategory
    this.descriptionFR = descriptionFR
    this.descriptionEN = descriptionEN
    this.unit = unit
    this.group = group
  }

  getYear (year = null) {
    let cyear = null
    if (this.decomposition.length === 1) {
      cyear = Object.keys(this.decomposition)[0]
    } else {
      if (year !== null) {
        if (Object.keys(this.decomposition).includes(String(year))) {
          cyear = String(year)
        } else {
          // if does not exist, find the closest year available
          let beforeYears = Object.keys(this.decomposition).filter(val => parseInt(val) <= parseInt(year))
          if (beforeYears.length === 1) {
            cyear = beforeYears[0]
          } else if (beforeYears.length > 1) {
            let closest = beforeYears.reduce((a, b) => {
              return Math.abs(parseInt(b) - parseInt(year)) < Math.abs(parseInt(a) - parseInt(year)) ? b : a
            })
            cyear = String(closest)
          } else {
            let closest = Object.keys(this.decomposition).reduce((a, b) => {
              return Math.abs(parseInt(b) - parseInt(year)) < Math.abs(parseInt(a) - parseInt(year)) ? b : a
            })
            cyear = String(closest)
          }
        }
      } else {
        // find the closest to current year
        let cYear = new Date().getFullYear()
        let closest = Object.keys(this.decomposition).reduce((a, b) => {
          return Math.abs(parseInt(b) - parseInt(cYear)) < Math.abs(parseInt(a) - parseInt(cYear)) ? b : a
        })
        cyear = String(closest)
      }
    }
    return cyear
  }

  getFactor (year = null) {
    let cyear = this.getYear(year)
    let factor = clonedeep(this.decomposition[cyear])
    for (let key of Object.keys(factor)) {
      if (typeof factor[key] === 'object') {
        factor[key] = Object.assign(
          {},
          DECOMPOSITION,
          factor[key]
        )
      }
    }
    factor.group = this.group
    return factor
  }

  getDecompositionKeys (year = null) {
    let cyear = this.getYear(year)
    return Object.keys(this.decomposition[cyear])
  }

  hasDecomposition (year = null) {
    let cyear = this.getYear(year)
    return Object.keys(this.decomposition[cyear]).length > 1
  }

  static createFromObj (factor = null) {
    if (factor) {
      return new EmissionFactor(
        factor.decomposition,
        factor.category,
        factor.subcategory,
        factor.subsubcategory,
        factor.descriptionFR,
        factor.descriptionEN,
        factor.unit,
        factor.group
      )
    } else {
      let cdate = new Date().getFullYear()
      return new EmissionFactor(
        {
          [cdate]: {
            'total': DECOMPOSITION,
            'combustion': DECOMPOSITION,
            'upstream': DECOMPOSITION,
            'manufacturing': DECOMPOSITION
          }
        },
        null,
        null,
        null,
        '',
        '',
        null,
        null
      )
    }
  }
}
