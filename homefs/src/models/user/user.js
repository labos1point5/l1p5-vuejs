/**
 * NOTE: there're similarities with the transition.Tag datastructure
 *       => we could factorize this in a common logic
 */
class Role {
  constructor () {
    this.descriptor = null
    this.i18n = {}
  }

  setDescriptor (descriptor) {
    this.descriptor = descriptor
    return this
  }

  setI18n (i18n) {
    this.i18n = i18n
    return this
  }

  translate (lang) {
    if (!('i18n' in this)) return this.descriptor
    if (!(lang in this.i18n)) return this.descriptor
    return this.i18n[lang]
  }

  static translate (d, lang) {
    return Role.fromJSON(d).translate(lang)
  }

  static fromJSON (d) {
    return new Role().setDescriptor(d.descriptor).setI18n(d.i18n)
  }
}

const ROLES = [
  {
    descriptor: 'reviewer',
    i18n: {
      fr: 'Relecteur·trice',
      en: 'Reviewer'
    }
  }
]

const ALL_ROLES = ROLES.map((r) => Role.fromJSON(r))

function makeRoles (descriptors) {
  let roles = descriptors
    .map((d) => {
      let candidates = ALL_ROLES.filter((r) => r.descriptor === d.trim())
      if (candidates.length === 1) {
        return candidates[0]
      }
      return null
    })
    .filter((r) => r !== null)

  return roles
}
/**
 * A user and its attributes
 *
 * Note on role.
 * The mapping between frontend user's role (here)
 * and backend implementation may seems cumbersome
 *
 * superuser is mapped to Django super user
 * reviewer is mapped to member of the django reviewer group
 * user is just a normal user (someone who isn't any of the above)
 */
class User {
  constructor () {
    this.dateJoined = null
    this.isActive = false
    this.isSuperUser = false
    this.email = ''
    this.active = false
    this.roles = makeRoles([])
  }

  setDateJoined (dateJoined) {
    this.dateJoined = dateJoined
    return this
  }

  setIsActive (isActive) {
    this.isActive = isActive
    return this
  }

  setIsSuperUser (isSuperUser) {
    this.isSuperUser = isSuperUser
    return this
  }

  setEmail (email) {
    this.email = email
    return this
  }

  setActive (active) {
    this.active = active
    return this
  }

  setRoles (roles) {
    this.roles = roles
    return this
  }

  static createFromObj (user) {
    return new User()
      .setDateJoined(new Date(user.date_joined))
      .setIsActive(user.is_active)
      .setIsSuperUser(user.is_superuser)
      .setEmail(user.email)
      .setActive(user.active)
      .setRoles(makeRoles(user.roles))
  }
}

export { ALL_ROLES, Role, makeRoles as makeRole, User }
