import MainNavbar from '@/layout/MainNavbar.vue'
import MainFooter from '@/layout/MainFooter.vue'
import { requireAuthenticated } from '@/router/utils'

export default [
  {
    path: '/scenario-1point5/',
    name: 'scenario-1point5',
    components: {
      default: () => import('@/views/Scenario1point5.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'Scénario 1point5',
      titleen: 'Scenario 1point5'
    }
  },
  {
    path: '/scenario-1point5/:uuid',
    name: 'scenario-1point5-uuid',
    components: {
      default: () => import('@/views/Scenario1point5.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    beforeEnter: requireAuthenticated,
    meta: {
      titlefr: 'Scénario 1point5',
      titleen: 'Scenario 1point5'
    }
  },
  {
    path: '/scenario-1point5-results/:uuid',
    name: 'scenario-1point5-results',
    components: {
      default: () => import('@/views/Scenario1point5Results.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'Scénario 1point5',
      titleen: 'Scenario 1point5'
    }
  }
]
