import MainNavbar from '@/layout/MainNavbar.vue'
import MainFooter from '@/layout/MainFooter.vue'

export default [
  {
    path: '/transition-1point5/',
    name: 'transition-1point5',
    components: {
      default: () => import('@/components/transition/Transition1point5.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'Transition 1point5',
      titleen: 'Transition 1point5'
    }
  },
  {
    path: '/transition-1point5/charter',
    name: 'transition-1point5-charter',
    components: {
      default: () => import('@/components/transition/wrappers/Charter.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'Transition 1point5 - La Charte',
      titleen: 'Transition 1point5 - The Charter'
    }
  },
  {
    path: '/transition-1point5/public/actions-map-fullscreen',
    name: 'transition-1point5-actions-map-fullscreen',
    components: {
      default: () => import('@/components/transition/maps/MapsFullscreen.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    beforeEnter: (to, from, next) => {
      // make sure we came from a place where the charter has been accepted
      if (
        from.name === 'transition-1point5' ||
        from.name === 'transition-1point5-named' ||
        from.name === 'administration-named'
      ) {
        next()
      } else {
        next('/transition-1point5')
      }
    },
    meta: {
      titlefr: 'Transition 1point5',
      titleen: 'Transition 1point5'
    }
  },
  {
    path: '/transition-1point5/public/:named',
    name: 'transition-1point5-named',
    components: {
      default: () => import('@/components/transition/Transition1point5.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    beforeEnter: (to, from, next) => {
      // intend: if we come from the fullscreen version we've already accepted the charter
      if (from.name === 'transition-1point5-actions-map-fullscreen') {
        next()
      } else {
        next('/transition-1point5')
      }
    },
    meta: {
      titlefr: 'Transition 1point5',
      titleen: 'Transition 1point5'
    }
  },
  {
    path: '/transition-1point5/reviewer/actions/:actionId',
    name: 'transition-1point5-reviewer-one-action',
    components: {
      default: () => import('@/components/transition/reviewer/ActionReview.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'Transition 1point5: relecture',
      titleen: 'Transition 1point5: review'
    }
  },

  {
    path: '/transition-1point5/public/actions/:actionId',
    name: 'transition-1point5-one-action',
    components: {
      default: () => import('@/components/transition/wrappers/ActionView.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'Transition 1point5: action',
      titleen: 'Transition 1point5: action'
    }
  }
]
