############################
#
# Runtime settings
#
############################

# Container names
# ---------------

L1P5="l1p5apps"
CONTAINER_L1P5="$L1P5"
CONTAINER_L1P5_DB="$L1P5-db"

# Webserver
# ---------

# if true: setup nginx reverse proxy and disable the below <PORT> parameter
# if false: expose l1p5 website on host, using the <PORT> parameter
# Check https://hub.docker.com/r/jwilder/nginx-proxy
NGINX_REVERSE_PROXY=false

# Connect l1p5 container to an existing Docker network
# which is used by nginx reverse-proxy container
DOCKER_NETWORK="picnet"
VIRTUAL_HOST="https://labo15.picttn.le-pic.org"

# Host port where is published container Apache port
# i.e. make site available on http://host:<PORT>
PORT=8182

# Log directory
LOG_DIR="/var/log/l1p5-apps"

# Storage directory for uploaded files
STORAGE_DIRECTORY_FOR_UPLOAD="pathToStorageDirectory"

# Database
# --------

DB_IMAGE="mariadb:10.9.2"

# If true: do not start 'l1p5_db' container and bind mount the socket on the host filesystem (might be useful for production)
# If false: start 'l1p5_db' container and use a docker volume to store the mariadb socket (recommended for development)
EXTERNAL_DB_SOCKET=false

MARIADB_ROOT_PASSWORD="mypassword"
DATABASE_NAME=l1p5
DATABASE_USER=l1p5
DATABASE_PASSWORD=mypassword
# Hostname or socket file, used only if EXTERNAL_DB_SOCKET=true
# set to "/run/mysqld/mysqld.sock" if EXTERNAL_DB_SOCKET=false
DATABASE_HOST=/var/run/mysqld/mysqld.sock
DATABASE_PORT=3306

# Django settings
# ---------------

# Email
EMAIL_HOST=localhost
EMAIL_PORT=25

# WARN: Disable debug mode for production
DEBUG=True

SECRET_KEY=unsafe-secret-key

############################
#
# Development mode settings
#
############################

# Containers and images
# ---------------------
GIT_HASH="$(git -C $DIR describe --dirty --always)"

# TAG is customized for gitlab-ci
TAG="${CI_PIPELINE_ID:-$GIT_HASH}"

# Image version created by build procedure
REPO="registry.gitlab.inria.fr/l1p5/l1p5-vuejs"
IMAGE="$REPO/l1p5apps:$TAG"

