#!/usr/bin/env bash

#
# Rationale: 
#   - reformat(prettier) and lint(eslint+vue plugin) our files
#   - used by pre-commit to run before committing

# Note "${@/homefs\//}" intent is to remove all homefs/ prefix from all the
# paths that are passed to the hook (seems that the replacement operate on each
# element of $@)
cd homefs && npx prettier --no-semi --single-quote --trailing-comma none  --write "${@/homefs\//}"  && npm run lint ${@/homefs\//}
